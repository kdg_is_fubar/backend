package be.kdg.fubar.backend.replay.controller;

import be.kdg.fubar.backend.flooding.bl.FloodingException;
import be.kdg.fubar.backend.game.bl.GameService;
import be.kdg.fubar.backend.identity.bl.UserException;
import be.kdg.fubar.backend.identity.bl.UserService;
import be.kdg.fubar.backend.identity.model.ApplicationUser;
import be.kdg.fubar.backend.player.PlayerException;
import be.kdg.fubar.backend.player.bl.PlayerService;
import be.kdg.fubar.backend.player.model.Player;
import be.kdg.fubar.backend.replay.bl.ReplayService;
import be.kdg.fubar.backend.replay.exception.ReplayException;
import be.kdg.fubar.backend.replay.model.Replay;
import be.kdg.fubar.backend.room.RoomException;
import be.kdg.fubar.backend.room.bl.RoomService;
import be.kdg.fubar.backend.room.controller.RoomController;
import be.kdg.fubar.backend.room.model.Room;
import be.kdg.fubar.backend.tile.TileException;
import be.kdg.fubar.backend.tile.bl.TileService;
import be.kdg.fubar.backend.tile.model.Tile;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext
public class ReplayTest {
    @Autowired
    private RoomService roomService;
    @Autowired
    private GameService gameService;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private UserService userService;
    @Autowired
    private BCryptPasswordEncoder encoder;
    @Autowired
    private PlayerService playerService;
    @Autowired
    private RoomController roomController;
    @Autowired
    private TileService tileService;
    @Autowired
    ReplayService replayService;
    private Room room;
    private Player player1;
    private Player player2;

    @Before
    @Transactional
    public void beforeTest() throws UserException, FloodingException, RoomException {

        Room room = roomService.createNewRoom("NamedRoom", 8,0,20000);
        ApplicationUser user1 = new ApplicationUser("TestUser123456", "chuckfon@lost.com", encoder.encode("somePassword"));
        user1.setEnabled(true);
        userService.saveNewUser(user1);
        ApplicationUser user2 = new ApplicationUser("TestUser123456789", "helpme@lost.com", encoder.encode("somePassword"));
        user2.setEnabled(true);
        userService.saveNewUser(user2);
        Player player1 = playerService.creatNewPlayer(user1.getEmail(), room);
        player1.setHost(true);
        this.player1 = playerService.save(player1);
        Player player2 = playerService.creatNewPlayer(user2.getEmail(), room);
        this.player2 = playerService.save(player2);
        roomService.addAI(room, 0);
        if (!roomService.getRooms().contains(room)) {
            roomService.saveRoom(room);
        }
        this.room = roomService.getRoom(room.getId());
        gameService.startGame(this.room.getId(), "TestUser123456");
    }
    @After
    public void cancelTimers() {
        for (Room roomServiceRoom : roomService.getRooms()) {
            if (roomServiceRoom.getTimer() != null) {
                roomServiceRoom.getTimer().cancel();
            }
        }
    }

    @Test
    @Transactional
    public void replayTest() throws ReplayException, FloodingException, RoomException, PlayerException, TileException {
        //region
        Tile tile1 = tileService.getTileByName("v").get().turnRight(1);
        tile1.setCoords(1, 0);
        Tile tile2 = tileService.getTileByName("x").get().turnRight(0);
        tile2.setCoords(1, 1);
        Tile tile3 = tileService.getTileByName("k").get().turnRight(2);
        tile3.setCoords(0, 1);
        gameService.placeTile(room.getId(), tile1);
        gameService.placeTile(room.getId(), tile2);
        gameService.placeTile(room.getId(), tile3);
        gameService.placeFollower(room.getId(), player1.getId(), 4, 8); //gives 4 points to player 1
        gameService.endGame(room.getId());
        //endregion
        List<Replay> replays = replayService.getAll(player1.getUsername());
        assert replays.size() ==1;
        assert replayService.nextMove(replays.get(0).getId()).get(1).getName().equals("v");
        assert player1.getScore() == 0;
        assert player2.getScore() == 0;
        assert replayService.nextMove(replays.get(0).getId()).get(2).getName().equals("x");
        assert player1.getScore() == 0;
        assert player2.getScore() == 0;
        assert replayService.nextMove(replays.get(0).getId()).get(3).getName().equals("k");
        assert player1.getScore() == 4;//2
        assert player2.getScore() == 0;
    }
}