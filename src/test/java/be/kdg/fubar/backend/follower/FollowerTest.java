package be.kdg.fubar.backend.follower;

import be.kdg.fubar.backend.flooding.bl.FloodingException;
import be.kdg.fubar.backend.game.bl.GameService;
import be.kdg.fubar.backend.identity.bl.UserException;
import be.kdg.fubar.backend.identity.bl.UserService;
import be.kdg.fubar.backend.identity.model.ApplicationUser;
import be.kdg.fubar.backend.player.bl.PlayerService;
import be.kdg.fubar.backend.player.model.Player;
import be.kdg.fubar.backend.room.RoomException;
import be.kdg.fubar.backend.room.bl.RoomService;
import be.kdg.fubar.backend.room.model.Room;
import be.kdg.fubar.backend.tile.bl.TileService;
import be.kdg.fubar.backend.tile.model.Tile;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@DirtiesContext
public class FollowerTest {
    @Autowired
    private RoomService roomService;
    @Autowired
    private PlayerService playerService;
    @Autowired
    private TileService tileService;
    @Autowired
    private UserService userService;
    @Autowired
    private GameService gameService;
    private Player player;
    private Room room;


    @Before
    public void setup() throws UserException, FloodingException, RoomException {
        userService.saveNewUser(new ApplicationUser("testUser", "test@test.com", "notEncoded"));
        room = roomService.createNewRoom("somename", 4, 0,999999);
        Player host = playerService.creatNewPlayer(userService.loadUserByUsername("testUser").getEmail(), room);
        host.setHost(true);
        playerService.save(host);
        roomService.addAI(room, 0);

        if (!roomService.getRooms().contains(room)) {
            roomService.saveRoom(room);
            room = roomService.getRoom(room.getId());
        }
        gameService.startGame(room.getId(), "testUser");
        this.player = host;
    }

    @After
    public void cancelTimers() {
        for (Room roomServiceRoom : roomService.getRooms()) {
            if (roomServiceRoom.getTimer() != null) {
                roomServiceRoom.getTimer().cancel();
            }
        }
    }
    @Test
    @Transactional
    public void placeFollowerTest() throws Exception {
        //placeTile
        Optional<Tile> optionalTile = tileService.getTileByName("x");
        if (!optionalTile.isPresent())
            throw new RuntimeException("Tile not found with name x");
        Tile tile = optionalTile.get();
        tile.setCoords(1, 0);
        gameService.placeTile(room.getId(), tile);
        room = roomService.getRoom(room.getId());
        //placeMeeple
        assert gameService.placeFollower(room.getId(), player.getId(), 0, 0);
        //check
        Room r = roomService.getRoom(room.getId());
        assert r.getBoard().contains(tile);

        assert r.getBoard().stream().anyMatch(t -> t.getLu().getLu().getFollower() != null);
        //endregion

    }

}