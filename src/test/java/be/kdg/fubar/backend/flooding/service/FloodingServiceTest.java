package be.kdg.fubar.backend.flooding.service;

import be.kdg.fubar.backend.TestTileFactory;
import be.kdg.fubar.backend.flooding.bl.FloodingException;
import be.kdg.fubar.backend.flooding.bl.FloodingService;
import be.kdg.fubar.backend.game.bl.GameService;
import be.kdg.fubar.backend.identity.bl.UserException;
import be.kdg.fubar.backend.identity.bl.UserService;
import be.kdg.fubar.backend.identity.model.ApplicationUser;
import be.kdg.fubar.backend.player.bl.PlayerService;
import be.kdg.fubar.backend.player.model.Player;
import be.kdg.fubar.backend.room.RoomException;
import be.kdg.fubar.backend.room.bl.RoomService;
import be.kdg.fubar.backend.room.model.Room;
import be.kdg.fubar.backend.tile.bl.TileService;
import be.kdg.fubar.backend.tile.model.SurfaceType;
import be.kdg.fubar.backend.tile.model.Tile;
import be.kdg.fubar.backend.tile.model.TileSegmentPart;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
@DirtiesContext
public class FloodingServiceTest {
    @Autowired
    private TestTileFactory tileFactory;
    @Autowired
    private FloodingService floodingService;
    @Autowired
    private RoomService roomService;
    @Autowired
    private TileService tileService;
    private Room room;
    @Autowired
    private UserService userService;
    @Autowired
    private PlayerService playerService;
    @Autowired
    private GameService gameService;

    @Before
    @Transactional
    public void before() throws UserException, FloodingException, RoomException {
        userService.saveNewUser(new ApplicationUser("testUser", "test@test.com", "notEncoded"));
        room = roomService.createNewRoom("somename", 4, 0,999999);
        Player host = playerService.creatNewPlayer(userService.loadUserByUsername("testUser").getEmail(), room);
        host.setHost(true);
        room.setCurrentPlayer(host);

        playerService.save(host);
        roomService.addAI(room, 0);

        if (!roomService.getRooms().contains(room)) {
            roomService.saveRoom(room);
            room = roomService.getRoom(room.getId());
        }
        gameService.startGame(room.getId(), "testUser");

        List<Tile> board = room.getBoard();

        Tile leftTile = tileFactory.horizontalRoad();
        Tile rightTile = tileFactory.horizontalRoad();

        leftTile.setCoords(0, 0);
        rightTile.setCoords(1, 0);

        board.add(leftTile);
        board.add(rightTile);
        room = roomService.saveRoom(room);
        //Finished Road


    }

    @After
    public void cancelTimers() {
        for (Room roomServiceRoom : roomService.getRooms()) {
            if (roomServiceRoom.getTimer() != null) {
                roomServiceRoom.getTimer().cancel();
            }
        }
    }

    @Test
    @Transactional
    public void testCompletedRoad() throws UserException, FloodingException, RoomException {
        Room room = roomService.createNewRoom("test", 4, 0,5000);
        room = roomService.saveRoom(room);
        Player host = playerService.creatNewPlayer(userService.loadUserByUsername("testUser").getEmail(), room);
        room.setCurrentPlayer(host);

        Tile left = tileFactory.monastaryWithRoad();
        Tile right = tileFactory.tRoad();

        left.setCoords(-1, 0);
        right.setCoords(1, 0);

        gameService.placeTile(room.getId(), left);

        floodingService.flood(room, -1, 0);

        assert !room.getBoard().get(0).getMm().getMm().isCompleted();
        gameService.placeTile(room.getId(), right);


        floodingService.flood(room, 1, 0);

        assert room.getBoard().get(0).getMm().getMm().isCompleted();
    }

    @Test
    public void testCompletedCity() throws FloodingException {
        Room someRoom = new Room("test", 4, 0);
        List<Tile> fCity = someRoom.getBoard();


        Tile left = tileFactory.cityRight();
        Tile right = tileFactory.cityLeft();

        left.setCoords(0, 0);
        right.setCoords(1, 0);
        fCity.add(left);
        fCity.add(right);

        floodingService.flood(someRoom, 0, 0);
        Tile s = someRoom.getBoard().get(0);

        assert someRoom.getBoard().get(0).getMr().getMm().isCompleted();


    }

    @Test
    public void testFailCompletedCity() {
        Room someRoom = new Room("test", 4, 0);
        List<Tile> fCity = someRoom.getBoard();

        Tile right = tileFactory.cityLeft();

        right.setCoords(0, 0);

        fCity.add(right);

        assert !someRoom.getBoard().get(0).getMr().getMm().isCompleted();

    }

    @Test
    public void floodTest() throws FloodingException {
        floodingService.flood(room, 1, 0);

        //Left Tile
        Optional<Tile> optionalLeftTile = room.getBoard()
                .stream()
                .filter(tile -> tile.getX() == 0 && tile.getY() == 0 && !tile.isStartTile())
                .findFirst();
        assertTrue(optionalLeftTile.isPresent());
        Tile leftTile = optionalLeftTile.get();

        //Right Tile
        Optional<Tile> optionalRightTile = room.getBoard()
                .stream()
                .filter(tile -> tile.getX() == 1 && tile.getY() == 0)
                .findFirst();
        assertTrue(optionalRightTile.isPresent());
        Tile rightTile = optionalRightTile.get();

        assertEquals(leftTile.getRu().getMr().getAdjecencyIdentifier(), rightTile.getLu().getMl().getAdjecencyIdentifier());
        assertEquals(leftTile.getMr().getMr().getAdjecencyIdentifier(), rightTile.getMl().getMl().getAdjecencyIdentifier());
        assertEquals(leftTile.getRd().getMr().getAdjecencyIdentifier(), rightTile.getLd().getMl().getAdjecencyIdentifier());
        assertNotEquals(leftTile.getRu().getMm().getAdjecencyIdentifier(), leftTile.getMr().getMm().getAdjecencyIdentifier());
        assertNotEquals(leftTile.getRu().getMm().getAdjecencyIdentifier(), leftTile.getRd().getMm().getAdjecencyIdentifier());
        assertNotEquals(leftTile.getMm().getMm().getAdjecencyIdentifier(), leftTile.getRd().getMm().getAdjecencyIdentifier());
    }

    @Test
    public void parseToMatrixTest() throws InvocationTargetException, IllegalAccessException, FloodingException {
        Method matrixMethod = Arrays.stream(floodingService.getClass().getDeclaredMethods())
                .filter(method -> method.getName().equals("parseToMatrix"))
                .findFirst().get();
        matrixMethod.setAccessible(true);
        TileSegmentPart[][][][][][] matrix = floodingService.parseToMatrix(room.getBoard());
        matrixMethod.setAccessible(false);
        assertNotNull(matrix);
        assertSame(matrix[0][0][0][0][0][0].getType(), SurfaceType.FIELD);
    }

    @Test
    @Transactional
    public void MonasteryCompletedTest() throws UserException, FloodingException, RoomException {
        Room room = roomService.createNewRoom("test", 4, 0,5000);
        room = roomService.saveRoom(room);
        Player host = playerService.creatNewPlayer(userService.loadUserByUsername("testUser").getEmail(), room);
        room.setCurrentPlayer(host);

        room.getBoard().set(0, tileFactory.monastaryWithRoad());
        Tile leftUpCorner = tileFactory.fieldFull();
        Tile midleUp = tileFactory.fieldFull();
        Tile rightUpCorner = tileFactory.fieldFull();
        Tile midleLeft = tileFactory.fieldFull();
        Tile leftDownCorner = tileFactory.fieldFull();
        Tile midleDown = tileFactory.fieldFull();
        Tile rightDownCorner = tileFactory.fieldFull();
        Tile midleRight = tileFactory.horizontalRoad();
        leftUpCorner.setCoords(-1, 1);
        midleUp.setCoords(0, 1);
        rightUpCorner.setCoords(1, 1);
        midleLeft.setCoords(-1, 0);
        leftDownCorner.setCoords(-1, -1);
        midleDown.setCoords(0, -1);
        rightDownCorner.setCoords(1, -1);
        midleRight.setCoords(1, 0);
        gameService.placeTile(room.getId(), midleUp);
        gameService.placeTile(room.getId(), midleLeft);
        gameService.placeTile(room.getId(), midleDown);
        gameService.placeTile(room.getId(), leftUpCorner);
        gameService.placeTile(room.getId(), rightUpCorner);
        gameService.placeTile(room.getId(), leftDownCorner);
        gameService.placeTile(room.getId(), rightDownCorner);

        floodingService.flood(room, 0, 0);

        assert !room.getBoard().get(0).getMm().getMm().isCompleted();
        gameService.placeTile(room.getId(), midleRight);
        floodingService.flood(room, 0, 0);


        assert room.getBoard().get(0).getMm().getMm().isCompleted();
    }
}