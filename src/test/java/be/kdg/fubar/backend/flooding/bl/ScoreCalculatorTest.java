package be.kdg.fubar.backend.flooding.bl;

import be.kdg.fubar.backend.TestTileFactory;
import be.kdg.fubar.backend.flooding.model.CoordinateDTO;
import be.kdg.fubar.backend.flooding.model.InfoObject;
import be.kdg.fubar.backend.game.bl.GameService;
import be.kdg.fubar.backend.identity.bl.UserException;
import be.kdg.fubar.backend.identity.bl.UserService;
import be.kdg.fubar.backend.identity.model.ApplicationUser;
import be.kdg.fubar.backend.player.PlayerException;
import be.kdg.fubar.backend.player.bl.PlayerService;
import be.kdg.fubar.backend.player.model.Player;
import be.kdg.fubar.backend.room.RoomException;
import be.kdg.fubar.backend.room.bl.RoomService;
import be.kdg.fubar.backend.room.model.Room;
import be.kdg.fubar.backend.tile.TileException;
import be.kdg.fubar.backend.tile.model.Tile;
import be.kdg.fubar.backend.tile.model.TileSegmentPart;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.AbstractMap;
import java.util.List;
import java.util.TreeMap;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext
public class ScoreCalculatorTest {
    @Autowired
    private ScoreCalculator scoreCalculator;
    @Autowired
    private TestTileFactory tileFactory;
    @Autowired
    private FloodingService floodingService;
    @Autowired
    private RoomService roomService;
    @Autowired
    private ScoreService scoreService;
    @Autowired
    private PlayerService playerService;
    @Autowired
    private UserService userService;
    @Autowired
    private GameService gameService;
    @Autowired
    private BCryptPasswordEncoder encoder;
    private Room room;
    private Player player1;
    private Player player2;
    private Player player3;
    private Player player4;
    TreeMap<Short, InfoObject> zoneMap;

    @Before
    @Transactional
    public void beforeEach() throws UserException, FloodingException, RoomException, PlayerException, TileException {
        ApplicationUser applicationUser1 = new ApplicationUser("someRegisteredUsername_1", "testtester2@hotmail.com", encoder.encode("somePassword"));
        applicationUser1.setEnabled(true);
        applicationUser1 = userService.saveNewUser(applicationUser1);
        ApplicationUser applicationUser2 = new ApplicationUser("someRegisteredUsername_2", "testtester3@hotmail.com", encoder.encode("somePassword"));
        applicationUser2.setEnabled(true);
        applicationUser2 = userService.saveNewUser(applicationUser2);
        ApplicationUser applicationUser3 = new ApplicationUser("someRegisteredUsername_3", "testtester4@hotmail.com", encoder.encode("somePassword"));
        applicationUser3.setEnabled(true);
        applicationUser3 = userService.saveNewUser(applicationUser3);
        ApplicationUser applicationUser4 = new ApplicationUser("someRegisteredUsername_4", "testtester5@hotmail.com", encoder.encode("somePassword"));
        applicationUser4.setEnabled(true);
        applicationUser4 = userService.saveNewUser(applicationUser4);

        room = roomService.createNewRoom("test_room", 8, 0,5000);
        player1 = playerService.creatNewPlayer(applicationUser1.getEmail(), room);//each time others ones
        player2 = playerService.creatNewPlayer(applicationUser2.getEmail(), room);//each time others ones
        player3 = playerService.creatNewPlayer(applicationUser3.getEmail(), room);//each time others ones
        player4 = playerService.creatNewPlayer(applicationUser4.getEmail(), room);//each time others ones

        //Start tile is Tile 0
        Tile horizontalRoad1 = tileFactory.horizontalRoad();
        Tile horizontalRoad2 = tileFactory.horizontalRoad();
        Tile horizontalRoad3 = tileFactory.horizontalRoad();
        Tile horizontalRoad4 = tileFactory.horizontalRoad();
        Tile cityCloserDown = tileFactory.cityLeft().turnRight(3);

        //Start tile is Tile@ 0,0
        horizontalRoad1.setCoords(1, 0);
        horizontalRoad2.setCoords(2, 0);
        horizontalRoad3.setCoords(3, 0);
        horizontalRoad4.setCoords(4, 0);
        cityCloserDown.setCoords(0, 1);

        boolean test1 = gameService.placeTile(room.getId(), horizontalRoad1);
        boolean test2 = gameService.placeTile(room.getId(), horizontalRoad2);
        boolean test3 = gameService.placeTile(room.getId(), horizontalRoad3);
        boolean test4 = gameService.placeTile(room.getId(), horizontalRoad4);
        boolean test5 = gameService.placeTile(room.getId(), cityCloserDown);

        playerService.placeFollower(player1.getId(), room.getBoard().get(0).getMm().getMm().getId());
        playerService.placeFollower(player1.getId(), room.getBoard().get(1).getMm().getMm().getId());
        playerService.placeFollower(player2.getId(), room.getBoard().get(2).getMm().getMm().getId());
        playerService.placeFollower(player2.getId(), room.getBoard().get(3).getMm().getMm().getId());
        playerService.placeFollower(player3.getId(), room.getBoard().get(4).getMm().getMm().getId());
        gameService.endTurn(room.getId());

        //player 4 doesn't get to place a Follower
        zoneMap = scoreService.matrixToInfoObjectMap(floodingService.parseToMatrix(room.getBoard()));
    }

    @Test
    @Transactional
    public void findMayorityShareHolderTest() {
        List<AbstractMap.SimpleEntry<CoordinateDTO, TileSegmentPart>> zone = zoneMap.get(room.getBoard().get(0).getMm().getMm().getAdjecencyIdentifier()).getSimpleEntries();
        List<Player> majorityShareHolders = scoreCalculator.findMayorityShareHolder(zone);
        assert majorityShareHolders.size() == 2;
        assert majorityShareHolders.contains(player1);
        assert majorityShareHolders.contains(player2);
        assert !majorityShareHolders.contains(player3);
        assert !majorityShareHolders.contains(player4);
    }

    @Test
    @Transactional
    public void scoreRoadTest() {
        List<AbstractMap.SimpleEntry<CoordinateDTO, TileSegmentPart>> zone = zoneMap.get(room.getBoard().get(0).getMm().getMm().getAdjecencyIdentifier()).getSimpleEntries();
        assert scoreCalculator.scoreRoad(zone) == 5;
    }

    @Test
    @Transactional
    public void scoreCityCompletedTest() {
        List<AbstractMap.SimpleEntry<CoordinateDTO, TileSegmentPart>> zone =
                zoneMap.get(room.getBoard().get(0).getMu().getMu().getAdjecencyIdentifier()).getSimpleEntries();
        assert scoreCalculator.scoreCity(zone) == 4;
    }

    @Test
    @Transactional
    public void scoreFieldTest() throws FloodingException {
        List<AbstractMap.SimpleEntry<CoordinateDTO, TileSegmentPart>> zone =
                zoneMap.get(room.getBoard().get(0).getMm().getMu().getAdjecencyIdentifier()).getSimpleEntries();
        assert scoreCalculator.scoreField(zone, floodingService.parseToMatrix(room.getBoard())) == 3;
    }


    @Test
    @Transactional
    public void finishRoadScoreTest() throws FloodingException, RoomException {
        Tile tRoad1 = tileFactory.tRoad();
        Tile tRoad2 = tileFactory.tRoad();
        tRoad1.setCoords(-1, 0);
        tRoad2.setCoords(5, 0);

        gameService.placeTile(room.getId(), tRoad1);
        gameService.endTurn(room.getId());
        assert player1.getScore() == 0;
        assert player2.getScore() == 0;
        assert player3.getScore() == 0;
        assert player4.getScore() == 0;
        gameService.placeTile(room.getId(), tRoad2);
        gameService.endTurn(room.getId());
        assert player1.getScore() == 7;
        assert player2.getScore() == 7;
        assert player3.getScore() == 0;
        assert player4.getScore() == 0;
    }

    @Test
    @Transactional
    public void finishFieldScoreTest() throws FloodingException, RoomException, PlayerException, TileException {
        Tile tField = tileFactory.fieldFull();
        Tile tField3 = tileFactory.fieldFull();
        Tile tField4 = tileFactory.fieldFull();
        Tile tCity = tileFactory.cityLeft();
        Tile tCity2 = tileFactory.cityLeft();
        Tile tRoad = tileFactory.horizontalRoad();
        tField.setCoords(-1, 1);
        tField3.setCoords(-1, 2);
        tField4.setCoords(-1, 3);
        tCity.turnRight(3);
        tCity2.turnRight(1);
        tCity.setCoords(0, 3);
        tCity2.setCoords(0, 2);
        tRoad.setCoords(-1, 0);

        gameService.placeTile(room.getId(), tRoad);
        gameService.endTurn(room.getId());

        gameService.placeTile(room.getId(), tField);
        gameService.endTurn(room.getId());

        gameService.placeTile(room.getId(), tField3);
        gameService.endTurn(room.getId());

        gameService.placeTile(room.getId(), tField4);
        gameService.endTurn(room.getId());

        gameService.placeTile(room.getId(), tCity2);
        gameService.endTurn(room.getId());

        gameService.placeTile(room.getId(), tCity);
        gameService.endTurn(room.getId());

        playerService.placeFollower(player1.getId(), room.getBoard().get(10).getMm().getMm().getId());

        assert player1.getScore() == 0;
        gameService.endGame(room.getId());
        assert roomService.getRoom(room.getId()).getPlayersScore().get(0) == 6;

    }

}