package be.kdg.fubar.backend;

import be.kdg.fubar.backend.identity.bl.UserService;
import be.kdg.fubar.backend.identity.model.ApplicationUser;
import be.kdg.fubar.backend.identity.model.ApplicationUserDTO;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@Transactional
@DirtiesContext
public class IdentityTests {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private UserService userService;
    @Autowired
    private BCryptPasswordEncoder encoder;

    @Before
    @Transactional
    public void beforeTests() throws Exception {
        ApplicationUser user = new ApplicationUser("someRegisteredUsername", "chuckfon@hotmail.com", encoder.encode("somePassword"));
        user.setEnabled(true);
        userService.saveNewUser(user);

    }


    @Test
    public void contextLoads() {
    }
    //End2End

    /**
     * The "login" test will see if when logging in:
     * 1. A Json Web Token (jwt) will be generated
     * 2. A redirect happens, which is default behavior of Spring Boot on successful login.
     *
     * @throws Exception
     */
    @Test
    @Transactional
    public void login() throws Exception {
        ObjectMapper om = new ObjectMapper();
        om.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        ObjectWriter ow = om.writer().withDefaultPrettyPrinter();
        ApplicationUserDTO existingPlayer = new ApplicationUserDTO("someRegisteredUsername", "chuckfon@hotmail.com", "somePassword");
        String request = ow.writeValueAsString(existingPlayer);
        mockMvc.perform(post("/login").contentType(MediaType.APPLICATION_JSON_UTF8).content(request)).andExpect(status().isOk()).andExpect(header().exists("Authorization"));
    }

    /**
     * the "failedLogin" test will test if when given wrong data the login will fail and give back a 401 http status code (Unauthorized).
     *
     * @throws Exception
     */
    @Test
    @Transactional
    public void failedLogin() throws Exception {
        ObjectMapper om = new ObjectMapper();
        om.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        ObjectWriter ow = om.writer().withDefaultPrettyPrinter();
        ApplicationUser existingPlayer = new ApplicationUser("someRegisteredUsername", "chuckfon@hotmail.com", "someWrongPassword");

        String request = ow.writeValueAsString(existingPlayer);

        mockMvc.perform(post("/login").contentType(MediaType.APPLICATION_JSON_UTF8).content(request)).andExpect(status().isUnauthorized());
    }

    /**
     * The "register" test will see if a correctly filled out UserDTO can be registered
     *
     * @throws Exception
     */
    @Test
    @Transactional
    public void register() throws Exception {
        ObjectMapper om = new ObjectMapper();
        om.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        ObjectWriter ow = om.writer().withDefaultPrettyPrinter();
        ApplicationUserDTO newPlayer = new ApplicationUserDTO("someUnregistered", "waffle.dealer@outlook.com", "somePassword");
        String request = ow.writeValueAsString(newPlayer);
        mockMvc.perform(post("/user/api/register").contentType(MediaType.APPLICATION_JSON_UTF8).content(request)).andExpect(status().is(202));
    }

}
