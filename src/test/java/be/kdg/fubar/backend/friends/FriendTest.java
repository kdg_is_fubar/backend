package be.kdg.fubar.backend.friends;

import be.kdg.fubar.backend.friends.model.FriendDTO;
import be.kdg.fubar.backend.identity.bl.UserService;
import be.kdg.fubar.backend.identity.model.ApplicationUser;
import be.kdg.fubar.backend.identity.model.ApplicationUserDTO;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@RunWith(SpringRunner.class)
@DirtiesContext
@AutoConfigureMockMvc
public class FriendTest {
    @Autowired
    private UserService userService;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @Autowired
    private MockMvc mockMvc;
    private String authentication;

    @Before
    @Transactional
    public void makeUsers() throws Exception {
        ApplicationUser applicationUser1 = new ApplicationUser("User1", "killer@queen.touch", bCryptPasswordEncoder.encode("hallo"));
        ApplicationUser applicationUser2 = new ApplicationUser("Friend1", "kono@dio.da", bCryptPasswordEncoder.encode("hallo"));
        applicationUser1.setEnabled(true);
        applicationUser2.setEnabled(true);
        userService.saveNewUser(applicationUser1);
        userService.saveNewUser(applicationUser2);

        ObjectMapper om = new ObjectMapper();
        ObjectWriter ow = om.writer();

        ApplicationUserDTO existingPlayer = new ApplicationUserDTO("User1", "killer@queen.touch", "hallo");

        String request = ow.writeValueAsString(existingPlayer);

        MvcResult result = mockMvc.perform(post("/login").contentType(MediaType.APPLICATION_JSON_UTF8).content(request)).andReturn();
        authentication = result.getResponse().getHeader("Authorization");

    }

    @Test
    @Transactional
    public void addFriend() throws Exception {
        FriendDTO addDTO = new FriendDTO();
        addDTO.setCurrentUsername("User1");
        addDTO.setFriendUsername("Friend1");

        ObjectMapper om = new ObjectMapper();
        om.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        ObjectWriter ow = om.writer();
        String content="";

        content = ow.writeValueAsString(addDTO);

        mockMvc.perform(post("/friend/add").content(content).accept("application/json;charset=UTF-8").contentType("application/json;charset=UTF-8").header("Authorization",authentication)).andExpect(status().isOk());
    }
}
