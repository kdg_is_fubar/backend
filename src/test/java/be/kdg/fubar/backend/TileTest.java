package be.kdg.fubar.backend;

import be.kdg.fubar.backend.tile.TileException;
import be.kdg.fubar.backend.tile.bl.TileService;
import be.kdg.fubar.backend.tile.dal.TileSegmentCreator;
import be.kdg.fubar.backend.tile.model.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
@DirtiesContext
public class TileTest {
    @Autowired
    private TileService tileService;
    @Autowired
    private TestTileFactory tileFactory;
    @Autowired
    private TileSegmentCreator tsc;

    // region CREATING TILES
    @Test
    public void makeSegmentTest() {
        TileSegment segement = new TileSegment();
    }

    @Test
    public void makeTileSegmentTest() {
        TileSegment tileSegment = new TileSegment();
        assert tileSegment.getLd().getType().equals(SurfaceType.FIELD);
        assert tileSegment.getMd().getType().equals(SurfaceType.FIELD);
        assert tileSegment.getMl().getType().equals(SurfaceType.FIELD);
        assert tileSegment.getMr().getType().equals(SurfaceType.FIELD);
        assert tileSegment.getMm().getType().equals(SurfaceType.FIELD);
        assert tileSegment.getLu().getType().equals(SurfaceType.FIELD);
        assert tileSegment.getLd().getType().equals(SurfaceType.FIELD);
        assert tileSegment.getRu().getType().equals(SurfaceType.FIELD);
        assert tileSegment.getRd().getType().equals(SurfaceType.FIELD);
        TileSegment tileSegmentparam = tsc.cityFull();
        assert tileSegmentparam.getLd().getType().equals(SurfaceType.CITY);
        assert tileSegmentparam.getMd().getType().equals(SurfaceType.CITY);
        assert tileSegmentparam.getMl().getType().equals(SurfaceType.CITY);
        assert tileSegmentparam.getMr().getType().equals(SurfaceType.CITY);
        assert tileSegmentparam.getMm().getType().equals(SurfaceType.CITY);
        assert tileSegmentparam.getLu().getType().equals(SurfaceType.CITY);
        assert tileSegmentparam.getLd().getType().equals(SurfaceType.CITY);
        assert tileSegmentparam.getRu().getType().equals(SurfaceType.CITY);
        assert tileSegmentparam.getRd().getType().equals(SurfaceType.CITY);
    }

    @Test
    public void makeTileTest() {
        Tile tile = new Tile();
        assert tile.getMu().equals(tsc.fieldFull());
        assert tile.getMl().equals(tsc.fieldFull());
        assert tile.getMm().equals(tsc.fieldFull());

        assert tile.getMr().equals(tsc.fieldFull());
        assert tile.getMu().equals(tsc.fieldFull());
        assert tile.getLd().equals(tsc.fieldFull());

        assert tile.getLu().equals(tsc.fieldFull());
        assert tile.getRd().equals(tsc.fieldFull());
        assert tile.getRu().equals(tsc.fieldFull());
    }

    @Test
    public void turnCompletelyTest() {
        Tile tile = tileFactory.getDemoTileCorner();
        Tile turned = tileFactory.getDemoTileCorner();
        turned = turned.turnRight(2);
        turned = turned.turnRight(2);//turns twice 2 times to get to 4 (just 4 would turn 0 times)
        assert tile.equals(turned);
    }

    @Test
    public void turnOnceTest() {
        Tile tile = tileFactory.getDemoTileCorner();
        Tile turned = tileFactory.getDemoTileCorner();
        turned.turnRight();
        assert !tile.equals(turned);
    }

    @Test
    public void turnSegmentTest() {
        TileSegment unturned = tsc.roadVertical(0);
        TileSegment turned = tsc.roadVertical(0).turnRight(1);
        assert !unturned.equals(turned);
        assert unturned.getLu().getType().equals(SurfaceType.FIELD);
        assert unturned.getMu().getType().equals(SurfaceType.ROAD);
        assert unturned.getRu().getType().equals(SurfaceType.FIELD);
        assert unturned.getMl().getType().equals(SurfaceType.FIELD);
        assert unturned.getMm().getType().equals(SurfaceType.ROAD);
        assert unturned.getMr().getType().equals(SurfaceType.FIELD);
        assert unturned.getLd().getType().equals(SurfaceType.FIELD);
        assert unturned.getMd().getType().equals(SurfaceType.ROAD);
        assert unturned.getRd().getType().equals(SurfaceType.FIELD);

        assert turned.getLu().getType().equals(SurfaceType.FIELD);
        assert turned.getMu().getType().equals(SurfaceType.FIELD);
        assert turned.getRu().getType().equals(SurfaceType.FIELD);
        assert turned.getMl().getType().equals(SurfaceType.ROAD);
        assert turned.getMm().getType().equals(SurfaceType.ROAD);
        assert turned.getMr().getType().equals(SurfaceType.ROAD);
        assert turned.getLd().getType().equals(SurfaceType.FIELD);
        assert turned.getMd().getType().equals(SurfaceType.FIELD);
        assert turned.getRd().getType().equals(SurfaceType.FIELD);
    }

    @Test
    public void turnRightCornerTest() {
        Tile turned = tileFactory.getDemoTileCorner();
        turned.turnRight();
        assert turned.getLu().equals(tsc.fieldFull());
        assert turned.getMu().equals(tsc.fieldFull());
        assert turned.getRu().equals(tsc.cityCornerDownRight(3));

        assert turned.getMl().equals(tsc.fieldFull());
        assert turned.getMm().equals(tsc.fieldFull());
        assert turned.getMr().equals(tsc.fieldFull());

        assert turned.getLd().equals(tsc.fieldFull());
        assert turned.getMd().equals(tsc.fieldFull());
        assert turned.getRd().equals(tsc.fieldFull());

    }

    @Test
    public void turnRightMiddleTest() {
        Tile turned = tileFactory.getDemoTileMiddle();
        turned.turnRight();
        assert turned.getLu().equals(tsc.fieldFull());
        assert turned.getMu().equals(tsc.fieldFull());
        assert turned.getRu().equals(tsc.fieldFull());

        assert turned.getMl().equals(tsc.fieldFull());
        assert turned.getMm().equals(tsc.fieldFull());
        assert turned.getMr().equals(tsc.roadTdown(1));

        assert turned.getLd().equals(tsc.fieldFull());
        assert turned.getMd().equals(tsc.fieldFull());
        assert turned.getRd().equals(tsc.fieldFull());
    }

// endregion
    // region MATCHING TILES

    @Test
    public void matchFullTileTest() {
        Tile cross_base = tileFactory.crossRoad();
        cross_base.setCoords(0, 0);
        Tile cross_up = tileFactory.verticalRoad();
        cross_up.setCoords(0, 1);
        Tile cross_down = tileFactory.verticalRoad();
        cross_down.setCoords(0, -1);
        Tile cross_left = tileFactory.horizontalRoad();
        cross_left.setCoords(-1, 0);
        Tile cross_right = tileFactory.horizontalRoad();
        cross_right.setCoords(1, 0);

        List<Tile> circled = new ArrayList<Tile>() {{
            add(cross_up);
            add(cross_down);
            add(cross_left);
            add(cross_right);
        }};
        assert tileService.matchTile(circled, cross_base);
    }

    @Test
    public void matchFullTileFailTest() {
        Tile tRoad = tileFactory.tRoad();
        tRoad.setCoords(0, 0);
        Tile cross_up = tileFactory.verticalRoad();
        cross_up.setCoords(0, 1);
        Tile cross_down = tileFactory.verticalRoad();
        cross_down.setCoords(0, -1);
        Tile cross_left = tileFactory.horizontalRoad();
        cross_left.setCoords(-1, 0);
        Tile cross_right = tileFactory.horizontalRoad();
        cross_right.setCoords(1, 0);

        List<Tile> circled = new ArrayList<Tile>() {{
            add(cross_up);
            add(cross_down);
            add(cross_left);
            add(cross_right);
        }};


        assert !tileService.matchTile(circled, tRoad);
    }

    //endregion
    //region FollowerPlacement related
    @Test
    public void getTileSegmentPartFromRelativeCoordinatesTest() throws TileException {
        Tile tileCorner = tileFactory.getDemoTileCorner();
        Tile tileCrossRoad = tileFactory.crossRoad();
        assert tileService.getTileSegmentPartFromRelativeCoordinates(tileCorner, 0, 0).getType().equals(SurfaceType.ELDRITCH);
        assert tileService.getTileSegmentPartFromRelativeCoordinates(tileCorner, 0, 1).getType().equals(SurfaceType.CITY);
        assert tileService.getTileSegmentPartFromRelativeCoordinates(tileCrossRoad, 4, 4).getType().equals(SurfaceType.ELDRITCH);
        assert tileService.getTileSegmentPartFromRelativeCoordinates(tileCrossRoad, 4, 5).getType().equals(SurfaceType.ROAD);
    }
    //endregion
}
