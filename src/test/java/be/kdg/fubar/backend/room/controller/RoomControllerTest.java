package be.kdg.fubar.backend.room.controller;

import be.kdg.fubar.backend.TestTileFactory;
import be.kdg.fubar.backend.identity.bl.UserException;
import be.kdg.fubar.backend.identity.bl.UserService;
import be.kdg.fubar.backend.identity.model.ApplicationUser;
import be.kdg.fubar.backend.identity.model.ApplicationUserDTO;
import be.kdg.fubar.backend.room.bl.RoomService;
import be.kdg.fubar.backend.room.model.JoinRoomDTO;
import be.kdg.fubar.backend.room.model.JoiningPlayer;
import be.kdg.fubar.backend.room.model.Room;
import be.kdg.fubar.backend.room.model.RoomDTO;
import be.kdg.fubar.backend.tile.dal.TileSegmentCreator;
import be.kdg.fubar.backend.tile.model.Tile;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.MediaType;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.stomp.StompFrameHandler;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.Transport;
import org.springframework.web.socket.sockjs.client.WebSocketTransport;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@DirtiesContext
public class RoomControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private RoomService roomService;
    @Autowired
    private TileSegmentCreator tsc;
    @Autowired
    private TestTileFactory tileFactory;
    @Autowired
    private UserService userService;
    @Autowired
    private BCryptPasswordEncoder encoder;
    private String authentication;
    private String authentication2;

    @LocalServerPort
    int port;

    private CompletableFuture<JoiningPlayer> completableFuture;
    private CompletableFuture<JoiningPlayer> completableFuture2;
    private CompletableFuture<String> completableSFuture;

    private static final String SENDJOIN = "/rooms/join/";
    private static final String SENDGAME = "/rooms/game/1/start";
    private static final String SUBJOIN = "/joined/";
    private static final String SUBGAME = "/game/1";
    private Room room;

    @Before
    public void beforeTests() throws Exception {
        completableFuture = new CompletableFuture<>();
        completableFuture2 = new CompletableFuture<>();
        room = roomService.createNewRoom("Test", 4, 0,5000);

        completableSFuture = new CompletableFuture<>();
        ApplicationUser user = new ApplicationUser("someRegisteredUsername", "chuckfon@hotmail.com", encoder.encode("somePassword"));
        user.setEnabled(true);
        try {
            userService.saveNewUser(user);
        } catch (UserException e) {
            System.out.println(e);
        }

        ApplicationUser user2 = new ApplicationUser("someRegisteredUsername2", "chuckfon2@hotmail.com", encoder.encode("somePassword"));
        user2.setEnabled(true);
        try {
            userService.saveNewUser(user2);
        } catch (UserException e) {
            System.out.println(e);
        }

        ObjectMapper om = new ObjectMapper();
        om.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        ObjectWriter ow = om.writer().withDefaultPrettyPrinter();
        ApplicationUserDTO existingPlayer = new ApplicationUserDTO(user.getUsername(), user.getEmail(), "somePassword");
        ApplicationUserDTO existingPlayer2 = new ApplicationUserDTO(user2.getUsername(), user2.getEmail(), "somePassword");

        String request = ow.writeValueAsString(existingPlayer);
        String request2 = ow.writeValueAsString(existingPlayer2);

        MvcResult result = mockMvc.perform(post("/login").contentType(MediaType.APPLICATION_JSON_UTF8).content(request)).andReturn();
        authentication = result.getResponse().getHeader("Authorization");

        MvcResult result2 = mockMvc.perform(post("/login").contentType(MediaType.APPLICATION_JSON_UTF8).content(request2)).andReturn();
        authentication2 = result2.getResponse().getHeader("Authorization");
    }


    private List<Transport> createTransportClient() {
        List<Transport> transports = new ArrayList<>(1);
        transports.add(new WebSocketTransport(new StandardWebSocketClient()));
        return transports;
    }

    @Test
    public void createNewRoomTest() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        RoomDTO dto = new RoomDTO();
        dto.setName("testRoom");
        dto.setMaxPlayerCount(4);
        dto.setUsername("someRegisteredUsername");
        String jsonRoom = objectMapper.writeValueAsString(dto);
        mockMvc.perform(post("/room")
                .contentType("application/json").header("Authorization", authentication)
                .content(jsonRoom))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("name", is("testRoom")));
    }


    @Test
    public void joinRoom() throws Exception {
        //region user1
        JoinRoomDTO testDTO = new JoinRoomDTO();
        testDTO.setJoining(true);
        testDTO.setUsername("someRegisteredUsername");
        String AUTHPOINT = "http://localhost:" + port
                + "/broker-guide?token=" + authentication;

        WebSocketStompClient stompClient = new WebSocketStompClient(new SockJsClient(createTransportClient()));
        stompClient.setMessageConverter(new MappingJackson2MessageConverter());
        StompSession stompSession = stompClient.connect(AUTHPOINT, new StompSessionHandlerAdapter() {
        }).get(100, SECONDS);

        stompSession.subscribe(SUBJOIN + room.getId(), new CreateGameStompFrameHandler(completableFuture));
        stompSession.send(SENDJOIN + room.getId(), testDTO);
        JoiningPlayer gameState = completableFuture.get(10, SECONDS);
        System.out.println(gameState);
        //endregion

        //region applicationUser2
        JoinRoomDTO testDTO2 = new JoinRoomDTO();
        testDTO2.setJoining(true);
        testDTO2.setUsername("someRegisteredUsername2");
        String AUTHPOINT2 = "http://localhost:" + port
                + "/broker-guide?token=" + authentication2;

        WebSocketStompClient stompClient2 = new WebSocketStompClient(new SockJsClient(createTransportClient()));
        stompClient2.setMessageConverter(new MappingJackson2MessageConverter());
        StompSession stompSession2 = stompClient2.connect(AUTHPOINT2, new StompSessionHandlerAdapter() {
        }).get(100, SECONDS);

        stompSession2.subscribe(SUBJOIN + room.getId(), new CreateGameStompFrameHandler(completableFuture2));
        stompSession2.send(SENDJOIN + room.getId(), testDTO2);
        JoiningPlayer gameState2 = completableFuture2.get(10, SECONDS);
        System.out.println(gameState2);
        //endregion

        ApplicationUser applicationUser = userService.loadUserByUsername("someRegisteredUsername");
        ApplicationUser applicationUser2 = userService.loadUserByUsername("someRegisteredUsername2");
        room = roomService.getRoom(room.getId());
        assert applicationUser.getPlayers().get(0).getId().equals(room.getPlayers().get(0).getId());
        assert applicationUser2.getPlayers().get(0).getId().equals(room.getPlayers().get(1).getId());
        assertNotNull(gameState);
    }

    private class CreateGameStompFrameHandler implements StompFrameHandler {
        private CompletableFuture<JoiningPlayer> completableFuture;

        CreateGameStompFrameHandler(CompletableFuture<JoiningPlayer> completableFuture) {
            this.completableFuture = completableFuture;
        }

        @Override
        public Type getPayloadType(StompHeaders stompHeaders) {
            return JoiningPlayer.class;
        }

        @Override
        public void handleFrame(StompHeaders stompHeaders, Object o) {
            this.completableFuture.complete((JoiningPlayer) o);
        }
    }

    private class CreateHandler implements StompFrameHandler {
        @Override
        public Type getPayloadType(StompHeaders stompHeaders) {
            return String.class;
        }

        @Override
        public void handleFrame(StompHeaders stompHeaders, Object o) {
            completableSFuture.complete((String) o);
        }
    }

}