package be.kdg.fubar.backend.room;

import be.kdg.fubar.backend.TestTileFactory;
import be.kdg.fubar.backend.flooding.bl.FloodingException;
import be.kdg.fubar.backend.flooding.bl.FloodingService;
import be.kdg.fubar.backend.flooding.bl.ScoreService;
import be.kdg.fubar.backend.follower.bl.FollowerService;
import be.kdg.fubar.backend.game.bl.GameService;
import be.kdg.fubar.backend.identity.bl.UserException;
import be.kdg.fubar.backend.identity.bl.UserService;
import be.kdg.fubar.backend.identity.model.ApplicationUser;
import be.kdg.fubar.backend.player.bl.PlayerService;
import be.kdg.fubar.backend.player.model.Player;
import be.kdg.fubar.backend.room.bl.RoomService;
import be.kdg.fubar.backend.room.model.Room;
import be.kdg.fubar.backend.tile.bl.TileService;
import be.kdg.fubar.backend.tile.model.Tile;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@Transactional
@DirtiesContext
public class RoomTest {

    @Autowired
    private RoomService roomService;
    @Autowired
    private TileService tileService;
    private Room r, room;
    @Autowired
    private TestTileFactory tileFactory;
    @Autowired
    FollowerService followerService;
    @Autowired
    PlayerService playerService;
    @Autowired
    private TileService tm;
    @Autowired
    private FloodingService floodingService;
    @Autowired
    private UserService userService;
    @Autowired
    private ScoreService scoreService;
    @Autowired
    private GameService gameService;

    @Before
    @Transactional
    public void setup() throws UserException, FloodingException, RoomException {
        userService.saveNewUser(new ApplicationUser("testUser", "test@test.com", "notEncoded"));
        r = roomService.createNewRoom("somename", 4, 0, 999999);
        Player host = playerService.creatNewPlayer(userService.loadUserByUsername("testUser").getEmail(), r);
        host.setHost(true);
        playerService.save(host);
        roomService.addAI(r, 0);

        if (!roomService.getRooms().contains(r)) {
            roomService.saveRoom(r);
            r = roomService.getRoom(r.getId());
        }
        gameService.startGame(r.getId(), "testUser");
        room = roomService.createNewRoom("someothername", 4, 0, 999999);
        Player hostTwo = playerService.creatNewPlayer(userService.loadUserByUsername("testUser").getEmail(), room);
        hostTwo.setHost(true);
        playerService.save(hostTwo);
        roomService.addAI(room, 0);
        if (!roomService.getRooms().contains(room)) {
            roomService.saveRoom(room);
            room = roomService.getRoom(room.getId());

        }
        gameService.startGame(room.getId(), "testUser");
    }

    @After
    public void cancelTimers() {
        for (Room roomManagerRoom : roomService.getRooms()) {
            if (roomManagerRoom.getTimer() != null) {
                roomManagerRoom.getTimer().cancel();
            }
        }
    }

    @Test
    public void createRoomTest() throws FloodingException {
        r = roomService.createNewRoom("aah", 4, 0, 5000);
        room = roomService.createNewRoom("iii", 4, 0, 5000);
        assert r.getBoard() != null;
        assert r.getDeck() != null;
        assert !r.getBoard().isEmpty();
        assert !r.getDeck().isEmpty();
    }

    @Test
    @Transactional
    public void boardTest() throws FloodingException, RoomException {
        assert !r.getBoard().isEmpty();
        Tile t = tileService.getStartTile();
        assert r.getBoard().contains(t);
        Tile addedTile = new Tile(tileFactory.horizontalRoad()
        );
        addedTile.setCoords(1, 0);
        gameService.placeTile(r.getId(), addedTile);
        assert roomService.getRoom(r.getId()).getBoard().contains(addedTile);
    }

    @Test
    @Transactional
    public void boardAddFailTest() throws FloodingException, RoomException {
        Tile addedTile = tileService.getStartTile();
        addedTile.setCoords(0, 0);
        gameService.placeTile(room.getId(), addedTile);
        assert roomService.getRoom(room.getId()).getBoard().size() == 1;
    }

    @Test
    @Transactional
    public void addToBoardTest() throws FloodingException, RoomException {
        Tile horizontalRoad1 = tileFactory.horizontalRoad();
        horizontalRoad1.setCoords(-1, 0);
        Tile horizontalRoad2 = tileFactory.horizontalRoad();
        horizontalRoad2.setCoords(1, 0);
        gameService.placeTile(room.getId(), horizontalRoad1);
        gameService.placeTile(room.getId(), horizontalRoad2);
        assert roomService.getRoom(room.getId()).getBoard().size() == 3;
    }

    @Test
    @Transactional
    public void addToBoardFailTest() throws FloodingException, RoomException {
        Tile horizontalRoad1 = tileFactory.horizontalRoad();
        horizontalRoad1.setCoords(-1, 0);
        Tile horizontalRoad2 = tileFactory.horizontalRoad();
        horizontalRoad2.setCoords(1, 0);
        gameService.placeTile(room.getId(), horizontalRoad1);
        gameService.placeTile(room.getId(), horizontalRoad2);
        assert roomService.getRoom(room.getId()).getBoard().size() == 3;
        Tile horizontalRoad3 = tileFactory.horizontalRoad();
        horizontalRoad2.setCoords(0, 1);
        gameService.placeTile(room.getId(), horizontalRoad3);
        assert roomService.getRoom(room.getId()).getBoard().size() == 3;
    }

}

