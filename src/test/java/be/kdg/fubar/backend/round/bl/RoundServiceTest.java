package be.kdg.fubar.backend.round.bl;

import be.kdg.fubar.backend.player.bl.PlayerService;
import be.kdg.fubar.backend.player.model.Player;
import be.kdg.fubar.backend.room.bl.RoomService;
import be.kdg.fubar.backend.room.model.Room;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RoundServiceTest {

    @Autowired
    private RoundService roundManager;
    @Autowired
    private PlayerService playerService;
    @Autowired
    private RoomService roomService;
    private Room room;

    @Before
    @Transactional
    public void setUp() throws Exception {
        this.room = roomService.createNewRoom("Test", 2, 0, 5000);
        playerService.creatNewPlayer("waffle.dealer@outlook.com", this.room);
        playerService.creatNewPlayer("ccc@hotmail.com", this.room);
    }

    @Test
    @Transactional
    public void setupGame() {
        //Executing the method
        room = roundManager.setupGame(room);

        //Doing the asserts
        assertNotNull(room);
        assertNotNull(room.getCurrentPlayer());
        assertTrue(room.getPlayers().contains(room.getCurrentPlayer()));
    }

    @Test
    @Transactional
    public void endRound() {
        //Setup
        room = roundManager.setupGame(room);
        Player firstPlayer = room.getCurrentPlayer();

        //Playing first round
        room = roundManager.endRound(room);

        //Doing the asserts
        assertNotNull(room);
        Player secondPlayer = room.getCurrentPlayer();
        assertNotEquals(firstPlayer, secondPlayer);

        //Playing second round
        roundManager.endRound(room);

        //Doing the asserts
        assertEquals(firstPlayer, room.getCurrentPlayer());
        assertNotEquals(secondPlayer, room.getCurrentPlayer());
    }
}