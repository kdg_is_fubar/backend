package be.kdg.fubar.backend.player;

import be.kdg.fubar.backend.TestTileFactory;
import be.kdg.fubar.backend.flooding.bl.FloodingException;
import be.kdg.fubar.backend.flooding.bl.FloodingService;
import be.kdg.fubar.backend.flooding.bl.ScoreService;
import be.kdg.fubar.backend.flooding.model.InfoObject;
import be.kdg.fubar.backend.game.bl.GameService;
import be.kdg.fubar.backend.identity.bl.UserException;
import be.kdg.fubar.backend.identity.bl.UserService;
import be.kdg.fubar.backend.identity.model.ApplicationUser;
import be.kdg.fubar.backend.player.bl.PlayerService;
import be.kdg.fubar.backend.player.model.Player;
import be.kdg.fubar.backend.room.RoomException;
import be.kdg.fubar.backend.room.bl.RoomService;
import be.kdg.fubar.backend.room.model.Room;
import be.kdg.fubar.backend.tile.TileException;
import be.kdg.fubar.backend.tile.model.Tile;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.TreeMap;


@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext
public class PlayerStatTest {
    @Autowired
    private TestTileFactory tileFactory;
    @Autowired
    private FloodingService floodingService;
    @Autowired
    private RoomService roomService;
    @Autowired
    private ScoreService scoreService;
    @Autowired
    private PlayerService playerService;
    @Autowired
    private UserService userService;
    @Autowired
    private GameService gameService;
    @Autowired
    private BCryptPasswordEncoder encoder;
    private Room room;
    private Player player1;
    private Player player2;

    TreeMap<Short, InfoObject> zoneMap;

    @Before
    @Transactional
    public void beforeEach() throws UserException, FloodingException, RoomException, PlayerException, TileException {
        ApplicationUser applicationUser1 = new ApplicationUser("someRegisteredUsername_6", "testtester6@hotmail.com", encoder.encode("somePassword"));
        applicationUser1.setEnabled(true);
        applicationUser1 = userService.saveNewUser(applicationUser1);
        ApplicationUser applicationUser2 = new ApplicationUser("someRegisteredUsername_7", "testtester7@hotmail.com", encoder.encode("somePassword"));
        applicationUser2.setEnabled(true);
        applicationUser2 = userService.saveNewUser(applicationUser2);
        room = roomService.createNewRoom("test_room_stat", 6, 0, 999999);
        player1 = playerService.creatNewPlayer(applicationUser1.getEmail(), room);//each time others ones
        player2 = playerService.creatNewPlayer(applicationUser2.getEmail(), room);//each time others ones


        //Start tile is Tile 0
        Tile cityCloserDown = tileFactory.cityLeft().turnRight(3);

        //Start tile is Tile@ 0,0
        cityCloserDown.setCoords(0, 1);

        gameService.placeTile(room.getId(), cityCloserDown);

        //place follower on city, should score 4 points
        playerService.placeFollower(player1.getId(), room.getBoard().get(0).getMu().getMm().getId());

        zoneMap = scoreService.matrixToInfoObjectMap(floodingService.parseToMatrix(room.getBoard()));
    }


    @Test
    @Transactional
    public void StatSave() throws RoomException, FloodingException {

        assert player1.getScore() == 0;
        gameService.endGame(room.getId());
        assert roomService.getRoom(room.getId()).getPlayersScore().get(0) == 4;
        ApplicationUser bob = userService.loadUserByUsername(player1.getUsername());
        assert bob.getMaximum_score() == 4;
        assert bob.getTimes_played() == 1;
        assert bob.getAverage_score() == 4;
        assert bob.getTimes_won() == 1;

    }
}
