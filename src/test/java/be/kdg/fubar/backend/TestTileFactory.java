package be.kdg.fubar.backend;

import be.kdg.fubar.backend.tile.bl.TileService;
import be.kdg.fubar.backend.tile.dal.TileSegmentCreator;
import be.kdg.fubar.backend.tile.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.test.annotation.DirtiesContext;

@Component
@DirtiesContext
public class TestTileFactory {
    @Autowired
    private TileSegmentCreator tsc;
    @Autowired
    private TileService tileService;

    public Tile cityRight() {
        if (!tileService.getTileByName("cityRight").isPresent()) {
            Tile tile = new Tile(tsc.fieldFull(), tsc.fieldFull(), tsc.fieldFull(),
                    tsc.fieldFull(), tsc.fieldFull(), tsc.cityFull(),
                    tsc.fieldFull(), tsc.fieldFull(), tsc.fieldFull());
            tile.setName("cityRight");
            tileService.getTileMap().put("cityRight", tile);
        }
        return new Tile(tileService.getTileByName("cityRight").get());
    }

    public Tile cityLeft() {
        if (!tileService.getTileByName("cityLeft").isPresent()) {
            Tile tile = new Tile(tsc.fieldFull(), tsc.fieldFull(), tsc.fieldFull(),
                    tsc.cityFull(), tsc.fieldFull(), tsc.fieldFull(),
                    tsc.fieldFull(), tsc.fieldFull(), tsc.fieldFull());
            tile.setName("cityLeft");
            tileService.getTileMap().put("cityLeft", tile);
        }
        return new Tile(tileService.getTileByName("cityLeft").get());
    }

    public Tile tRoad() {

        return new Tile(tileService.getTileByName("w").get()).turnRight(2);
    }

    public Tile crossRoad() {
        return new Tile(tileService.getTileByName("x").get());
    }

    public Tile verticalRoad() {
        return new Tile(tileService.getTileByName("u").get());
    }

    public Tile horizontalRoad() {
        return verticalRoad().turnRight(1);
    }

    public Tile monastaryWithRoad() {
        //Road o the right
        if (!tileService.getTileByName("monastaryWithRoad").isPresent()) {
            Tile tile = new Tile(tsc.fieldFull(), tsc.fieldFull(), tsc.fieldFull(),
                    tsc.fieldFull(), tsc.monestary(), tsc.roadVertical(1),
                    tsc.fieldFull(), tsc.fieldFull(), tsc.fieldFull());
            tile.setName("monastaryWithRoad");
            tileService.getTileMap().put("monastaryWithRoad", tile);
        }
        return new Tile(tileService.getTileByName("monastaryWithRoad").get());

    }


    public Tile getDemoTileCorner() {
        if (!tileService.getTileByName("demoC").isPresent()) {
            Tile tile = new Tile(tsc.cityCornerDownRight(2), new TileSegment(), new TileSegment(),
                    new TileSegment(), new TileSegment(), new TileSegment(),
                    new TileSegment(), new TileSegment(), new TileSegment());
            tile.setName("demoC");
            tileService.getTileMap().put("demoC", tile);
        }
        return new Tile(tileService.getTileByName("demoC").get());
    }

    public Tile getDemoTileMiddle() {
        if (!tileService.getTileByName("demoU").isPresent()) {
            Tile tile = new Tile(new TileSegment(), tsc.roadTdown(0), new TileSegment(),
                    new TileSegment(), new TileSegment(), new TileSegment(),
                    new TileSegment(), new TileSegment(), new TileSegment());
            tile.setName("demoU");
            tileService.getTileMap().put("demoU", tile);
        }
        return new Tile(tileService.getTileByName("demoU").get());
    }

    public Tile fieldFull() {
        if (!tileService.getTileByName("fieldFull").isPresent()) {
            Tile tile = new Tile(new TileSegment(), new TileSegment(), new TileSegment(),
                    new TileSegment(), new TileSegment(), new TileSegment(),
                    new TileSegment(), new TileSegment(), new TileSegment());
            tile.setName("fieldFull");
            tileService.getTileMap().put("fieldFull", tile);
        }
        return new Tile(tileService.getTileByName("fieldFull").get());
    }
}
