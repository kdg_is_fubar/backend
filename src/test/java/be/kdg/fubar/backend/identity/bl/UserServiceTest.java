package be.kdg.fubar.backend.identity.bl;

import be.kdg.fubar.backend.flooding.bl.FloodingException;
import be.kdg.fubar.backend.identity.model.ApplicationUser;
import be.kdg.fubar.backend.notification.bl.NotificationService;
import be.kdg.fubar.backend.room.bl.RoomService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@Transactional
@DirtiesContext
public class UserServiceTest {
    @Autowired
    private UserService userService;
    @Autowired
    NotificationService notificationService;
    @Autowired
    RoomService roomService;

    @Before
    public void beforeTests() throws Exception {
        userService.saveNewUser(new ApplicationUser("testUser3", "test3@test.com", "notEncoded"));
    }

    @Test
    public void createNotification() throws FloodingException {
        assert userService.loadUserByUsername("testUser3").getNotifications().size() == 0;
        notificationService.createNotification("testUser3", "Hi there this is your notification",roomService.createNewRoom("apple",3, 0,99999));
        assert userService.loadUserByUsername("testUser3").getNotifications().size() == 1;
    }
}