package be.kdg.fubar.backend.invitation.controller;

import be.kdg.fubar.backend.identity.util.UserUtil;
import be.kdg.fubar.backend.invitation.bl.InvitationService;
import be.kdg.fubar.backend.room.RoomException;
import be.kdg.fubar.backend.room.model.InviteStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("invite")
@RestController
public class InvitationController {
    private final UserUtil userUtil;
    private final InvitationService invitationService;

    @Autowired
    public InvitationController(UserUtil userUtil, InvitationService invitationService) {
        this.userUtil = userUtil;
        this.invitationService = invitationService;
    }

    @GetMapping("/{roomId}/{username}")
    public InviteStatus invite(@PathVariable("roomId") Long roomId, @PathVariable("username") String invite_username) throws RoomException {
        String inviter_username = userUtil.getUserByToken();
        return invitationService.invite(roomId, invite_username, inviter_username);
    }

    @GetMapping("/{roomId}/email/{email}")
    public InviteStatus inviteByEmail(@PathVariable("roomId") Long roomId, @PathVariable("email") String email) throws RoomException {
        String inviter_username = userUtil.getUserByToken();
        return invitationService.inviteByEmail(roomId, email, inviter_username);
    }
}
