package be.kdg.fubar.backend.invitation.bl;

import be.kdg.fubar.backend.identity.bl.EmailCouldNotSendException;
import be.kdg.fubar.backend.identity.bl.EmailService;
import be.kdg.fubar.backend.identity.bl.UserException;
import be.kdg.fubar.backend.identity.bl.UserService;
import be.kdg.fubar.backend.identity.model.ApplicationUser;
import be.kdg.fubar.backend.notification.bl.NotificationService;
import be.kdg.fubar.backend.notification.model.FIREBASEMESSAGETYPE;
import be.kdg.fubar.backend.room.RoomException;
import be.kdg.fubar.backend.room.bl.RoomService;
import be.kdg.fubar.backend.room.model.InviteStatus;
import be.kdg.fubar.backend.room.model.Room;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class InvitationService {
    private final RoomService roomService;
    private final NotificationService notificationService;
    private final UserService userService;
    private final EmailService emailService;

    @Autowired
    public InvitationService(RoomService roomService, NotificationService notificationService, UserService userService, EmailService emailService) {
        this.roomService = roomService;
        this.notificationService = notificationService;
        this.userService = userService;
        this.emailService = emailService;
    }

    /**
     * This method will invite a player in a room
     * @param roomId The room which the player is being invited to
     * @param invited_username The username of the user who is being invited
     * @param inviter_username The username of the user is sent the invite
     * @return The InviteStatus of the invitee
     * @throws RoomException an exception concerning the Rooms
     */
    public InviteStatus invite(Long roomId, String invited_username, String inviter_username) throws RoomException {
        Room room = roomService.getRoom(roomId);
        try {
            notificationService.sendNotifications(invited_username, String.format("%s invited you to join %s", inviter_username, room.getName()), room, FIREBASEMESSAGETYPE.USER);
            return InviteStatus.INVITATION_SENT;
        } catch (UsernameNotFoundException e) {
            return InviteStatus.NOT_REGISTERED;
        }
    }
    /**
     * This method will invite a player in a room via Email
     * @param roomId The room which the player is being invited to
     * @param email The email of the user who is being invited
     * @param inviter_username The username of the user is sent the invite
     * @return The InviteStatus of the invitee
     * @throws RoomException an exception concerning the Rooms
     */
    public InviteStatus inviteByEmail(Long roomId, String email, String inviter_username) throws RoomException {
        Room room = roomService.getRoom(roomId);
        try {
            ApplicationUser applicationUser = userService.loadUserByEmail(email);
            notificationService.createNotification(applicationUser.getUsername(), String.format("%s invited you to join (by email) %s", inviter_username, room.getName()), room);
            return InviteStatus.INVITATION_SENT;
        } catch (UserException e) {
            try {
                emailService.sendMailInvite(inviter_username, email);
            } catch (EmailCouldNotSendException e1) {
                return InviteStatus.UNABLE_TO_SENT;
            }
            return InviteStatus.NOT_REGISTERED;
        }
    }
}
