package be.kdg.fubar.backend.identity.util;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

@Component
public class UserUtil {
    /**
     * This method will return the username of the person who has initiated the request with the backend.
     * This will be derived from his JWT.
     * @return the username of the user who has initiated the request.
     */
    public String getUserByToken() {
        String username;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            username = ((UserDetails) principal).getUsername();
        } else {
            username = principal.toString();
        }
        return username;
    }
}
