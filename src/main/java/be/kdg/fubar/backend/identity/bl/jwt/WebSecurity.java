package be.kdg.fubar.backend.identity.bl.jwt;

import be.kdg.fubar.backend.identity.bl.UserService;

import be.kdg.fubar.backend.identity.config.CorsFilterImplementation;
import be.kdg.fubar.backend.identity.config.JSONConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.client.web.AuthorizationRequestRepository;
import org.springframework.security.oauth2.client.web.HttpSessionOAuth2AuthorizationRequestRepository;
import org.springframework.security.oauth2.core.endpoint.OAuth2AuthorizationRequest;


@EnableWebSecurity
public class WebSecurity extends WebSecurityConfigurerAdapter {
    private JSONConfig JSONConfig;
    private UserService userService;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public WebSecurity(UserService userService, BCryptPasswordEncoder bCryptPasswordEncoder, JSONConfig JSONConfig) {
            this.userService = userService;
            this.bCryptPasswordEncoder = bCryptPasswordEncoder;
            this.JSONConfig = JSONConfig;
        }

    @Bean
    CorsFilterImplementation corsFilterImplementation() {
        CorsFilterImplementation filter = new CorsFilterImplementation();
        return filter;
    }


    /**
     * configure will have cors and csrf disabled
     * any urls having POSTs & OPTIONs, the register URL and the Log In URL do not need to be validated for incoming requests
     * (Log In URL is not needed anymore, because of the addFilter it gets delegated to the JWT Authentication object)
     * JWTAuthentication & JWT Authorization will be called when a request comes in that needs to be validated (this is set on any URL which is not the ones mentioned above).
     * @param http
     * @throws Exception
     */
        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http.cors().and().csrf().disable().authorizeRequests()
                    .antMatchers(HttpMethod.OPTIONS).permitAll()
                    .antMatchers(HttpMethod.POST, JSONConfig.getResetPass(),JSONConfig.getChangePass(), JSONConfig.getSignupurl(), JSONConfig.getLoginurl(),JSONConfig.getVerificationurl()).permitAll()
                    .anyRequest().authenticated()
                    .and()
                    .addFilter(new JWTAuthentication(authenticationManager(), JSONConfig))
                    .addFilter(new JWTAuthorization(authenticationManager(), JSONConfig))
                    .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        }


    @Bean
    public AuthorizationRequestRepository<OAuth2AuthorizationRequest>
    authorizationRequestRepository() {

        return new HttpSessionOAuth2AuthorizationRequestRepository();
    }

    /**
     * Configuration of the authenticationManager.
     * Tells the authenticationManager which service to use to search users in and which encoder to use to encode passwords.
     * @param auth
     * @throws Exception
     */
        @Override
        public void configure(AuthenticationManagerBuilder auth) throws Exception {
            auth.userDetailsService(userService).passwordEncoder(bCryptPasswordEncoder);
        }
}
