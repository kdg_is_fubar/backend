package be.kdg.fubar.backend.identity.bl;

public class EmailCouldNotSendException extends Exception {
    public EmailCouldNotSendException(String message) {
        super(message);
    }

}
