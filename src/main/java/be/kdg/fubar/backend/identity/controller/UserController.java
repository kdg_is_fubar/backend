package be.kdg.fubar.backend.identity.controller;

import be.kdg.fubar.backend.identity.bl.EmailCouldNotSendException;
import be.kdg.fubar.backend.identity.bl.UserException;
import be.kdg.fubar.backend.identity.bl.UserService;
import be.kdg.fubar.backend.identity.model.ApplicationUser;
import be.kdg.fubar.backend.identity.model.ChangePasswordDTO;
import be.kdg.fubar.backend.identity.model.ResetPasswordDTO;
import be.kdg.fubar.backend.identity.model.VerificationDTO;
import be.kdg.fubar.backend.identity.util.UserUtil;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


/**
 * Rest controller for registering and logging in.
 * (This class and line will need to be removed if we don't use this anymore for testing).
 */
@RestController
@RequestMapping("/user/api")
public class UserController {
    private UserService userService;
    private UserUtil userUtil;

    public UserController(UserService userService, UserUtil userUtil) {
        this.userService = userService;
        this.userUtil = userUtil;
    }

    @PostMapping("/register")
    public ResponseEntity signUp(@RequestBody ApplicationUser applicationUser) throws UserException {
        Boolean registered = userService.register(applicationUser);
        if (!registered) {
            return new ResponseEntity(HttpStatus.CONFLICT);
        } else {
            return new ResponseEntity(HttpStatus.ACCEPTED);
        }
    }

    @GetMapping("/profile")
    public ApplicationUser getProfile(@RequestHeader("Authorization") String token) {
        String username = this.userUtil.getUserByToken();
        return this.userService.loadUserByUsername(username);
    }

    @GetMapping("/visit/{username}")
    public ApplicationUser getVisitorProfile(@PathVariable("username") String username) {
        return this.userService.loadUserByUsername(username);
    }

    @GetMapping("/delete")
    public int deleteProfile(@RequestHeader("Authorization") String token) throws UserException {
        String username = this.userUtil.getUserByToken();
        return this.userService.deleteUser(username);
    }
    @PostMapping("/resetPassword")
    public void resetPassword(@RequestBody ResetPasswordDTO resetPasswordDTO) throws EmailCouldNotSendException, UserException {
        userService.sendReset(resetPasswordDTO.getEmail());
    }

    @PostMapping("/changePassword")
    public void changePassword(@RequestBody ChangePasswordDTO changeDTO) throws UserException {
        userService.changePassword(changeDTO.getEmail(),changeDTO.getPassword());
    }

    @PostMapping("/changeStatsStatus")
    public boolean changeStatus() {
        ApplicationUser user = this.userService.loadUserByUsername(userUtil.getUserByToken());
        user.setStatsPublic(!user.isStatsPublic());
        this.userService.updateUser(user);
        return user.isStatsPublic();
    }

    @PostMapping("/verifyToken")
    public void verify(@RequestBody VerificationDTO verificationDTO) {
        ApplicationUser user = this.userService.loadUserByUsername(verificationDTO.getUsername());
        if (verificationDTO.getVerificationToken().equals(user.getVerificationToken())) {
            user.setEnabled(true);
            this.userService.updateUser(user);

        }
    }


}
