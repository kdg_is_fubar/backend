package be.kdg.fubar.backend.identity.dal;

import be.kdg.fubar.backend.identity.model.ApplicationUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Repository of the users of the application, also called CarcasonnePlayers.
 */
@Repository
public interface UserRepo extends JpaRepository<ApplicationUser, Long> {
    Optional<ApplicationUser> findApplicationUserByEmail(String email);

    Optional<ApplicationUser> findUserByUsername(String user);
    int deleteByUsername(String username);
}
