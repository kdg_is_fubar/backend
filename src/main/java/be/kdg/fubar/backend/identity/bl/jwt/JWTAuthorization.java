package be.kdg.fubar.backend.identity.bl.jwt;

import be.kdg.fubar.backend.identity.config.JSONConfig;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;



/**
 * This class will handle the authorization of a jwt token.
 * It will see if the jwt that has been send is still valid with the constants that have been used to create it in the first call
 * These constants can be found in the IdentityConfig class.
 */
public class JWTAuthorization  extends BasicAuthenticationFilter {

    private JSONConfig JSONConfig;

    public JWTAuthorization(AuthenticationManager authManager, JSONConfig JSONConfig) {
        super(authManager);
        this.JSONConfig = JSONConfig;
    }

    /**
     * Will do an internal filter to get the authentication and see if the token is still valid.
     * Once it has been validated it will continue doing what it was asked to do from the client.
     * @param req the HTTPRequest that has initiated the authorization
     * @param res the response of the backend
     * @param chain the chain of stuff spring boot does will be paused and continued at the end of the method.
     * @throws IOException
     * @throws ServletException
     */
    @Override
    protected void doFilterInternal(HttpServletRequest req,
                                    HttpServletResponse res,
                                    FilterChain chain) throws IOException, ServletException {
        String header;
        if (req.getRequestURL().toString().contains("broker-guide")) {
            header = req.getQueryString().replace("token=","").replace("%20"," ");
        } else {
            header = req.getHeader(JSONConfig.getHeaderstring());
        }
        if (header == null || !header.startsWith(JSONConfig.getTokenprefix())) {
            chain.doFilter(req, res);
            return;
        }

        UsernamePasswordAuthenticationToken authentication = getAuthentication(req);

        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(req, res);
    }

    /**
     * Will return the authentication of the requester
     * @param request the HTTPRequest that has initiated the authorization
     * @return a UsernamePasswordAuthenticationToken if the JWT has a certain user embedded.
     */
    private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request) {
        String token;
        if (request.getRequestURL().toString().contains("broker-guide")) {
            token = request.getParameter("token");
        } else {
            token = request.getHeader(JSONConfig.getHeaderstring());
        }
        if (token != null) {
            String user = JWT.require(Algorithm.HMAC512(JSONConfig.getSecret().getBytes()))
                    .build()
                    .verify(token.replace(JSONConfig.getTokenprefix() + " ", ""))
                    .getSubject();

            if (user != null) {
                return new UsernamePasswordAuthenticationToken(user, null, new ArrayList<>());
            }
        }
        return null;
    }
}
