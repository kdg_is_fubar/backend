package be.kdg.fubar.backend.identity.bl;

/**
 * Custom Exception for when an exception happens in the UserService.
 */
public class UserException extends Exception {
    public UserException(String message) {
        super(message);
    }
}
