package be.kdg.fubar.backend.identity.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@ConfigurationProperties(prefix = "jwt")
@Configuration
@Data
public class JSONConfig {
    private String secret;
    private String expirationtime;
    private String tokenprefix;
    private String headerstring;
    private String signupurl;
    private String loginurl;
    private String sendgridkey;
    private String baseurl;
    private String verificationurl;
    private String resetPass;
    private String changePass;

}
