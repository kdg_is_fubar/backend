package be.kdg.fubar.backend.identity.bl;

import be.kdg.fubar.backend.identity.config.JSONConfig;
import com.sendgrid.*;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class EmailService {
    private SendGrid client;
    private JSONConfig config;

    public EmailService(JSONConfig config) {
        this.config = config;
        client = new SendGrid(config.getSendgridkey());
    }

    public void sendRegistrationMail(Email registeredUser, String username, String verificationToken) throws EmailCouldNotSendException {
        Email from = new Email("Fubar@Carcassonne.com");
        String subject = "Please activate your account";
        Content content = new Content("text/html", "<p>You can activate you account by clicking <a href=\"" + config.getBaseurl() + "?username=" + username + "&token=" + verificationToken + "\">somewhere</a></p>");
        sendMail(from, subject, registeredUser, content);
    }

    public void sendMailInvite(String inviter_username, String email) throws EmailCouldNotSendException {
        Email from = new Email("Fubar@Carcassonne.com");
        String subject = "A friend has invited you to join!";
        Content content = new Content("text/html", "" +
                "<p>" + inviter_username + " has invited you to join!</p>" +
                "<p>You can register for a new account,  <a href=\"" + config.getBaseurl() + "/register" + "\">here</a></p>" +
                "");
        Email to = new Email(email);
        sendMail(from, subject, to, content);

    }

    private void sendMail(Email from, String subject, Email to, Content content) throws EmailCouldNotSendException {
        Mail constructedMail = new Mail(from, subject, to, content);
        Request request = new Request();
        try {
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(constructedMail.build());
            Response response = client.api(request);
        } catch (IOException ex) {
            throw new EmailCouldNotSendException("Email couldn't be sent to user");
        }
    }

    public void sendResetMail(String email) throws EmailCouldNotSendException {
        Email from = new Email("Fubar@Carcassonne.com");
        String subject = "Please reset your Password";
        Content content = new Content("text/html", "<p>You can reset your password by clicking <a href=\"" + config.getBaseurl() + "/changePassword?email=" + email + "\">somewhere</a></p>");
        sendMail(from, subject, new Email(email), content);
        System.out.println("Reset email sent to " + email);
    }
}
