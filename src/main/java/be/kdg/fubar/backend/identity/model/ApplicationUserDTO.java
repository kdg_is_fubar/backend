package be.kdg.fubar.backend.identity.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ApplicationUserDTO {
    private String username;
    private String email;
    private String password;
    private int times_played;
    private int times_won;
    private int average_score;
    private int maximum_score;

    public ApplicationUserDTO(String username, String email, String password) {
        this.username = username;
        this.email = email;
        this.password = password;
    }
}
