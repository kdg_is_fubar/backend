package be.kdg.fubar.backend.identity.bl;

import be.kdg.fubar.backend.identity.dal.UserRepo;
import be.kdg.fubar.backend.identity.model.ApplicationUser;
import com.sendgrid.Email;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
public class UserService implements UserDetailsService {
    private final UserRepo userRepo;
    private EmailService emailService;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UserService(EmailService emailService, BCryptPasswordEncoder bCryptPasswordEncoder, UserRepo userRepo) {
        this.emailService = emailService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.userRepo = userRepo;
    }

    public ApplicationUser saveNewUser(ApplicationUser applicationUser) throws UserException {
        if (applicationUser == null) {
            throw new UserException("applicationUser was null");
        }
        try {
            loadUserByUsername(applicationUser.getUsername());
        } catch (UsernameNotFoundException e) {
            return userRepo.save(applicationUser);
        }
        throw new UserException("applicationUser already exsists");

    }

    public ApplicationUser loadUserByEmail(String email) throws UserException {
        Optional<ApplicationUser> user = userRepo.findApplicationUserByEmail(email);
        if (user.isPresent())
            return user.get();
        throw new UserException("User not found");
    }

    public ApplicationUser loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<ApplicationUser> user = userRepo.findUserByUsername(username);
        if (user.isPresent())
            return user.get();
        throw new UsernameNotFoundException(username + " has not been found.");
    }

    public ApplicationUser updateUser(ApplicationUser applicationUser) throws UsernameNotFoundException {
        ApplicationUser user = this.loadUserByUsername(applicationUser.getUsername());
        user.setEnabled(applicationUser.isEnabled());
        user.setEmail(applicationUser.getEmail());
        user.setPassword(applicationUser.getPassword());
        user.setPlayers(applicationUser.getPlayers());
        user.setNotifications(applicationUser.getNotifications());
        user.setTimes_won(applicationUser.getTimes_won());
        user.setTimes_played(applicationUser.getTimes_played());
        user.setMaximum_score(applicationUser.getMaximum_score());
        user.setAverage_score(applicationUser.getAverage_score());
        user.setFriends(applicationUser.getFriends());
        return userRepo.save(user);
    }

    public int deleteUser(String username) throws UserException {
        if (!username.isEmpty())
            return userRepo.deleteByUsername(username);
        throw new UserException("user was null");
    }

    public Boolean register(ApplicationUser applicationUser) throws UserException {
        applicationUser.setPassword(bCryptPasswordEncoder.encode(applicationUser.getPassword()));
        try {
            loadUserByUsername(applicationUser.getUsername());
            loadUserByEmail(applicationUser.getEmail());
        } catch (UserException | UsernameNotFoundException e) {
            applicationUser.makeVerificationToken();
            saveNewUser(applicationUser);
            try {
                emailService.sendRegistrationMail(new Email(applicationUser.getEmail()), applicationUser.getUsername(), applicationUser.getVerificationToken());
                System.out.println("email sent");
                return true;
            } catch (EmailCouldNotSendException e_email) {
                System.out.println("Unable to register applicationUser");
                System.out.println(e.getMessage());
                return false;
            }
        }
        return false;
    }

    public void changePassword(String email, String password) throws UserException {
        ApplicationUser user = loadUserByEmail(email);
        user.setPassword(bCryptPasswordEncoder.encode(password));
        updateUser(user);
        System.out.println("Password reset");
    }

    public void sendReset(String email) throws EmailCouldNotSendException, UserException {
        if (loadUserByEmail(email) != null) {
            emailService.sendResetMail(email);
        }
    }
}
