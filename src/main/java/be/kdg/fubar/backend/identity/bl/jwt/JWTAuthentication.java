package be.kdg.fubar.backend.identity.bl.jwt;

import be.kdg.fubar.backend.identity.config.JSONConfig;
import be.kdg.fubar.backend.identity.model.ApplicationUser;
import com.auth0.jwt.JWT;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import static com.auth0.jwt.algorithms.Algorithm.HMAC512;

/**
 * This class will handle the authentication of a user when they log into the service.
 * The request will be parsed into a ApplicationUser (Application ApplicationUser) object and seen if they can be found.
 * A Json Web Token (jwt) will be made with constants used from the IdentityConfig class if they have been found.
 *
 */
public class JWTAuthentication extends UsernamePasswordAuthenticationFilter {
    private JSONConfig JSONConfig;
    private AuthenticationManager authenticationManager;


    public JWTAuthentication(AuthenticationManager authenticationManager, JSONConfig JSONConfig) {
        this.authenticationManager = authenticationManager;
        this.JSONConfig = JSONConfig;
    }

    /**
     * Will take the credentials out of the request and ask the authenticationManager to authenticate the credentials with the service and the encoder given in the WebSecurity class.
     * Will give out an AuthenticationException (e.g. Bad Credentials Exception) when it is not found.
     * @param request
     * @param response
     * @return
     * @throws AuthenticationException
     */
    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        try {
            ApplicationUser creds = new ObjectMapper()
                    .readValue(request.getInputStream(), ApplicationUser.class);
            return authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            creds.getUsername(),
                            creds.getPassword(),
                            new ArrayList<>())
            );
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * On successful Authentication there will be a jwt attached to the request for the client to keep in localstorage.
     * @param request
     * @param response
     * @param chain
     * @param authResult
     * @throws IOException
     * @throws ServletException
     */
    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {

        String token = JWT.create()
                .withSubject(((ApplicationUser) authResult.getPrincipal()).getUsername())
                .withExpiresAt(new Date(System.currentTimeMillis() + Long.parseLong(JSONConfig.getExpirationtime())))
                .sign(HMAC512(JSONConfig.getSecret().getBytes()));

        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Methods", "*");
        response.setHeader("Access-Control-Allow-Headers", "content-type");
        response.setHeader("Access-Control-Expose-Headers", JSONConfig.getHeaderstring());
        response.setHeader("Access-Control-Allow-Credentials", "true");
        response.setHeader("Access-Control-Max-Age", "1728000");

        response.addHeader(JSONConfig.getHeaderstring(), JSONConfig.getTokenprefix() + ' ' + token);

    }
}
