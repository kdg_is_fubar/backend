package be.kdg.fubar.backend.identity.model;

import be.kdg.fubar.backend.friends.model.Friend;
import be.kdg.fubar.backend.notification.model.Notification;
import be.kdg.fubar.backend.player.model.Player;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.*;

/**
 * This class represents a user of the App and as such a Player of the game.
 */
@Entity
@Data
@AllArgsConstructor
public class ApplicationUser implements UserDetails {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter(AccessLevel.NONE)
    private Long id;
    private String username;
    private String email;
    private String password;
    private boolean enabled = false;
    private boolean accountNonExpired = true;
    private boolean accountNonLocked = true;
    private boolean credentialsNonExpired = true;
    // User Object Relations
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = {CascadeType.ALL}, targetEntity = Notification.class)
    private List<Notification> notifications;
    @OneToMany(cascade = CascadeType.ALL, targetEntity = Friend.class, fetch = FetchType.LAZY)
    private List<Friend> friends = new ArrayList<>();
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = {CascadeType.ALL}, targetEntity = Player.class)
    private List<Player> players;
    // User Simple Element Relations
    private boolean statsPublic = true;
    private int times_played = 0;
    private int times_won = 0;
    private int average_score = 0;
    private int maximum_score = 0;
    private String websocketSession;
    private String notificationToken = null;
    private String verificationToken;


    public ApplicationUser() {
        players = new ArrayList<>();
        notifications = new ArrayList<>();
    }

    public ApplicationUser(String username, String email, String password) {
        this();
        this.username = username;
        this.email = email;
        this.password = password;
        friends = new ArrayList<>();
        this.players = new ArrayList<>();
    }

    //wegens all-args constructor voordat statistic attributes erbij kwamen
    public ApplicationUser(List<Notification> notifications, String username, String email, String password, boolean enabled, String verificationToken, boolean accountNonExpired, boolean accountNonLocked, boolean credentialsNonExpired, List<Player> players) {
        this.notifications = notifications;
        this.username = username;
        this.email = email;
        this.password = password;
        this.enabled = enabled;
        this.verificationToken = verificationToken;
        this.accountNonExpired = accountNonExpired;
        this.accountNonLocked = accountNonLocked;
        this.credentialsNonExpired = credentialsNonExpired;
        this.players = players;
    }

    public void makeVerificationToken() {
        verificationToken = UUID.randomUUID().toString();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.emptyList();
    }

    @Override
    public boolean isAccountNonExpired() {
        return accountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return accountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return credentialsNonExpired;
    }

    public void updateTimesPlayed() {
        this.setTimes_played(this.getTimes_played() + 1);
    }

    public void updateTimesWon(boolean thePlayerWon) {
        if (thePlayerWon) this.setTimes_won(this.getTimes_won() + 1);

    }

    public void updateAverageScore(short score) {
        if (getTimes_played() == 0) {
            setAverage_score(score);
        } else {
            setAverage_score(getAverage_score() * getTimes_played() + score / (getTimes_played() + 1));
        }

    }

    public void updateMaximumScore(short score) {
        if (getMaximum_score() < score) setMaximum_score(score);
    }
}
