package be.kdg.fubar.backend.game.bl;

import be.kdg.fubar.backend.ai.bl.AIService;
import be.kdg.fubar.backend.flooding.bl.FloodingException;
import be.kdg.fubar.backend.flooding.bl.FloodingService;
import be.kdg.fubar.backend.flooding.bl.ScoreService;
import be.kdg.fubar.backend.follower.model.FollowerDTO;
import be.kdg.fubar.backend.identity.bl.UserService;
import be.kdg.fubar.backend.identity.model.ApplicationUser;
import be.kdg.fubar.backend.notification.bl.NotificationService;
import be.kdg.fubar.backend.notification.model.FIREBASEMESSAGETYPE;
import be.kdg.fubar.backend.player.PlayerException;
import be.kdg.fubar.backend.player.bl.PlayerService;
import be.kdg.fubar.backend.player.model.Player;
import be.kdg.fubar.backend.player.model.PlayerDTO;
import be.kdg.fubar.backend.replay.bl.ReplayService;
import be.kdg.fubar.backend.room.RoomException;
import be.kdg.fubar.backend.room.bl.RoomService;
import be.kdg.fubar.backend.room.model.GameStateDTO;
import be.kdg.fubar.backend.room.model.LeaderboardDTO;
import be.kdg.fubar.backend.room.model.Room;
import be.kdg.fubar.backend.room.model.TurnTimer;
import be.kdg.fubar.backend.round.bl.RoundService;
import be.kdg.fubar.backend.tile.TileException;
import be.kdg.fubar.backend.tile.bl.TileService;
import be.kdg.fubar.backend.tile.model.SurfaceType;
import be.kdg.fubar.backend.tile.model.Tile;
import be.kdg.fubar.backend.tile.model.TileDTO;
import be.kdg.fubar.backend.tile.model.TileSegmentPart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
public class GameService {
    private final RoomService roomService;
    private final TileService tileService;
    private final FloodingService floodingService;
    private final ScoreService scoreService;
    private final PlayerService playerService;
    private final SimpMessagingTemplate toSendMessage;
    private final UserService userService;
    private final RoundService roundService;
    private final NotificationService notificationService;
    private final ReplayService replayService;
    private final AIService aiService;
    private Random random;

    @Autowired
    public GameService(RoomService roomService,
                       TileService tileService,
                       FloodingService floodingService,
                       ScoreService scoreService,
                       PlayerService playerService,
                       SimpMessagingTemplate toSendMessage,
                       UserService userService,
                       RoundService roundService,
                       NotificationService notificationService,
                       ReplayService replayService,
                       AIService aiService) {
        this.roomService = roomService;
        this.tileService = tileService;
        this.floodingService = floodingService;
        this.scoreService = scoreService;
        this.playerService = playerService;
        this.toSendMessage = toSendMessage;
        this.userService = userService;
        this.roundService = roundService;
        this.notificationService = notificationService;
        this.random = new Random();
        this.replayService = replayService;
        this.aiService = aiService;
    }

    //region follower

    /**
     * This method will effectively place a follower on a tile for a player in a room
     * If this is successful the method will return true, else false.
     *
     * @param roomId   The room in which the follower is being placed in
     * @param playerId The player who is setting the Follower
     * @param relX     The X coordinate of the position where the Follower is being placed on
     * @param relY     The Y coordinate of the position where the Follower is being placed on
     * @return Will return true if it has been effectively placed
     * @throws PlayerException   An exception concerning the Player
     * @throws FloodingException An exception concerning the Flooding
     * @throws RoomException     An exception concerning the Room
     */
    public boolean placeFollower(Long roomId, Long playerId, int relX, int relY) throws PlayerException, FloodingException, RoomException, TileException {
        if (verifyFollower(roomId, playerId, relX, relY).isPresent()) {
            Room room = roomService.getRoom(roomId);
            Tile placedTile = room.getLastPlacedTile();
            TileSegmentPart part = tileService.getTileSegmentPartFromRelativeCoordinates(placedTile, relX, relY);
            boolean followerIsPlaced = playerService.placeFollower(playerId, part.getId());
            if (followerIsPlaced) {
                endTurn(room.getId());
                return true;
            }
        }
        return false;
    }

    /**
     * This method will delegate the Placing of the follower to the one with the playerId.
     *
     * @param roomId   The room in which the follower is being placed in
     * @param username The username of the player who is setting the Follower
     * @param relX     The X coordinate of the position where the Follower is being placed on
     * @param relY     The Y coordinate of the position where the Follower is being placed on
     * @return Will return true if it has been effectively placed
     * @throws PlayerException   An exception concerning the Player
     * @throws FloodingException An exception concerning the Flooding
     * @throws RoomException     An exception concerning the Room
     */
    public boolean placeFollower(Long roomId, String username, int relX, int relY) throws PlayerException, FloodingException, RoomException, TileException {
        ApplicationUser user = userService.loadUserByUsername(username);
        Optional<Player> optionalPlayer = playerService.getPlayerFromRoom(user, roomId);
        if (optionalPlayer.isPresent()) {
            Player player = optionalPlayer.get();
            return placeFollower(roomId, player.getId(), relX, relY);
        }
        return false;
    }

    /**
     * This method will verify if a Follower can be placed and if the tile which it is set upon is not occupied by another Follower
     *
     * @param roomId   The room which it is being placed in
     * @param playerId The player who is placing the follower
     * @param relX     The X position of the place it is being placed in
     * @param relY     The Y coordinate of the place it is being placed in
     * @return Will return an empty if it is not possible to place it else one with the type of the part it is set upon
     * (CITY/ROAD/FIELD/MONASTERY)
     * @throws PlayerException An exception concerning the Player
     * @throws RoomException   An exception concerning the Room
     */
    public Optional<SurfaceType> verifyFollower(Long roomId, Long playerId, int relX, int relY) throws PlayerException, RoomException, TileException {
        if (!canPlaceFollower(roomId, playerId)) {
            return Optional.empty();
        } else {
            Room room = roomService.getRoom(roomId);
            Tile placedTile = room.getLastPlacedTile();
            TileSegmentPart part = tileService.getTileSegmentPartFromRelativeCoordinates(placedTile, relX, relY);
            if (!scoreService.containsOtherFollower(room, playerId, part.getAdjecencyIdentifier())) {
                return Optional.of(part.getType());
            }
            return Optional.empty();
        }
    }

    /**
     * This method will delegate the verifyFollower method to the one that uses the PlayerId
     *
     * @param roomId   The room which it is being placed in
     * @param username The username of the player who is setting the Follower
     * @param relX     The X position of the place it is being placed in
     * @param relY     The Y coordinate of the place it is being placed in
     * @return Will return an empty if it is not possible to place it else one with the type of the part it is set upon
     * (CITY/ROAD/FIELD/MONASTERY)
     * @throws PlayerException An exception concerning the Player
     * @throws RoomException   An exception concerning the Room
     */
    public Optional<SurfaceType> verifyFollower(Long roomId, String username, int relX, int relY) throws PlayerException, RoomException, TileException {
        ApplicationUser user = userService.loadUserByUsername(username);
        Optional<Player> optionalPlayer = playerService.getPlayerFromRoom(user, roomId);
        if (optionalPlayer.isPresent()) {
            //Player has no zone to place a follower on
            Player player = optionalPlayer.get();
            return verifyFollower(roomId, player.getId(), relX, relY);
        }
        return Optional.empty();
    }

    /**
     * This method will see if a follower can be placed
     *
     * @param roomId   Which room the follower is being placed in
     * @param playerId Who is placing the follower
     * @return True if it can be placed, else false.
     * @throws PlayerException an exception concerning the player.
     */
    private boolean canPlaceFollower(Long roomId, Long playerId) throws PlayerException, RoomException {
        //No followers available
        if (playerService.getNumberOfFreeFollowers(playerId) == 0)
            return false;

        Room room = roomService.getRoom(roomId);

        //Valid placement left
        Set<Short> identifiers = floodingService.getAdjacencyIdentifiers(room.getBoard().get(room.getBoard().size() - 1));

        //Checking if any zone is free it returns true, and the player may place a follower.
        for (Short adjacencyIdentifier : identifiers) {
            if (!scoreService.containsOtherFollower(room, playerId, adjacencyIdentifier))
                return true;
        }

        //No zones were free, the player may not place a follower
        return false;
    }
    //endregion

    //region tile

    /**
     * This method will place a tile in a room for a human Player.
     *
     * @param tile to be added to the baord
     * @return whether the tile has been added
     */
    public boolean placeTile(Long roomId, Tile tile) throws FloodingException, RoomException {
        Room room = roomService.getRoom(roomId);
        room.getCurrentPlayer().setPlaced(true);
        if (isPlaceFree(room, tile.getX(), tile.getY()) && isNextToTile(room, tile.getX(), tile.getY())) {
            Optional<Tile> foundTileOptional = tileService.getTileByName(tile.getName());
            if (!foundTileOptional.isPresent())
                throw new RoomException("There is no tile with name " + tile.getName());
            Tile foundTile = foundTileOptional.get();
            foundTile.setCoords(tile.getX(), tile.getY());
            foundTile.turnRight(tile.getRotations());
            tile = foundTile;
            if (tileService.matchTile(room.getBoard(), tile)) {
                room.getBoard().add(tile);
                room.setLastPlacedTile(tile); //saves last placed tile for later checks
                floodingService.flood(room, tile.getX(), tile.getY());
                roomService.saveRoom(room);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * A method to see if the coordinate which the tile is being placed is free.
     *
     * @param room which room the tile has been place in
     * @param x    the X coordinate of the placed tile
     * @param y    the Y coordinate of the placed tile
     * @return will return true if the place is free, else false
     */
    private boolean isPlaceFree(Room room, int x, int y) {
        return room.getBoard().stream().noneMatch(t -> t.getX() == x && t.getY() == y);
    }

    /**
     * A method to see if the coordinate which the tile is next to another tile.
     *
     * @param room which room the tile has been place in
     * @param x    the X coordinate of the placed tile
     * @param y    the Y coordinate of the placed tile
     * @return will return true if the place is next to another, else false
     */
    private boolean isNextToTile(Room room, int x, int y) {
        return room.getBoard().stream().anyMatch(t ->
                t.getX() == (x - 1) && t.getY() == y ||
                        t.getX() == (x) && t.getY() == y - 1 ||
                        t.getX() == (x + 1) && t.getY() == y ||
                        t.getX() == (x) && t.getY() == y + 1
        );
    }

    /**
     * Will draw a tile in a certain room automatically, the drawn tile will be sent to the current player via user based websockets.
     *
     * @param roomId The room in which the tile has been drawn from
     * @return the updated room without the drawn tile in the deck.
     */
    public Room drawTileFromDeck(Long roomId) throws RoomException {
        Room room = roomService.getRoom(roomId);
        List<Tile> deck = room.getDeck();
        if (!deck.isEmpty()) {
            Tile t = deck.get(random.nextInt(deck.size()));
            room.getCurrentPlayer().setDrawnTile(t);
            deck.remove(t);
            playerService.save(room.getCurrentPlayer());
            toSendMessage.convertAndSendToUser(room.getCurrentPlayer().getUsername(), "/hand", new TileDTO(t.getId(), t.getX(), t.getY(), t.getRotations(), t.isStartTile(), t.getName(), new FollowerDTO(t)));
            return roomService.saveRoom(room);
        } else {
            throw new RoomException("drew from empty deck");
        }
    }
    //endregion

    //region board

    /**
     * This method will send the next Gamestate to everyone in a room
     *
     * @param roomId The room whoise boardstate will be converted into a GameStateDTO
     * @throws RoomException an exception concerning the Rooms
     */
    public void sendUpdatedBoard(Long roomId) throws RoomException {
        Room room = roomService.getRoom(roomId);
        TileDTO[] tileDTOS = this.getBoard(room.getId())
                .stream()
                .map(tile -> (
                        new TileDTO(
                                tile.getId(),
                                tile.getX(),
                                tile.getY(),
                                tile.getRotations(),
                                tile.isStartTile(),
                                tile.getName(),
                                new FollowerDTO(tile))
                ))
                .toArray(TileDTO[]::new);
        PlayerDTO[] playerDTOS = room.getPlayers().stream().map(playerService::convertToDTO).toArray(PlayerDTO[]::new);
        GameStateDTO gameStateDTO = new GameStateDTO(tileDTOS, room.getName(), playerDTOS, room.getCurrentPlayer().getUsername(), room.getCurrentPlayer().getDrawnTile(), room.isEnded());

        toSendMessage.convertAndSend("/game/" + room.getId(), gameStateDTO);
    }

    public List<Tile> getBoard(Long roomId) throws RoomException {
        return roomService.getRoom(roomId).getBoard();
    }
    //endregion

    //region round

    /**
     * This method will end the turn in a room
     *
     * @param roomId The room in which the turn is being ended in
     * @throws FloodingException An exception concerning the Flooding
     * @throws RoomException     An exception concerning the Rooms
     */
    public void endTurn(Long roomId) throws FloodingException, RoomException {
        Room room = roomService.getRoom(roomId);
        scoreService.distributeScore(room.getBoard(), room.getLastPlacedTile());
        room = roundService.endRound(room);
        room = roomService.saveRoom(room);
        if (room.getDeck().size() == 0 && !room.isEnded()) {
            endGame(room.getId());
            this.sendUpdatedBoard(room.getId());
        } else {
            room = roomService.saveRoom(room);
            this.notifyNextPlayer(room.getId());
            this.sendUpdatedBoard(room.getId());
            room = drawTileFromDeck(room.getId());
            Pattern p = Pattern.compile("AI[0-7]");
            Matcher m = p.matcher(room.getCurrentPlayer().getUsername());
            if (m.find()) {
                rescheduleTimer(room, 1000);
            } else {
                rescheduleTimer(room, room.getTurnTime());
            }
        }
    }

    /**
     * This method will notify the next player in the room that it is his turn
     *
     * @param roomId The room whose next player is being notified
     * @throws RoomException an exception concerning the Rooms
     */
    public void notifyNextPlayer(Long roomId) throws RoomException {
        Room room = roomService.getRoom(roomId);
        String user = room.getCurrentPlayer().getUsername();
        notificationService.sendNotifications(user, "It's your turn in " + room.getName(), room, FIREBASEMESSAGETYPE.USER);
    }

    /**
     * This method will start the game
     *
     * @param roomId   The room which will be started
     * @param username The player who has initiated the request
     * @throws RoomException An exception concerning Rooms
     */
    public void startGame(Long roomId, String username) throws RoomException {
        Room room = roomService.getRoom(roomId);
        room = roundService.setupGame(room);
        roomService.updateRoom(room);
        room = drawTileFromDeck(room.getId());
        scheduleTimer(room, room.getTurnTime());
        roomService.updateRoom(room);
    }

    /**
     * This method will end the game.
     *
     * @param roomId The room that has just ended
     * @throws FloodingException An exception concerning the Flooding
     * @throws RoomException     An exception concerning the Room
     */
    public void endGame(Long roomId) throws FloodingException, RoomException {
        Room room = roomService.getRoom(roomId);
        scoreService.setFieldComplete(room.getBoard());
        scoreService.distributeScore(room.getBoard(), room.getLastPlacedTile());
        room.setPlayersScore(new ArrayList<>());
        for (Player player : room.getPlayers()) {
            room.getPlayersScore().add(player.getScore());
        }
        addUserScore(roomId);
        room.setEnded(true);
        TurnTimer timer = room.getTimer();
        if (timer != null) {
            room.getTimer().cancel();
            room.getTimer().purge();
        }
        replayService.createReplay(room);
        roomService.saveRoom(room);
    }

    /**
     * Will schedule a timer in a room.
     *
     * @param r    The room which the timer will be set for
     * @param time the delay for the timer
     */
    public void scheduleTimer(Room r, long time) {
        r.setTimer(new TurnTimer());
        r.getTimer().schedule(new TimerTask() {
            @Override
            public void run() {
                try {
                    final Room room = roomService.getRoom(r.getId());
                    aiService.executeTurn(room, room.getCurrentPlayer(), room.getCurrentPlayer().getDrawnTile(), true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, time);
    }

    /**
     * This method will cancel the previous timer and reschedule the timer for the room
     *
     * @param room The room which timer will be rescheduled
     * @param time The delay for the timer
     */
    public void rescheduleTimer(Room room, long time) throws RoomException {
        if (room.getTimer() != null) {
            room.getTimer().cancel();
            room.getTimer().purge();
        }
        scheduleTimer(room, time);
        roomService.updateRoom(room);
    }

    /**
     * This method will determine the winner of the game
     *
     * @param id           The roomid of the room that has just ended
     * @param toTestPlayer The Player that is being tested if he has won
     * @return It will say if the player that is being tested is the winner (true) or not (false)
     * @throws RoomException An exception concerning Rooms
     */
    private boolean determineWinner(Long id, Player toTestPlayer) throws RoomException {
        List<Player> players = new ArrayList<>(roomService.getRoom(id).getPlayers());
        Player winner = players.get(0);
        for (Player player : players) {
            if (player.getScore() > winner.getScore()) {
                winner = player;
            }
        }
        return winner.equals(toTestPlayer);
    }

    /**
     * This method will update the stats of player
     *
     * @param id The room whose stats will be used to update the stats of the ApplicationUser
     * @throws RoomException An exception concerning the Room
     */
    private void addUserScore(Long id) throws RoomException {
        List<Player> players = new ArrayList<>(roomService.getRoom(id).getPlayers());
        for (Player player : players) {
            ApplicationUser applicationUser = userService.loadUserByUsername(player.getUsername());
            applicationUser.updateAverageScore(player.getScore());
            applicationUser.updateTimesPlayed();
            applicationUser.updateTimesWon(determineWinner(id, player));
            applicationUser.updateMaximumScore(player.getScore());
        }
    }
    //endregion

    /**
     * Compiles a leaderboard into a DTO object.
     *
     * @param roomId The room to get leaderboard from.
     * @throws RoomException Throws exception if the room can't be found.
     */
    public LeaderboardDTO getLeaderboard(Long roomId) throws RoomException {
        Room room = roomService.getRoom(roomId);
        return new LeaderboardDTO(room
                .getPlayers()
                .stream()
                .map(Player::getUsername)
                .collect(Collectors.toList()), room.getPlayersScore());
    }
}
