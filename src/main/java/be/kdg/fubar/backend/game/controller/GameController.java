package be.kdg.fubar.backend.game.controller;

import be.kdg.fubar.backend.flooding.bl.FloodingException;
import be.kdg.fubar.backend.follower.model.FollowerDTO;
import be.kdg.fubar.backend.game.bl.GameService;
import be.kdg.fubar.backend.identity.util.UserUtil;
import be.kdg.fubar.backend.player.PlayerException;
import be.kdg.fubar.backend.room.RoomException;
import be.kdg.fubar.backend.room.model.LeaderboardDTO;
import be.kdg.fubar.backend.tile.TileException;
import be.kdg.fubar.backend.tile.model.SurfaceType;
import be.kdg.fubar.backend.tile.model.Tile;
import be.kdg.fubar.backend.tile.model.TileDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RequestMapping("game")
@RestController
public class GameController {
    private final GameService gameService;
    private final UserUtil userUtil;
    private final SimpMessagingTemplate toSendMessage;

    @Autowired
    public GameController(GameService gameService, UserUtil userUtil, SimpMessagingTemplate simp) {
        this.gameService = gameService;
        this.userUtil = userUtil;
        toSendMessage = simp;
    }

    /**
     * Endpoint for placing a tile. Will be called during a player's turn.
     *
     * @param roomId
     * @param tile
     * @return
     */
    @PostMapping("/{roomId}/placetile")
    public boolean placeTile(@PathVariable("roomId") Long roomId, @RequestBody Tile tile) throws FloodingException, RoomException {
        tile.turnRight(tile.getRotations());
        boolean resolved = gameService.placeTile(roomId, tile);
        if (resolved)
            gameService.sendUpdatedBoard(roomId);
        return resolved;
    }

    /**
     * Endpoint for placing a follower;
     *
     * @param roomId
     * @param followerDTO
     * @return
     */
    @PostMapping("/{roomId}/placefollower")
    public boolean placeFollower(@PathVariable("roomId") Long roomId, @RequestBody FollowerDTO followerDTO, @RequestHeader("Authorization") String token) throws RoomException, FloodingException, PlayerException, TileException {
        return gameService.placeFollower(roomId, userUtil.getUserByToken(), followerDTO.getRelativeX(), followerDTO.getRelativeY());
    }

    /**
     * Endpoint for verifying if a follower can be placed on a specific place
     *
     * @param roomId
     * @param followerDTO
     * @return
     */
    @PostMapping("/{roomId}/verifyfollower")
    public Optional<SurfaceType> verifyFollower(@PathVariable("roomId") Long roomId, @RequestBody FollowerDTO followerDTO, @RequestHeader("Authorization") String token) throws RoomException, PlayerException, TileException {
        return gameService.verifyFollower(roomId, userUtil.getUserByToken(), followerDTO.getRelativeX(), followerDTO.getRelativeY());
    }

    /**
     * Getter for a room, this will be called when a player enters a room.
     *
     * @param roomId
     * @return
     */
    @GetMapping("/{roomId}/board")
    public TileDTO[] getBoard(@PathVariable("roomId") Long roomId) throws RoomException {
        return gameService.getBoard(roomId)
                .stream()
                .map(tile -> (
                        new TileDTO(
                                tile.getId(),
                                tile.getX(),
                                tile.getY(),
                                tile.getRotations(),
                                tile.isStartTile(),
                                tile.getName(),
                                new FollowerDTO(tile))
                ))
                .toArray(TileDTO[]::new);
    }

    @GetMapping("/{roomId}/endturn")
    public Boolean endTurn(@PathVariable("roomId") Long roomId) throws RoomException, FloodingException {
        gameService.endTurn(roomId);
        return true;
    }

    /**
     * Websocket endpoint for starting a game for a room
     * only the host can start a game.
     *
     * @param roomId
     */
    @MessageMapping("/game/{roomId}/start")
    public void startGame(@DestinationVariable Long roomId, @RequestBody String username, @RequestHeader("Authorization") String token) throws RoomException {
        gameService.startGame(roomId, username);
        toSendMessage.convertAndSend("/game/" + roomId, "game start");
    }

    /**
     * Compiling a leaderboard.
     *
     * @param roomId
     */
    @GetMapping("/{roomId}/leaderboard")
    public LeaderboardDTO leaderboard(@PathVariable("roomId") Long roomId) throws RoomException {
        return gameService.getLeaderboard(roomId);

    }
}
