package be.kdg.fubar.backend.follower.dal;

import be.kdg.fubar.backend.follower.model.Follower;
import org.springframework.data.jpa.repository.JpaRepository;
public interface FollowerRepository  extends JpaRepository<Follower, Long>{
}
