package be.kdg.fubar.backend.follower.model;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Data
@Entity
@Table(name = "Follower")
@NoArgsConstructor
public class Follower {
    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter(AccessLevel.NONE)
    private Long id;

    private Long playerId;

    public Follower(Long playerId) {
        this.playerId = playerId;
    }

    private boolean placed = false;
}
