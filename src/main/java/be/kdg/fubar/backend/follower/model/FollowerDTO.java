package be.kdg.fubar.backend.follower.model;

import be.kdg.fubar.backend.tile.model.Tile;
import be.kdg.fubar.backend.tile.model.TileSegmentPart;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class FollowerDTO {

    private boolean placed = false;

    private byte relativeX;
    private byte relativeY;


    public FollowerDTO(Tile tile) {
        TileSegmentPart[][][][] matrix = tile.toMatrix();
        for (int x = 0; x < 3; x++) {
            for (int y = 0; y < 3; y++) {
                for (int x2 = 0; x2 < 3; x2++) {
                    for (int y2 = 0; y2 < 3; y2++) {
                        TileSegmentPart tileSegmentPart = matrix[x][y][x2][y2];
                        if (tileSegmentPart.getFollower() != null) {
                            this.relativeX = (byte) (x * 3 + x2);
                            this.relativeY = (byte) (y * 3 + y2);
                            this.placed = true;
                        }
                    }
                }
            }
        }
    }
}
