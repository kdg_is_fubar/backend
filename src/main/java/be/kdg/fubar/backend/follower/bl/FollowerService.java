package be.kdg.fubar.backend.follower.bl;

import be.kdg.fubar.backend.follower.dal.FollowerRepository;
import be.kdg.fubar.backend.follower.model.Follower;
import be.kdg.fubar.backend.tile.TileException;
import be.kdg.fubar.backend.tile.bl.TileService;
import be.kdg.fubar.backend.tile.model.TileSegmentPart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class FollowerService {
    private final FollowerRepository followerRepository;
    private final TileService tileService;

    @Autowired
    public FollowerService(FollowerRepository followerRepository, TileService tileService) {
        this.followerRepository = followerRepository;
        this.tileService = tileService;
    }

    public Follower getFollower(Long id) {
        Optional<Follower> optionalFollower = followerRepository.findById(id);
        if (optionalFollower.isPresent()) {
            return optionalFollower.get();
        } else {
            throw new RuntimeException("Invalid FollowerID"); // TODO custom Exception
        }
    }


    public Follower save(Follower follower) {
        return followerRepository.save(follower);
    }

    public void placeFollower(Long partId, Follower follower) throws TileException {
        TileSegmentPart part = tileService.getTileSegmentPart(partId);
        part.setFollower(follower);
        follower.setPlaced(true);
        tileService.saveTileSegmentPart(part);
    }
}
