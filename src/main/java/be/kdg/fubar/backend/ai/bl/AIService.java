package be.kdg.fubar.backend.ai.bl;

import be.kdg.fubar.backend.flooding.bl.FloodingException;
import be.kdg.fubar.backend.game.bl.GameService;
import be.kdg.fubar.backend.identity.bl.UserException;
import be.kdg.fubar.backend.player.PlayerException;
import be.kdg.fubar.backend.player.bl.PlayerService;
import be.kdg.fubar.backend.player.model.Player;
import be.kdg.fubar.backend.room.RoomException;
import be.kdg.fubar.backend.room.bl.RoomService;
import be.kdg.fubar.backend.room.model.Room;
import be.kdg.fubar.backend.tile.TileException;
import be.kdg.fubar.backend.tile.model.Coordinate;
import be.kdg.fubar.backend.tile.model.Tile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class AIService {
    @Lazy
    @Autowired
    private GameService gameService;
    @Lazy
    @Autowired
    private RoomService roomService;
    private final PlayerService playerService;
    private Random random;

    @Autowired
    public AIService(PlayerService playerService) {
        this.playerService = playerService;
        this.random = new Random();
    }

    /**
     * This will add AI to a room
     * @param room which room the AI will be added in
     * @param amount How many AIs will be added
     * @throws UserException
     */
    public void addAI(Room room, int amount) throws UserException {
        if (amount > 8)
            amount = 8;
        for (int i = 0; i < amount; i++) {
            playerService.creatNewPlayer("AI" + i, room);
        }
    }

    /**
     * Will execute a turn as the AI, is used for the Timer and for when the AI plays.
     * @param room which room the action is taking place in
     * @param player as which player it will play
     * @param t which tile it will use during the turn
     * @param isExpired if action is because it has expired.
     */
    public void executeTurn(Room room, Player player, Tile t, boolean isExpired) throws FloodingException, RoomException, PlayerException, TileException {
        // If the turn has been expired and the tile has been placed
        Room checkingRoom = roomService.getRoom(room.getId());
        roomService.updateRoom(checkingRoom);
    if (!checkingRoom.getCurrentPlayer().isPlaced() && t != null)
    {
        placeTile(checkingRoom, t, isExpired);
    }
        if (!placeFollower(checkingRoom, player))
            gameService.endTurn(checkingRoom.getId());
    }

    /**
     * This will draw a tile if the AI cannot place the given tile.
     * @param roomId Which rooom the tile will be drawn from
     * @return The tile that has been drawn will be returned
     */
    private Tile AIdraw(Long roomId) throws RoomException {
        Room room = roomService.getRoom(roomId);
        List<Tile> deck = room.getDeck();
        if (!deck.isEmpty()) {
            Tile t = deck.get(random.nextInt(deck.size()));
            room.getCurrentPlayer().setDrawnTile(t);
            deck.remove(t);
            playerService.save(room.getCurrentPlayer());
            roomService.saveRoom(room);
            return t;
        } else {
            throw new RuntimeException("drew from empty deck");
        }
    }

    /**
     * This will place a tile in a certain room as the AI
     * @param room Which room the AI will place the tile in
     * @param tile Which tile has been given tot the AI to place.
     * @param isExpired If this is initiated because of the timer expiring or not.
     */
    private void placeTile(Room room, Tile tile, boolean isExpired) throws FloodingException, RoomException {
        room = roomService.getRoom(room.getId());
        if (!isExpired || tile == null || room.getCurrentPlayer().getDrawnTile() == null) {
            tile = AIdraw(room.getId());
        } else {
            tile = room.getCurrentPlayer().getDrawnTile();
        }
        byte rotation = 0;
        do {
            Queue<Coordinate> possibleSpots = new PriorityQueue<>(room.getEmptyNeighbours());
            do {
                Coordinate coordinate = possibleSpots.poll();
                tile.setX(coordinate.getX());
                tile.setY(coordinate.getY());
                tile.setRotations(rotation);
                boolean succeeded = gameService.placeTile(room.getId(), tile);
                if (succeeded)
                    return;
            } while (possibleSpots.size() > 0);
            rotation++;
        } while (rotation < 4);
        placeTile(room,null,false);
    }

    /**
     * This will place a follower in a room as the AI.
     * @param room which room the follower will be placed in
     * @param player Which player the placed follower will be taken from.
     * @return
     */
    private boolean placeFollower(Room room, Player player) throws RoomException, FloodingException, PlayerException, TileException {
        if (random.nextInt(3) < 2)
            return false;
        List<Coordinate> parts = new ArrayList<>();
        for (int x = 0; x < 9; x++) {
            for (int y = 0; y < 9; y++) {
                parts.add(new Coordinate(x, y));
            }
        }
        Collections.shuffle(parts);
        for (Coordinate coordinate : parts) {
            if (gameService.placeFollower(room.getId(), player.getId(), coordinate.getX(), coordinate.getY()))
                return true;
        }
        return false;
    }
}
