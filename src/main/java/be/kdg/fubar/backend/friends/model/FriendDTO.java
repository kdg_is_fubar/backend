package be.kdg.fubar.backend.friends.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FriendDTO {
    private String currentUsername;
    private String friendUsername;
}
