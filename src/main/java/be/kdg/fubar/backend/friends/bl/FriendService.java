package be.kdg.fubar.backend.friends.bl;

import be.kdg.fubar.backend.friends.FriendException;
import be.kdg.fubar.backend.friends.model.Friend;
import be.kdg.fubar.backend.identity.bl.UserException;
import be.kdg.fubar.backend.identity.bl.UserService;
import be.kdg.fubar.backend.identity.model.ApplicationUser;
import be.kdg.fubar.backend.notification.bl.NotificationService;
import be.kdg.fubar.backend.notification.model.FIREBASEMESSAGETYPE;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class FriendService {

    private NotificationService notificationService;
    private final UserService userService;

    @Autowired
    public FriendService(NotificationService notificationService, UserService userService) {
        this.notificationService = notificationService;
        this.userService = userService;
    }

    /**
     * This method will add a friend request to the concerning user.
     * @param currentUsername Who has sent the invite
     * @param friendUsername Who has been sent the invite
     * @return Will return the list of friends back to the person who has sent the invite
     * @throws FriendException An exception that happened concerning a Friend
     * @throws UserException An exception that happened concerning a User
     */
    public List<Friend> addFriend(String currentUsername, String friendUsername) throws FriendException, UserException {
        if (currentUsername.equalsIgnoreCase(friendUsername))
            throw new FriendException("You can't add yourself");

        ApplicationUser main = userService.loadUserByUsername(friendUsername);

        if (userService.loadUserByUsername(friendUsername) == null) {
            throw new FriendException("Friend does not have an account");
        }
        if (main.getFriends().stream().noneMatch(x -> x.getUsername().equalsIgnoreCase(currentUsername))) {
            main.getFriends().add(new Friend(currentUsername, true));
            userService.updateUser(main);
        }

        notificationService.sendNotifications(friendUsername, "you have a new friend request.", null, FIREBASEMESSAGETYPE.USER);
        return getFriends(currentUsername);
    }

    /**
     * This method will delete a friend from someone's friends list.
     * @param currentUsername Who has sent the deletion request, the friend will be deleted from his friends list
     * @param friendUsername The user who has been deleted, the requester will be deleted from his friends list
     * @return Will return the list of friends back to the person who has sent the deletion request
     * @throws FriendException An exception that happened concerning a Friend
     * @throws UserException An exception that happened concerning a User
     */
    public List<Friend> deleteFriend(String currentUsername, String friendUsername) throws FriendException, UserException {
        if (currentUsername.equalsIgnoreCase(friendUsername))
            throw new FriendException("You cant remove yourself");

        ApplicationUser currentUser = userService.loadUserByUsername(currentUsername);
        ApplicationUser friendToRemove = userService.loadUserByUsername(friendUsername);

        if (friendToRemove == null) {
            throw new FriendException("Friend does not have an account");
        }

        Optional<Friend> toDeleteFriend = currentUser.getFriends().stream().filter(x -> x.getUsername().equals(friendUsername)).findFirst();
        Optional<Friend> toDeleteCurrent = friendToRemove.getFriends().stream().filter(x -> x.getUsername().equals(currentUsername)).findFirst();

        if (!toDeleteFriend.isPresent() || !toDeleteCurrent.isPresent()) {
            throw new FriendException("Friend was never asked.");
        }

        currentUser.getFriends().remove(toDeleteFriend.get());
        friendToRemove.getFriends().remove(toDeleteCurrent.get());

        userService.updateUser(currentUser);
        userService.updateUser(friendToRemove);


        return getFriends(currentUsername);
    }
    /**
     * This method will accept a friend request and add them to each other's friends list.
     * @param currentUsername Who has sent the acceptation
     * @param friendUsername Who has been accepted
     * @return Will return a the list of friends back to the person who has sent the acceptation
     * @throws FriendException An exception that happened concerning a Friend
     * @throws UserException An exception that happened concerning a User
     */
    public List<Friend> acceptFriend(String currentUsername, String friendUsername) throws FriendException, UserException {
        if (currentUsername.equalsIgnoreCase(friendUsername))
            throw new FriendException("Can't accept yourself");

        ApplicationUser asked = userService.loadUserByUsername(currentUsername);
        ApplicationUser asking = userService.loadUserByUsername(friendUsername);

        Friend invite = getInvite(asked, asking);

        asked.getFriends().get(asked.getFriends().indexOf(invite)).setPending(false);

        //add asking user to asked's friendslist
        asking.getFriends().add(new Friend(currentUsername,false));
        userService.updateUser(asking);
        userService.updateUser(asked);


        notificationService.sendNotifications(friendUsername, currentUsername + " has accepted your friend request.", null, FIREBASEMESSAGETYPE.USER);
        return getFriends(currentUsername);
    }

    /**
     * This method will refuse a friend request.
     * @param currentUsername Who has refused the request, pending invite will be deleted
     * @param friendUsername User who has been refused
     * @return Will return a the list of friends back to the person who has sent the refusal
     * @throws FriendException An exception that happened concerning a Friend
     * @throws UserException An exception that happened concerning a User
     */
    public List<Friend> refuseFriend(String currentUsername, String friendUsername) throws FriendException, UserException {
        if (currentUsername.equalsIgnoreCase(friendUsername))
            throw new FriendException("You can't refuse yourself");

        ApplicationUser asked = userService.loadUserByUsername(currentUsername);
        ApplicationUser asking = userService.loadUserByUsername(friendUsername);

        Friend invite = getInvite(asked, asking);
        asked.getFriends().remove(invite);

        userService.updateUser(asking);
        userService.updateUser(asked);

        return getFriends(friendUsername);
    }

    public List<Friend> getFriends(String currentUsername) {
        return userService.loadUserByUsername(currentUsername).getFriends();
    }

    /**
     * Will return the friend request between two people.
     * @param asked Who is concerned in this request.
     * @param asking Who has initiated the request
     * @return Will return the concerning Friend Object.
     * @throws FriendException An exception that happened concerning a Friend
     */
    private Friend getInvite(ApplicationUser asked, ApplicationUser asking) throws FriendException {
        if (asking == null ||asked == null)
            throw new FriendException("Friend does not have an account");

        Optional<Friend> invite = asked.getFriends().stream().filter(x -> x.getUsername().equals(asking.getUsername())).findFirst();
        if (!invite.isPresent())
            throw new FriendException("Friend was never asked");
        return invite.get();
    }
}
