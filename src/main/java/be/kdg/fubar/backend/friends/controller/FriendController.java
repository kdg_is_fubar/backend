package be.kdg.fubar.backend.friends.controller;

import be.kdg.fubar.backend.friends.FriendException;
import be.kdg.fubar.backend.friends.bl.FriendService;
import be.kdg.fubar.backend.friends.model.Friend;
import be.kdg.fubar.backend.friends.model.FriendDTO;
import be.kdg.fubar.backend.identity.bl.UserException;
import be.kdg.fubar.backend.identity.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//TODO MOVE LOGIC TO BL
@RequestMapping("/friend")
@RestController
public class FriendController {

    private FriendService friendService;
    private final UserUtil userUtil;

    @Autowired
    public FriendController(FriendService friendService, UserUtil userUtil) {
        this.friendService = friendService;
        this.userUtil = userUtil;
    }

    @GetMapping("/getall")
    public ResponseEntity<List<Friend>> getAllFriends() {
        return new ResponseEntity<>(friendService.getFriends(userUtil.getUserByToken()), HttpStatus.OK);
    }

    @PostMapping("/accept")
    public ResponseEntity<List<Friend>> acceptFriend(@RequestBody FriendDTO friendDTO) throws FriendException, UserException {
        return new ResponseEntity<>(friendService.acceptFriend(friendDTO.getCurrentUsername(), friendDTO.getFriendUsername()), HttpStatus.OK);
    }

    @PostMapping("/refuse")
    public ResponseEntity<List<Friend>> refuseFriend(@RequestBody FriendDTO friendDTO) throws FriendException, UserException {
        return new ResponseEntity<>(friendService.refuseFriend(friendDTO.getCurrentUsername(), friendDTO.getFriendUsername()), HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<List<Friend>> addFriend(@RequestBody FriendDTO friendDTO) throws FriendException, UserException {
        return new ResponseEntity<>(friendService.addFriend(friendDTO.getCurrentUsername(), friendDTO.getFriendUsername()), HttpStatus.OK);
    }

    @PostMapping("/delete")
    public ResponseEntity<List<Friend>> deleteFriend(@RequestBody FriendDTO friendDTO) throws FriendException, UserException {
        return new ResponseEntity<>(friendService.deleteFriend(friendDTO.getCurrentUsername(), friendDTO.getFriendUsername()), HttpStatus.OK);
    }
}
