package be.kdg.fubar.backend.friends;

public class FriendException extends Exception {
    public FriendException(String error) {
        super(error);
    }
}
