package be.kdg.fubar.backend.chat.controller;

import be.kdg.fubar.backend.chat.ChatDTO;
import be.kdg.fubar.backend.room.bl.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ChatController {
    @Autowired
    private RoomService roomService;
    @Autowired
    private SimpMessagingTemplate toSendMessage;
    /**
     * Websocket endpoint for chat
     * Will post to anyone subscribed to the template:
     * /chat/{roomid}
     * Will be activated when sent a message to:
     * /rooms/chat/{roomId}
     */
    @MessageMapping("/chat/{roomId}")
    public void chat(ChatDTO chatDTO, @DestinationVariable String roomId) {
        System.out.println("test");
        toSendMessage.convertAndSend("/chat/" + roomId, chatDTO.getFrom() + ": " + chatDTO.getBody());
    }
}
