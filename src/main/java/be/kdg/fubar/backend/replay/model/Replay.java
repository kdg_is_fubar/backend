package be.kdg.fubar.backend.replay.model;

import be.kdg.fubar.backend.room.model.Room;
import be.kdg.fubar.backend.tile.model.Tile;
import lombok.*;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Table(name = "Replay")
@Entity
@NoArgsConstructor
public class Replay {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter(AccessLevel.NONE)
    private Long id;
    @Column
    private int moveNumber;
    @OneToOne(cascade = {CascadeType.REMOVE}, targetEntity = Room.class)
    private Room room;
    @OneToMany(cascade = {CascadeType.ALL}, targetEntity = Tile.class)
    private List<Tile> ownBoardState; //needs to be copied

    public Replay(Room room) {
        this.room=room;
        moveNumber=0;
        this.ownBoardState=new ArrayList<>();
    }

    public boolean addTile(Tile tile) {
        return ownBoardState.add(new Tile(tile));
    }
}
