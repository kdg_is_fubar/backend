package be.kdg.fubar.backend.replay.dal;

import be.kdg.fubar.backend.replay.model.Replay;
import be.kdg.fubar.backend.room.model.Room;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReplayRepository extends JpaRepository<Replay, Long> {
}
