package be.kdg.fubar.backend.replay.controller;

import be.kdg.fubar.backend.flooding.bl.FloodingException;
import be.kdg.fubar.backend.follower.model.FollowerDTO;
import be.kdg.fubar.backend.identity.util.UserUtil;
import be.kdg.fubar.backend.replay.bl.ReplayService;
import be.kdg.fubar.backend.replay.exception.ReplayException;
import be.kdg.fubar.backend.replay.model.Replay;
import be.kdg.fubar.backend.replay.model.ReplayDTO;
import be.kdg.fubar.backend.tile.model.TileDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/replay")
public class ReplayController {
    private final ReplayService replayService;
    private final UserUtil userUtil;
    @Autowired
    public ReplayController(ReplayService replayService, UserUtil userUtil) {
        this.replayService = replayService;
        this.userUtil = userUtil;
    }
    @GetMapping("/{replayId}/next")
    public TileDTO[] getNext(@PathVariable("replayId") Long replayId) throws ReplayException, FloodingException {
       return replayService.nextMove(replayId).stream()
                .map(tile -> (
                        new TileDTO(
                                tile.getId(),
                                tile.getX(),
                                tile.getY(),
                                tile.getRotations(),
                                tile.isStartTile(),
                                tile.getName(),
                                new FollowerDTO(tile))
                ))
                .toArray(TileDTO[]::new);
    }
    @GetMapping("/{replayID}/reset")
    public TileDTO[] reset(@PathVariable("replayID") Long replayID) throws ReplayException {
        replayService.reset(replayID);
        return replayService.get(replayID).getOwnBoardState().stream()
                .map(tile -> (
                        new TileDTO(
                                tile.getId(),
                                tile.getX(),
                                tile.getY(),
                                tile.getRotations(),
                                tile.isStartTile(),
                                tile.getName(),
                                new FollowerDTO(tile))
                ))
                .toArray(TileDTO[]::new);
    }
    @GetMapping("/all")
    public List<ReplayDTO> getAll(@RequestHeader("Authorization") String token){
        String username = this.userUtil.getUserByToken();
        return replayService.getAll(username).stream().map(replayService::replayToDTO).collect(Collectors.toList());
    }

    @GetMapping("/{replayID}")
    public Replay getReplay(@PathVariable("replayID") Long replayId) throws ReplayException {
        return replayService.get(replayId);
    }
}
