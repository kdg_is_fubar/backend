package be.kdg.fubar.backend.replay.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class ReplayDTO {
    Long id;
    String roomName;
    List<String> userNames;
    List<Short> endScores;
}
