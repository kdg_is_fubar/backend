package be.kdg.fubar.backend.replay.bl;

import be.kdg.fubar.backend.flooding.bl.FloodingException;
import be.kdg.fubar.backend.flooding.bl.ScoreService;
import be.kdg.fubar.backend.flooding.bl.FloodingService;
import be.kdg.fubar.backend.identity.bl.UserService;
import be.kdg.fubar.backend.identity.model.ApplicationUser;
import be.kdg.fubar.backend.player.bl.PlayerService;
import be.kdg.fubar.backend.player.model.Player;
import be.kdg.fubar.backend.replay.dal.ReplayRepository;
import be.kdg.fubar.backend.replay.exception.ReplayException;
import be.kdg.fubar.backend.replay.model.Replay;
import be.kdg.fubar.backend.replay.model.ReplayDTO;
import be.kdg.fubar.backend.room.RoomException;
import be.kdg.fubar.backend.room.bl.RoomService;
import be.kdg.fubar.backend.room.model.Room;
import be.kdg.fubar.backend.tile.bl.TileService;
import be.kdg.fubar.backend.tile.model.Tile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ReplayService {
    @Lazy
    @Autowired
    private RoomService roomService;
    private final FloodingService floodingService;
    private final PlayerService playerService;
    private final ScoreService scoreService;
    private final UserService userService;
    private final TileService tileService;
    private final ReplayRepository replayRepository;

    @Autowired
    public ReplayService(FloodingService floodingService, PlayerService playerService, ScoreService scoreService, UserService userService, TileService tileService, ReplayRepository replayRepository) {
        this.floodingService = floodingService;
        this.playerService = playerService;
        this.scoreService = scoreService;
        this.userService = userService;
        this.tileService = tileService;
        this.replayRepository = replayRepository;
    }

    /**
     * This method will calculate the next move of the player in a replay.
     * @param replayId the Id of the replay that is being watched
     * @return Will return the new boardState after a move has been made
     * @throws ReplayException An exception concerning the Replay itself
     * @throws FloodingException An exception concerning the flooding of a tile inside of the replay.
     */
    public List<Tile> nextMove(Long replayId) throws ReplayException, FloodingException {
        //find the right replay
        Replay replay = get(replayId);
        Room room = replay.getRoom();
        //increment the move
        replay.setMoveNumber((replay.getMoveNumber() + 1));
        //simulate the board of this turn
        Tile placedTile = room.getBoard().get(replay.getMoveNumber());
        replay.getOwnBoardState().add(placedTile);
        List<Tile> back=room.getBoard();
        room.setBoard(replay.getOwnBoardState());
        floodingService.flood(room, placedTile.getX(), placedTile.getY());
        replay.setOwnBoardState(room.getBoard());
        room.setBoard(back);
        scoreService.distributeScore(replay.getOwnBoardState(), placedTile);
        replayRepository.save(replay);
        return replay.getOwnBoardState();
    }

    public Replay createReplay(Room room) {
        for (Player player : room.getPlayers()) {
            player.setScore((short) 0);
            playerService.save(player);

        }
        Replay replay = new Replay(room);
        replay.addTile(room.getBoard().get(0));
        room.setCurrentPlayer(room.getPlayers().get(room.getIndexOfFirstTPlay()));
        replayRepository.save(replay);
        //save room happens in endGame of roomService. Is this ok?
        return replay;
    }

    public Replay reset(Replay replay) {
        Room room =replay.getRoom();
        //reset the move
        replay.setMoveNumber(0);
        //recreate a start state board
        List<Tile> board=new ArrayList<>();
        board.add(tileService.getStartTile());
        replay.setOwnBoardState(board);
        replayRepository.save(replay);
        roomService.saveRoom(room);
        return replay;
    }
    public Replay reset (Long replayId) throws ReplayException {
        return reset(get(replayId));
    }

    public List<Replay> getAll(String userName){
        List<Replay> replays= replayRepository.findAll();
        ApplicationUser user= this.userService.loadUserByUsername(userName);
        return replays.stream().filter(replay -> {
            boolean replayPlayer = false;
            try {
                replayPlayer =  playerService.getPlayerFromRoom(user,replay.getRoom().getId()).isPresent();
            } catch (RoomException e) {
                e.printStackTrace();
            }
            return replayPlayer;
        }).collect(Collectors.toList());
    }

    public ReplayDTO replayToDTO(Replay replay){
        return new ReplayDTO(
                replay.getId(),
                replay.getRoom().getName(),
                replay.getRoom().getPlayers().stream().map(Player::getUsername).collect(Collectors.toList()),
                replay.getRoom().getPlayersScore());
    }

    public Replay get(Long replayId) throws ReplayException {
        if(replayRepository.findById(replayId).isPresent())
       return replayRepository.findById(replayId).get();
        else throw new ReplayException("Replay not found");
    }
}

/*
 * hier gaan we aan de hand van ons replay data object, beurt naargelang de gebruiker wenst.
 *
 * de hoofData word gezet in de endGame()
 * naast uw Room we een extra Board bijhouden
 * het extra board gaat opnieuw gevuld EN GEFLOOD EN GESCOORD worden => replay
 * we gaan uit gemak enkel stap voor stap vooruit laten gaan
 * we kunnen de speler volgorde afleiden uit de eerste spelersIndex en de groote van de speler lijst
 * */