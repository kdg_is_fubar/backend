package be.kdg.fubar.backend.replay.exception;

public class ReplayException extends Exception {
    public ReplayException(String message) {
        super(message);
    }
}
