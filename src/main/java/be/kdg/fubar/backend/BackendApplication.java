package be.kdg.fubar.backend;

import be.kdg.fubar.backend.identity.bl.UserException;
import be.kdg.fubar.backend.identity.bl.UserService;
import be.kdg.fubar.backend.identity.dal.UserRepo;
import be.kdg.fubar.backend.identity.model.ApplicationUser;
import be.kdg.fubar.backend.player.bl.PlayerService;
import be.kdg.fubar.backend.room.bl.RoomService;
import be.kdg.fubar.backend.tile.bl.TileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class BackendApplication {
    private final UserService userService;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public BackendApplication(UserService userService, BCryptPasswordEncoder bCryptPasswordEncoder, RoomService roomService, PlayerService playerService, TileService tileService, UserRepo userRepo) {
        this.userService = userService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public static void main(String[] args) {
        SpringApplication.run(BackendApplication.class, args);
    }

    /**
     * Will initialize the app with a couple users.
     */
    @PostConstruct
    private void initalizeApp() {
        ApplicationUser cs = new ApplicationUser("appelmoes", "waffle.dealer@outlook.com", bCryptPasswordEncoder.encode("appelmoes"));
        cs.setEnabled(true);
        try {
            userService.saveNewUser(cs);
        } catch (UsernameNotFoundException | UserException e) {
            System.out.println(e);
        }

        ApplicationUser ss = new ApplicationUser("sinaasappelsap", "ccc@hotmail.com", bCryptPasswordEncoder.encode("appelmoes"));
        ss.setEnabled(true);
        try {
            userService.saveNewUser(ss);
        } catch (UsernameNotFoundException | UserException e) {
            System.out.println(e);
        }

        for (int i = 0; i < 8; i++) {
            ApplicationUser ai = new ApplicationUser("AI" + i, "AI" + i, bCryptPasswordEncoder.encode("95gsarT?L3!]2pVb"));
            ss.setEnabled(false);
            try {
                userService.saveNewUser(ai);
            } catch (UsernameNotFoundException | UserException e) {
                System.out.println(e);
            }
        }
    }
}

