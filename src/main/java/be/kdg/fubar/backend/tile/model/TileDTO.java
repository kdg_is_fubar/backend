package be.kdg.fubar.backend.tile.model;

import be.kdg.fubar.backend.follower.model.FollowerDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TileDTO {
    private Integer id;
    private int x,y,rotations;
    private boolean startTile;
    private String name;
    private FollowerDTO follower;
}
