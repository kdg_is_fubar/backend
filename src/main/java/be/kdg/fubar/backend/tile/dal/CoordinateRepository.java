package be.kdg.fubar.backend.tile.dal;

import be.kdg.fubar.backend.tile.model.Coordinate;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CoordinateRepository extends JpaRepository<Coordinate, Long> {
}
