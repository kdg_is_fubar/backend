package be.kdg.fubar.backend.tile.model;


import be.kdg.fubar.backend.follower.model.Follower;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.util.Objects;

@Table(name = "TileSegmentPart")
@Entity
@Data
@NoArgsConstructor
public class TileSegmentPart {
    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private SurfaceType type;
    @OneToOne(cascade = {CascadeType.ALL}, targetEntity= Follower.class, fetch=FetchType.EAGER)
    private Follower follower;
    private boolean flooded = false;
    private boolean completed = false;
    private boolean hasBonusPoints;

    private short adjecencyIdentifier;
    public TileSegmentPart(SurfaceType type) {
        this.type = type;
    }
    //copy constructor
    TileSegmentPart(TileSegmentPart tileSegmentPart){
        this.follower=tileSegmentPart.follower;
        this.flooded=tileSegmentPart.flooded;
        this.type=tileSegmentPart.type;
        this.adjecencyIdentifier=tileSegmentPart.adjecencyIdentifier;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TileSegmentPart that = (TileSegmentPart) o;
        return Objects.equals(type, that.type) &&hasBonusPoints==that.hasBonusPoints;
    }

    @Override
    public int hashCode() {
        return Objects.hash(type);
    }

    public void setAdjecencyIdentifier(int adjecencyIdentifier) {
        this.adjecencyIdentifier = (short)adjecencyIdentifier;
    }
}
