package be.kdg.fubar.backend.tile.model;

import be.kdg.fubar.backend.flooding.model.Floodable;
import lombok.Data;

import javax.persistence.*;
import java.util.Objects;

@Table(name = "TileSegment")
@Entity
@Data
public class TileSegment implements Floodable<TileSegmentPart[][]> {
    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @OneToOne(cascade = {CascadeType.ALL})
    private TileSegmentPart lu, mu, ru, ml, mm, mr, ld, md, rd;

    public TileSegment() {
        this.lu = new TileSegmentPart(SurfaceType.FIELD);
        this.ru = new TileSegmentPart(SurfaceType.FIELD);
        this.ld = new TileSegmentPart(SurfaceType.FIELD);
        this.rd = new TileSegmentPart(SurfaceType.FIELD);
        this.mu = new TileSegmentPart(SurfaceType.FIELD);
        this.md = new TileSegmentPart(SurfaceType.FIELD);
        this.ml = new TileSegmentPart(SurfaceType.FIELD);
        this.mr = new TileSegmentPart(SurfaceType.FIELD);
        this.mm = new TileSegmentPart(SurfaceType.FIELD);
    }

    TileSegment(TileSegment tileSegment) {
        this.lu = new TileSegmentPart(tileSegment.getLu());
        this.ru = new TileSegmentPart(tileSegment.getRu());
        this.ld = new TileSegmentPart(tileSegment.getLd());
        this.rd = new TileSegmentPart(tileSegment.getRd());
        this.mu = new TileSegmentPart(tileSegment.getMu());
        this.md = new TileSegmentPart(tileSegment.getMd());
        this.ml = new TileSegmentPart(tileSegment.getMl());
        this.mr = new TileSegmentPart(tileSegment.getMr());
        this.mm = new TileSegmentPart(tileSegment.getMm());
    }

    public void turnRight() {
        TileSegmentPart lu_ = lu;
        TileSegmentPart mu_ = mu;
        //change corners
        lu = ld;
        ld = rd;
        rd = ru;
        ru = lu_;
        // change middles
        mu = ml;
        ml = md;
        md = mr;
        mr = mu_;
    }

    public TileSegment turnRight(int rotations) {
        rotations = rotations % 4;
        for (int i = 0; i < rotations; i++) {
            turnRight();
        }
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TileSegment that = (TileSegment) o;
        return Objects.equals(lu, that.lu) &&
                Objects.equals(ru, that.ru) &&
                Objects.equals(ld, that.ld) &&
                Objects.equals(rd, that.rd) &&
                Objects.equals(mu, that.mu) &&
                Objects.equals(md, that.md) &&
                Objects.equals(ml, that.ml) &&
                Objects.equals(mr, that.mr) &&
                Objects.equals(mm, that.mm);
    }

    @Override
    public int hashCode() {
        return Objects.hash(lu, ru, ld, rd, mu, md, ml, mr, mm);
    }

    @Override
    public TileSegmentPart[][] toMatrix() {
        TileSegmentPart[][] matrix = new TileSegmentPart[3][3];
        matrix[0][0] = lu;
        matrix[1][0] = mu;
        matrix[2][0] = ru;
        matrix[0][1] = ml;
        matrix[1][1] = mm;
        matrix[2][1] = mr;
        matrix[0][2] = ld;
        matrix[1][2] = md;
        matrix[2][2] = rd;
        return matrix;
    }

    //#region toString()
    public String topLine() {
        return lu.getType() + "\t" + mu.getType() + "\t" + ru.getType();
    }

    public String midLine() {
        return ml.getType() + "\t" + mm.getType() + "\t" + mr.getType();
    }

    public String bottomLine() {
        return ld.getType() + "\t" + md.getType() + "\t" + rd.getType();
    }

    public TileSegmentPart[] tileSegmentPart() {
        TileSegmentPart[] matrix = new TileSegmentPart[9];
        matrix[0] = lu;
        matrix[1] = mu;
        matrix[2] = ru;
        matrix[3] = ml;
        matrix[4] = mm;
        matrix[5] = mr;
        matrix[6] = ld;
        matrix[7] = md;
        matrix[8] = rd;
        return matrix;
    }
    //#endregion
}
