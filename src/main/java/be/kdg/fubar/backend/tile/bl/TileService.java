package be.kdg.fubar.backend.tile.bl;

import be.kdg.fubar.backend.tile.TileException;
import be.kdg.fubar.backend.tile.dal.TileRepository;
import be.kdg.fubar.backend.tile.dal.TileSegmentCreator;
import be.kdg.fubar.backend.tile.dal.TileSegmentPartRepository;
import be.kdg.fubar.backend.tile.model.RelativePosition;
import be.kdg.fubar.backend.tile.model.Tile;
import be.kdg.fubar.backend.tile.model.TileSegment;
import be.kdg.fubar.backend.tile.model.TileSegmentPart;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class TileService {
    private final TileSegmentPartRepository tileSegmentPartrepository;
    private final TileSegmentCreator tsc;
    private final TileRepository tileRepository;
    @Getter
    private Map<String, Tile> tileMap = new TreeMap<>(); // tree map so tiles are saved in order, makes lookups on keys faster.
    @Autowired
    public TileService(TileSegmentPartRepository tileSegmentPartrepository, TileSegmentCreator tileSegmentCreator, TileRepository tileRepository) {
        this.tileSegmentPartrepository = tileSegmentPartrepository;
        this.tsc = tileSegmentCreator;
        this.tileRepository = tileRepository;
        fillTileMap();
    }
    /**
     * Will check if the placed tile matches the settled tile on the left
     * @param placeTile The placed tile
     * @param settledTile The tile that has already been placed
     * @return True if matches, else false.
     */
    private boolean matchLeft(Tile placeTile, Tile settledTile) {
        return settledTile.getMl().getMl().getType().equals(placeTile.getMr().getMr().getType());
    }
    /**
     * Will check if the placed tile matches the settled tile from below
     * @param placeTile The placed tile
     * @param settledTile The tile that has already been placed
     * @return True if matches, else false.
     */
    private boolean matchBelow(Tile placeTile, Tile settledTile) {
        return settledTile.getMd().getMd().getType().equals(placeTile.getMu().getMu().getType());
    }
    /**
     * Will check if the placed tile matches the settled tile on the right
     * @param placeTile The placed tile
     * @param settledTile The tile that has already been placed
     * @return True if matches, else false.
     */
    private boolean matchRight(Tile placeTile, Tile settledTile) {
        return settledTile.getMr().getMr().getType().equals(placeTile.getMl().getMl().getType());
    }
    /**
     * Will check if the placed tile matches the settled tile from above
     * @param placeTile The placed tile
     * @param settledTile The tile that has already been placed
     * @return True if matches, else false.
     */
    private boolean matchAbove(Tile placeTile, Tile settledTile) {
        return settledTile.getMu().getMu().getType().equals(placeTile.getMd().getMd().getType());
    }

    /**
     * This method will check if the tiles match when a tile is being placed next to one or multiple tiles. (CITY next to CITY, FIELD next to FIELD, etc.)
     * @param toPlaceTile The tile that is being placed
     * @param aroundTile The tile that has already been placed and being compared against
     * @return will return true if they match else not.
     */
    public boolean matchTile(List<Tile> aroundTile, Tile toPlaceTile) {
        int x = toPlaceTile.getX();
        int y = toPlaceTile.getY();

        boolean matches = true;
        for (Tile borderdTile : aroundTile) {
            if (x == borderdTile.getX() && y == borderdTile.getY() - 1) {
                if (!matchBelow(toPlaceTile, borderdTile)) {
                    matches = false;
                }
            } else if (x == borderdTile.getX() && y == borderdTile.getY() + 1) {
                if (!matchAbove(toPlaceTile, borderdTile)) {
                    matches = false;
                }
            } else if (y == borderdTile.getY() && x == borderdTile.getX() - 1) {
                if (!matchLeft(toPlaceTile, borderdTile)) {
                    matches = false;
                }
            } else if (y == borderdTile.getY() && x == borderdTile.getX() + 1) {
                if (!matchRight(toPlaceTile, borderdTile)) {
                    matches = false;
                }
            }
        }
        return matches;
    }

    public Tile getTileById(Integer i) {
        return getTile(i);

    }

    public Optional<Tile> getTileByName(String name) {
        Optional<Tile> optionalTile;
        try {
            optionalTile = Optional.of(new Tile(tileMap.get(name)));
        } catch (Exception e) {
            optionalTile = Optional.empty();
        }
        return optionalTile;
    }

    public List<Tile> getTiles() {
        return tileRepository.findAll();
    }

    public void saveTile(Tile tile) {
        tileRepository.save(tile);
    }

    /**
     * This method will return the TileSegmentPart which is next to a certain X and Y on the tile that is being placed
     * @param tile The tile that is being placed
     * @param relX The X coordinate of the segment whose neighbour are being asked
     * @param relY The Y coordinate of the segment whose neighbour are being asked
     * @return the neighbouring segmentpart
     */
    public TileSegmentPart getTileSegmentPartFromRelativeCoordinates(Tile tile, int relX, int relY) throws TileException {
        if (relX < 3 && relY <= 2) return getTileSegmentPartFromRelativeCoordinates(tile.getLu(), relX, relY);
        if (relX < 6 && relY <= 2) return getTileSegmentPartFromRelativeCoordinates(tile.getMu(), relX, relY);
        if (relX < 9 && relY <= 2) return getTileSegmentPartFromRelativeCoordinates(tile.getRu(), relX, relY);

        if (relX < 3 && relY <= 5) return getTileSegmentPartFromRelativeCoordinates(tile.getMl(), relX, relY);
        if (relX < 6 && relY <= 5) return getTileSegmentPartFromRelativeCoordinates(tile.getMm(), relX, relY);
        if (relX < 9 && relY <= 5) return getTileSegmentPartFromRelativeCoordinates(tile.getMr(), relX, relY);

        if (relX < 3 && relY <= 8) return getTileSegmentPartFromRelativeCoordinates(tile.getLd(), relX, relY);
        if (relX < 6 && relY <= 8) return getTileSegmentPartFromRelativeCoordinates(tile.getMd(), relX, relY);
        if (relX < 9 && relY <= 8) return getTileSegmentPartFromRelativeCoordinates(tile.getRd(), relX, relY);
        else throw new TileException("Exception in TileService getTileSegmentPartFromRelativeCoordinates()");
    }

    /**
     * This method will return the TileSegmentPart which is next to a certain X and Y on the TileSegmentPart that is being placed
     * @param tileSegment The segment whose neighbouring segments are being asked
     * @param relX The X coordinate of the segment whose neighbour are being asked
     * @param relY The Y coordinate of the segment whose neighbour are being asked
     * @return the neighbouring segmentpart
     */
    private TileSegmentPart getTileSegmentPartFromRelativeCoordinates(TileSegment tileSegment, int relX, int relY) throws TileException {
        relX = relX % 3;
        relY = relY % 3;
        if (relX == 0 && relY == 0) return tileSegment.getLu();
        if (relX == 1 && relY == 0) return tileSegment.getMu();
        if (relX == 2 && relY == 0) return tileSegment.getRu();

        if (relX == 0 && relY == 1) return tileSegment.getMl();
        if (relX == 1 && relY == 1) return tileSegment.getMm();
        if (relX == 2 && relY == 1) return tileSegment.getMr();

        if (relX == 0 && relY == 2) return tileSegment.getLd();
        if (relX == 1 && relY == 2) return tileSegment.getMd();
        if (relX == 2 && relY == 2) return tileSegment.getRd();
        else throw new TileException("Exception in TileService getTileSegmentPartFromRelativeCoordinates()");
    }

    public TileSegmentPart getTileSegmentPart(Long partId) throws TileException {
        Optional<TileSegmentPart> optionalTileSegmentPart = getTileSegmentPartById(partId);
        if (optionalTileSegmentPart.isPresent())
            return optionalTileSegmentPart.get();
        throw new TileException("TileSegmentPart not found");
    }

    public TileSegmentPart saveTileSegmentPart(TileSegmentPart part) {
        return tileSegmentPartrepository.save(part);
    }

    /**
     * This method will get he startTile of the game (this is predefined in the game)
     * @return returns the starting tile
     */
    public Tile getStartTile() {
        Tile tile = new Tile(
                tsc.cityDiagonaleLeftUp(1), tsc.cityFull(), tsc.cityDiagonaleLeftUp(0),
                tsc.roadVertical(1), tsc.roadVertical(1), tsc.roadVertical(1),
                tsc.fieldFull(), tsc.fieldFull(), tsc.fieldFull()
        );
        tile.setStartTile(true);
        tile.setName("d");
        tile.setCoords(0, 0);
        return tile;
    }

    public Optional<TileSegmentPart> getTileSegmentPartById(Long partId) {
        return tileSegmentPartrepository.findById(partId);
    }

    public Tile getTile(Integer i) {
        if (tileRepository.findById(i).isPresent()) {
            return tileRepository.findById(i).get();
        }
        return null;
    }

    /**
     * This method creates all the different kinds of tiles in the game.
     */
    private void fillTileMap() {
        //1 ROW
        Tile a = new Tile("a",
                tsc.fieldFull(), tsc.fieldFull(), tsc.fieldFull(),
                tsc.fieldFull(), tsc.monestary(), tsc.fieldFull(),
                tsc.fieldFull(), tsc.roadVertical(0), tsc.fieldFull()
        );
        tileMap.put(a.getName(), a);
        Tile b = new Tile("b",
                tsc.fieldFull(), tsc.fieldFull(), tsc.fieldFull(),
                tsc.fieldFull(), tsc.monestary(), tsc.fieldFull(),
                tsc.fieldFull(), tsc.fieldFull(), tsc.fieldFull()
        );
        tileMap.put(b.getName(), b);
        Tile c = new Tile("c",
                tsc.cityFull(), tsc.cityFull(), tsc.cityFull(),
                tsc.cityFull(), tsc.cityFull(), tsc.cityFull(),
                tsc.cityFull(), tsc.cityFull(), tsc.cityFull()
        );
        c.getLu().getMm().setHasBonusPoints(true);
        tileMap.put(c.getName(), c);
        Tile d = new Tile("d",
                tsc.cityDiagonaleLeftUp(1), tsc.cityFull(), tsc.cityDiagonaleLeftUp(0),
                tsc.roadVertical(1), tsc.roadVertical(1), tsc.roadVertical(1),
                tsc.fieldFull(), tsc.fieldFull(), tsc.fieldFull()
        );
        tileMap.put(d.getName(), d);
        Tile e = new Tile("e",
                tsc.cityDiagonaleLeftUp(1), tsc.cityFull(), tsc.cityDiagonaleLeftUp(0),
                tsc.fieldFull(), tsc.fieldFull(), tsc.fieldFull(),
                tsc.fieldFull(), tsc.fieldFull(), tsc.fieldFull()
        );
        tileMap.put(e.getName(), e);
        Tile f = new Tile("f",
                tsc.cityDiagonaleLeftUp(3), tsc.fieldFull(), tsc.cityDiagonaleLeftUp(2),
                tsc.cityFull(), tsc.cityFull(), tsc.cityFull(),
                tsc.cityDiagonaleLeftUp(0), tsc.fieldFull(), tsc.cityDiagonaleLeftUp(1)
        );
        f.getMr().getMm().setHasBonusPoints(true);
        tileMap.put(f.getName(), f);
//2 ROW
        Tile g = new Tile("g",
                tsc.cityDiagonaleLeftUp(3), tsc.fieldFull(), tsc.cityDiagonaleLeftUp(2),
                tsc.cityFull(), tsc.cityFull(), tsc.cityFull(),
                tsc.cityDiagonaleLeftUp(0), tsc.fieldFull(), tsc.cityDiagonaleLeftUp(1)
        );
        tileMap.put(g.getName(), g);
        Tile h = new Tile("h",
                tsc.cityDiagonaleLeftUp(3), tsc.fieldFull(), tsc.cityDiagonaleLeftUp(2),
                tsc.cityFull(), tsc.fieldFull(), tsc.cityFull(),
                tsc.cityDiagonaleLeftUp(0), tsc.fieldFull(), tsc.cityDiagonaleLeftUp(1)
        );
        tileMap.put(h.getName(), h);
        Tile i = new Tile("i",
                tsc.cityCornerDownRight(2), tsc.cityFull(), tsc.cityDiagonaleLeftUp(0),
                tsc.cityFull(), tsc.fieldFull(), tsc.fieldFull(),
                tsc.cityDiagonaleLeftUp(0), tsc.fieldFull(), tsc.fieldFull()
        );
        tileMap.put(i.getName(), i);
        Tile j = new Tile("j",
                tsc.cityDiagonaleLeftUp(1), tsc.cityFull(), tsc.cityDiagonaleLeftUp(0),
                tsc.fieldFull(), tsc.roadCornerDownToRigth(0), tsc.roadVertical(1),
                tsc.fieldFull(), tsc.roadVertical(0), tsc.fieldFull()
        );
        tileMap.put(j.getName(), j);
        Tile k = new Tile("k",
                tsc.cityDiagonaleLeftUp(1), tsc.cityFull(), tsc.cityDiagonaleLeftUp(0),
                tsc.roadVertical(1), tsc.roadCornerDownToRigth(1), tsc.fieldFull(),
                tsc.fieldFull(), tsc.roadVertical(0), tsc.fieldFull()
        );
        tileMap.put(k.getName(), k);
        Tile l = new Tile("l",
                tsc.cityDiagonaleLeftUp(1), tsc.cityFull(), tsc.cityDiagonaleLeftUp(0),
                tsc.roadVertical(1), tsc.roadTdown(0), tsc.roadVertical(1),
                tsc.fieldFull(), tsc.roadVertical(0), tsc.fieldFull()
        );
        tileMap.put(l.getName(), l);
        //3 ROW
        Tile m = new Tile("m",
                tsc.cityFull(), tsc.cityFull(), tsc.cityCornerDownRight(0),
                tsc.cityFull(), tsc.roadCornerDownToRigth(0), tsc.roadVertical(1),
                tsc.cityCornerDownRight(0), tsc.roadVertical(0), tsc.fieldFull()
        );
        m.getLu().getMm().setHasBonusPoints(true);
        tileMap.put(m.getName(), m);
        Tile n = new Tile("n",
                tsc.cityFull(), tsc.cityFull(), tsc.cityCornerDownRight(0),
                tsc.cityFull(), tsc.fieldFull(), tsc.fieldFull(),
                tsc.cityCornerDownRight(0), tsc.fieldFull(), tsc.fieldFull()
        );
        tileMap.put(n.getName(), n);
        Tile o = new Tile("o",
                tsc.cityFull(), tsc.cityFull(), tsc.cityCornerDownRight(0),
                tsc.cityFull(), tsc.fieldFull(), tsc.fieldFull(),
                tsc.cityCornerDownRight(0), tsc.fieldFull(), tsc.fieldFull()
        );
        o.getLu().getMm().setHasBonusPoints(true);
        tileMap.put(o.getName(), o);
        Tile p = new Tile("p",
                tsc.cityFull(), tsc.cityFull(), tsc.cityCornerDownRight(0),
                tsc.cityFull(), tsc.roadCornerDownToRigth(0), tsc.roadVertical(1),
                tsc.cityCornerDownRight(0), tsc.roadVertical(0), tsc.fieldFull()
        );
        tileMap.put(p.getName(), p);
        Tile q = new Tile("q",
                tsc.cityFull(), tsc.cityFull(), tsc.cityFull(),
                tsc.cityFull(), tsc.cityFull(), tsc.cityFull(),
                tsc.cityCornerDownRight(0), tsc.fieldFull(), tsc.cityCornerDownRight(1)
        );
        q.getLu().getMm().setHasBonusPoints(true);
        tileMap.put(q.getName(), q);
        Tile r = new Tile("r",
                tsc.cityFull(), tsc.cityFull(), tsc.cityFull(),
                tsc.cityFull(), tsc.cityFull(), tsc.cityFull(),
                tsc.cityCornerDownRight(0), tsc.fieldFull(), tsc.cityCornerDownRight(1)
        );
        tileMap.put(r.getName(), r);
        //4 ROW
        Tile s = new Tile("s",
                tsc.cityFull(), tsc.cityFull(), tsc.cityFull(),
                tsc.cityFull(), tsc.cityFull(), tsc.cityFull(),
                tsc.cityCornerDownRight(0), tsc.roadVertical(0), tsc.cityCornerDownRight(1)
        );
        s.getLu().getMm().setHasBonusPoints(true);
        tileMap.put(s.getName(), s);
        Tile t = new Tile("t",
                tsc.cityFull(), tsc.cityFull(), tsc.cityFull(),
                tsc.cityFull(), tsc.cityFull(), tsc.cityFull(),
                tsc.cityCornerDownRight(0), tsc.roadVertical(0), tsc.cityCornerDownRight(1)
        );
        tileMap.put(t.getName(), t);
        Tile u = new Tile("u",
                tsc.fieldFull(), tsc.roadVertical(0), tsc.fieldFull(),
                tsc.fieldFull(), tsc.roadVertical(0), tsc.fieldFull(),
                tsc.fieldFull(), tsc.roadVertical(0), tsc.fieldFull()
        );
        tileMap.put(u.getName(), u);
        Tile v = new Tile("v",
                tsc.fieldFull(), tsc.fieldFull(), tsc.fieldFull(),
                tsc.roadVertical(1), tsc.roadCornerDownToRigth(1), tsc.fieldFull(),
                tsc.fieldFull(), tsc.roadVertical(0), tsc.fieldFull()
        );
        tileMap.put(v.getName(), v); //TRoadDown
        Tile w = new Tile("w",
                tsc.fieldFull(), tsc.fieldFull(), tsc.fieldFull(),
                tsc.roadVertical(1), tsc.roadTdown(0), tsc.roadVertical(1),
                tsc.fieldFull(), tsc.roadVertical(0), tsc.fieldFull()
        );
        tileMap.put(w.getName(), w);
        Tile x = new Tile("x", //CrossRoad
                tsc.fieldFull(), tsc.roadVertical(0), tsc.fieldFull(),
                tsc.roadVertical(1), tsc.roadFourWay(), tsc.roadVertical(1),
                tsc.fieldFull(), tsc.roadVertical(0), tsc.fieldFull()
        );
        tileMap.put(x.getName(), x);
    }

    /**
     * This will create all the tiles which are available in the deck (which tile x amount of times the tile is available)
     * @return this returns a list of 72 tiles.
     */
    // ZIE https://www.fgbradleys.com/rules/rules2/Carcassonne-rules.pdf
    public List<Tile> getNewDeck() {
        List<Tile> tiles = new ArrayList<>();
        addTiles(tiles, tileMap.get("a"), 2);
        addTiles(tiles, tileMap.get("b"), 4);
        addTiles(tiles, tileMap.get("c"), 2);
        addTiles(tiles, tileMap.get("d"), 3);
        addTiles(tiles, tileMap.get("e"), 5);
        addTiles(tiles, tileMap.get("f"), 2);
        addTiles(tiles, tileMap.get("g"), 1);
        addTiles(tiles, tileMap.get("h"), 3);
        addTiles(tiles, tileMap.get("i"), 2);
        addTiles(tiles, tileMap.get("j"), 3);
        addTiles(tiles, tileMap.get("k"), 3);
        addTiles(tiles, tileMap.get("l"), 3);
        addTiles(tiles, tileMap.get("m"), 2);
        addTiles(tiles, tileMap.get("n"), 3);
        addTiles(tiles, tileMap.get("o"), 2);
        addTiles(tiles, tileMap.get("p"), 3);
        addTiles(tiles, tileMap.get("q"), 1);
        addTiles(tiles, tileMap.get("r"), 3);
        addTiles(tiles, tileMap.get("s"), 2);
        addTiles(tiles, tileMap.get("t"), 1);
        addTiles(tiles, tileMap.get("u"), 8);
        addTiles(tiles, tileMap.get("v"), 9);
        addTiles(tiles, tileMap.get("w"), 4);
        addTiles(tiles, tileMap.get("x"), 1);
        return tiles;
    }

    /**
     * This will add tiles to a list of tiles
     * @param tiles The list of tiles
     * @param tile Which tile is being added
     * @param i the amount of timse the tile should be added to the list
     */
    private void addTiles(List<Tile> tiles, Tile tile, int i) {
        for (int x = 0; x < i; x++) {
            tiles.add(new Tile(tile));
        }
    }
}
