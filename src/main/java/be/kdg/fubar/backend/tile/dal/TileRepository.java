package be.kdg.fubar.backend.tile.dal;

import be.kdg.fubar.backend.tile.model.Tile;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TileRepository extends JpaRepository<Tile, Integer> {

}
