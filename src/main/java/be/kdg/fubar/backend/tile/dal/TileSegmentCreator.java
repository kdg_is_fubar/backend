package be.kdg.fubar.backend.tile.dal;

import be.kdg.fubar.backend.tile.model.SurfaceType;
import be.kdg.fubar.backend.tile.model.TileSegment;
import be.kdg.fubar.backend.tile.model.TileSegmentPart;
import org.springframework.stereotype.Component;

/**
 * This class will make generating tiles easier.
 **/
@Component
public class TileSegmentCreator {

    public TileSegment fieldFull() {
        TileSegment fieldfull = new TileSegment();
        fieldfull.setLu(new TileSegmentPart(SurfaceType.FIELD));
        fieldfull.setRu(new TileSegmentPart(SurfaceType.FIELD));
        fieldfull.setLd(new TileSegmentPart(SurfaceType.FIELD));
        fieldfull.setRd(new TileSegmentPart(SurfaceType.FIELD));
        fieldfull.setMu(new TileSegmentPart(SurfaceType.FIELD));
        fieldfull.setMd(new TileSegmentPart(SurfaceType.FIELD));
        fieldfull.setMl(new TileSegmentPart(SurfaceType.FIELD));
        fieldfull.setMr(new TileSegmentPart(SurfaceType.FIELD));
        fieldfull.setMm(new TileSegmentPart(SurfaceType.FIELD));
        return fieldfull;
    }

    public TileSegment cityFull() {
        TileSegment cityfull = new TileSegment();
        cityfull.setLu(new TileSegmentPart(SurfaceType.CITY));
        cityfull.setRu(new TileSegmentPart(SurfaceType.CITY));
        cityfull.setLd(new TileSegmentPart(SurfaceType.CITY));
        cityfull.setRd(new TileSegmentPart(SurfaceType.CITY));
        cityfull.setMu(new TileSegmentPart(SurfaceType.CITY));
        cityfull.setMd(new TileSegmentPart(SurfaceType.CITY));
        cityfull.setMl(new TileSegmentPart(SurfaceType.CITY));
        cityfull.setMr(new TileSegmentPart(SurfaceType.CITY));
        cityfull.setMm(new TileSegmentPart(SurfaceType.CITY));
        return cityfull;
    }

    public TileSegment monestary() {
        TileSegment ts = new TileSegment();
        ts.setLu(new TileSegmentPart(SurfaceType.MONASTERY));
        ts.setRu(new TileSegmentPart(SurfaceType.MONASTERY));
        ts.setLd(new TileSegmentPart(SurfaceType.MONASTERY));
        ts.setRd(new TileSegmentPart(SurfaceType.MONASTERY));
        ts.setMu(new TileSegmentPart(SurfaceType.MONASTERY));
        ts.setMd(new TileSegmentPart(SurfaceType.MONASTERY));
        ts.setMl(new TileSegmentPart(SurfaceType.MONASTERY));
        ts.setMr(new TileSegmentPart(SurfaceType.MONASTERY));
        ts.setMm(new TileSegmentPart(SurfaceType.MONASTERY));
        return ts;
    }

    public TileSegment roadCornerDownToRigth(int rotation) {
        TileSegment ts = new TileSegment();
        ts.setLu(new TileSegmentPart(SurfaceType.FIELD));
        ts.setMu(new TileSegmentPart(SurfaceType.FIELD));
        ts.setRu(new TileSegmentPart(SurfaceType.FIELD));

        ts.setMl(new TileSegmentPart(SurfaceType.FIELD));
        ts.setMm(new TileSegmentPart(SurfaceType.ROAD));
        ts.setMr(new TileSegmentPart(SurfaceType.ROAD));

        ts.setLd(new TileSegmentPart(SurfaceType.FIELD));
        ts.setMd(new TileSegmentPart(SurfaceType.ROAD));
        ts.setRd(new TileSegmentPart(SurfaceType.FIELD));


        ts.turnRight(rotation);
        return ts;
    }

    public TileSegment roadVertical(int rotation) {
        TileSegment ts = new TileSegment();
        ts.setLu(new TileSegmentPart(SurfaceType.FIELD));
        ts.setMu(new TileSegmentPart(SurfaceType.ROAD));
        ts.setRu(new TileSegmentPart(SurfaceType.FIELD));

        ts.setMl(new TileSegmentPart(SurfaceType.FIELD));
        ts.setMm(new TileSegmentPart(SurfaceType.ROAD));
        ts.setMr(new TileSegmentPart(SurfaceType.FIELD));

        ts.setLd(new TileSegmentPart(SurfaceType.FIELD));
        ts.setMd(new TileSegmentPart(SurfaceType.ROAD));
        ts.setRd(new TileSegmentPart(SurfaceType.FIELD));

        ts.turnRight(rotation);
        return ts;
    }

    public TileSegment roadFourWay() {
        TileSegment ts = new TileSegment();
        ts.setLu(new TileSegmentPart(SurfaceType.FIELD));
        ts.setMu(new TileSegmentPart(SurfaceType.ROAD));
        ts.setRu(new TileSegmentPart(SurfaceType.FIELD));

        ts.setMl(new TileSegmentPart(SurfaceType.ROAD));
        ts.setMm(new TileSegmentPart(SurfaceType.ELDRITCH));
        ts.setMr(new TileSegmentPart(SurfaceType.ROAD));

        ts.setLd(new TileSegmentPart(SurfaceType.FIELD));
        ts.setMd(new TileSegmentPart(SurfaceType.ROAD));
        ts.setRd(new TileSegmentPart(SurfaceType.FIELD));
        return ts;
    }

    public TileSegment cityCornerDownRight(int rotation) {
        TileSegment ts = new TileSegment();
        ts.setLu(new TileSegmentPart(SurfaceType.FIELD));
        ts.setMu(new TileSegmentPart(SurfaceType.FIELD));
        ts.setRu(new TileSegmentPart(SurfaceType.CITY));

        ts.setMl(new TileSegmentPart(SurfaceType.FIELD));
        ts.setMm(new TileSegmentPart(SurfaceType.FIELD));
        ts.setMr(new TileSegmentPart(SurfaceType.CITY));

        ts.setLd(new TileSegmentPart(SurfaceType.CITY));
        ts.setMd(new TileSegmentPart(SurfaceType.CITY));
        ts.setRd(new TileSegmentPart(SurfaceType.ELDRITCH));
        ts.turnRight(rotation);
        return ts;
    }

    public TileSegment roadTdown(int rotation) {
        TileSegment ts = new TileSegment();
        ts.setLu(new TileSegmentPart(SurfaceType.FIELD));
        ts.setMu(new TileSegmentPart(SurfaceType.FIELD));
        ts.setRu(new TileSegmentPart(SurfaceType.FIELD));

        ts.setMl(new TileSegmentPart(SurfaceType.ROAD));
        ts.setMm(new TileSegmentPart(SurfaceType.ELDRITCH));
        ts.setMr(new TileSegmentPart(SurfaceType.ROAD));

        ts.setLd(new TileSegmentPart(SurfaceType.FIELD));
        ts.setMd(new TileSegmentPart(SurfaceType.ROAD));
        ts.setRd(new TileSegmentPart(SurfaceType.FIELD));

        ts.turnRight(rotation);
        return ts;
    }

    public TileSegment cityDiagonaleLeftUp(int rotation) {
        TileSegment ts = new TileSegment();
        ts.setLu(new TileSegmentPart(SurfaceType.CITY));
        ts.setMu(new TileSegmentPart(SurfaceType.CITY));
        ts.setRu(new TileSegmentPart(SurfaceType.ELDRITCH));

        ts.setMl(new TileSegmentPart(SurfaceType.CITY));
        ts.setMm(new TileSegmentPart(SurfaceType.CITY));
        ts.setMr(new TileSegmentPart(SurfaceType.FIELD));

        ts.setLd(new TileSegmentPart(SurfaceType.ELDRITCH));
        ts.setMd(new TileSegmentPart(SurfaceType.FIELD));
        ts.setRd(new TileSegmentPart(SurfaceType.FIELD));

        ts.turnRight(rotation);
        return ts;
    }
}
