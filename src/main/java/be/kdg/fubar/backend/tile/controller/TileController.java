package be.kdg.fubar.backend.tile.controller;

import be.kdg.fubar.backend.tile.bl.TileService;
import be.kdg.fubar.backend.tile.model.Tile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RequestMapping("/tile")
@RestController
public class TileController {

    private final TileService tmanager;

    @Autowired
    public TileController(TileService tmanager) {
        this.tmanager = tmanager;
    }

    @PostMapping("/")
    public void postTile(Tile tile) {
        tmanager.saveTile(tile);
    }

    @RequestMapping("/all")
    public List<Tile> getTiles() {
        return tmanager.getTiles();
    }

    @RequestMapping("/{id}")
    public Tile getTile(@PathVariable("id") Integer id) {
        return tmanager.getTileById(id);
    }

    @RequestMapping("/test")
    public Tile test() {
        return new Tile();
    }

    /**
     * This endpoint will retrieve a new deck.
     * @return a new deck of tiles
     */
    @RequestMapping("/newDeck")
    public List<Tile> newDeck() {
        return tmanager.getNewDeck();
    }
}
