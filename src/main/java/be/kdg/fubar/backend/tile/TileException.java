package be.kdg.fubar.backend.tile;

public class TileException extends Exception {
    public TileException(String message) {
        super(message);
    }
}
