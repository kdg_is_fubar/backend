package be.kdg.fubar.backend.tile.model;

import be.kdg.fubar.backend.flooding.model.Floodable;
import lombok.Data;

import javax.persistence.*;
import java.util.Objects;

@Table(name = "Tile")
@Entity
@Data
public class Tile implements Floodable<TileSegmentPart[][][][]> {

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column
    private int x, y;
    @Column
    private boolean startTile;
    @Column
    private String name;
    @Column
    private String userNameOfPlacer;
    @OneToOne(cascade = {CascadeType.ALL}, targetEntity = TileSegment.class, fetch = FetchType.EAGER)
    //left up , right up, left down, right down, middle up, middle down, middle left, middle right, middle middle;
    private TileSegment lu, mu, ru, ml, mm, mr, ld, md, rd;
    @Column
    private byte rotations;

    public Tile() {
        this.lu = new TileSegment();
        this.ru = new TileSegment();
        this.ld = new TileSegment();
        this.rd = new TileSegment();
        this.mu = new TileSegment();
        this.md = new TileSegment();
        this.ml = new TileSegment();
        this.mr = new TileSegment();
        this.mm = new TileSegment();
        this.rotations = 0;
        this.startTile = false;
    }

    public Tile(int x, int y, boolean startTile, String name, int rotations) {
        this();
        this.x = x;
        this.y = y;
        this.startTile = startTile;
        this.name = name;
        this.rotations = (byte) rotations;
    }

    public Tile(TileSegment lu, TileSegment mu, TileSegment ru, TileSegment ml, TileSegment mm, TileSegment mr, TileSegment ld, TileSegment md, TileSegment rd) {
        this();
        this.lu = lu;
        this.ru = ru;
        this.ld = ld;
        this.rd = rd;
        this.mu = mu;
        this.md = md;
        this.ml = ml;
        this.mr = mr;
        this.mm = mm;
    }

    public Tile(String name, TileSegment lu, TileSegment mu, TileSegment ru, TileSegment ml, TileSegment mm, TileSegment mr, TileSegment ld, TileSegment md, TileSegment rd) {
        this(lu, mu, ru, ml, mm, mr, ld, md, rd);
        this.setName(name);
    }

    //Copy constructor
    public Tile(Tile tile) {
        this.x = tile.x;
        this.y = tile.y;
        this.startTile = tile.startTile;
        this.name = tile.name;
        this.rotations = tile.rotations;
        this.lu = new TileSegment(tile.lu);
        this.ru = new TileSegment(tile.ru);
        this.ld = new TileSegment(tile.ld);
        this.rd = new TileSegment(tile.rd);
        this.mu = new TileSegment(tile.mu);
        this.md = new TileSegment(tile.md);
        this.ml = new TileSegment(tile.ml);
        this.mr = new TileSegment(tile.mr);
        this.mm = new TileSegment(tile.mm);
    }

    public void turnRight() {
        TileSegment lu_ = lu;
        TileSegment mu_ = mu;
        //change corners
        lu = ld;
        ld = rd;
        rd = ru;
        ru = lu_;
        // change middles
        mu = ml;
        ml = md;
        md = mr;
        mr = mu_;

        lu.turnRight();
        ld.turnRight();
        rd.turnRight();
        ru.turnRight();
        mu.turnRight();
        ml.turnRight();
        md.turnRight();
        mr.turnRight();
        mm.turnRight();
    }

    public Tile turnRight(int rotations) {
        rotations = rotations % 4;
        for (int i = 0; i < rotations; i++) {
            turnRight();
        }
        this.rotations = (byte) rotations;
        return this;
    }

    public void setCoords(int x, int y) {
        this.setX(x);
        this.setY(y);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tile tile = (Tile) o;
        return Objects.equals(lu, tile.lu) &&
                Objects.equals(ru, tile.ru) &&
                Objects.equals(ld, tile.ld) &&
                Objects.equals(rd, tile.rd) &&
                Objects.equals(mu, tile.mu) &&
                Objects.equals(md, tile.md) &&
                Objects.equals(ml, tile.ml) &&
                Objects.equals(mr, tile.mr) &&
                Objects.equals(mm, tile.mm);
    }

    @Override
    public int hashCode() {
        return Objects.hash(lu, ru, ld, rd, mu, md, ml, mr, mm);
    }


    //Tile segment X Y
    //TIle segment part XY
    @Override
    public TileSegmentPart[][][][] toMatrix() {
        TileSegmentPart[][][][] matrix = new TileSegmentPart[3][3][3][3];
        matrix[0][0] = lu.toMatrix();
        matrix[1][0] = mu.toMatrix();
        matrix[2][0] = ru.toMatrix();
        matrix[0][1] = ml.toMatrix();
        matrix[1][1] = mm.toMatrix();
        matrix[2][1] = mr.toMatrix();
        matrix[0][2] = ld.toMatrix();
        matrix[1][2] = md.toMatrix();
        matrix[2][2] = rd.toMatrix();
        return matrix;
    }

    @Override //for developing purposes
    public String toString() {
        return lu.topLine() + mu.topLine() + ru.topLine() + "\n" +
                lu.midLine() + mu.midLine() + ru.midLine() + "\n" +
                lu.bottomLine() + mu.bottomLine() + ru.bottomLine() + "\n" +
                ml.topLine() + mm.topLine() + mr.topLine() + "\n" +
                ml.midLine() + mm.midLine() + mr.midLine() + "\n" +
                ml.bottomLine() + mm.bottomLine() + mr.bottomLine() + "\n" +
                ld.topLine() + md.topLine() + rd.topLine() + "\n" +
                ld.midLine() + md.midLine() + rd.midLine() + "\n" +
                ld.bottomLine() + md.bottomLine() + rd.bottomLine() + "\n"
                ;
    }

    public TileSegment[] tileSegments() {
        TileSegment[] matrix = new TileSegment[9];
        matrix[0] = lu;
        matrix[1] = mu;
        matrix[2] = ru;
        matrix[3] = ml;
        matrix[4] = mm;
        matrix[5] = mr;
        matrix[6] = ld;
        matrix[7] = md;
        matrix[8] = rd;
        return matrix;
    }
}