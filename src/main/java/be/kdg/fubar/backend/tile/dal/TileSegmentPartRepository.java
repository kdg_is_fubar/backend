package be.kdg.fubar.backend.tile.dal;

import be.kdg.fubar.backend.tile.model.TileSegmentPart;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TileSegmentPartRepository extends JpaRepository<TileSegmentPart,Long> {
}
