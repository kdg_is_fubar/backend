package be.kdg.fubar.backend.tile.model;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Table
@Entity
@NoArgsConstructor
public class Coordinate implements Comparable<Coordinate> {
    @Id
    @Getter
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private int x;
    private int y;

    public Coordinate(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public int compareTo(Coordinate o) {

        int result = Integer.compare(o.getX(), x);
        if (result == 0)
            result = Integer.compare(o.getY(), y);
        return result;
    }
}
