package be.kdg.fubar.backend.tile.dal;

import be.kdg.fubar.backend.tile.model.TileSegment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TileSegmentRepository extends JpaRepository<TileSegment, Long> {
}
