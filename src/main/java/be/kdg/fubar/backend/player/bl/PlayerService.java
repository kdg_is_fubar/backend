package be.kdg.fubar.backend.player.bl;

import be.kdg.fubar.backend.follower.bl.FollowerService;
import be.kdg.fubar.backend.follower.model.Follower;
import be.kdg.fubar.backend.identity.bl.UserException;
import be.kdg.fubar.backend.identity.bl.UserService;
import be.kdg.fubar.backend.identity.model.ApplicationUser;
import be.kdg.fubar.backend.player.PlayerException;
import be.kdg.fubar.backend.player.dal.PlayerRepository;
import be.kdg.fubar.backend.player.model.Player;
import be.kdg.fubar.backend.player.model.PlayerDTO;
import be.kdg.fubar.backend.room.RoomException;
import be.kdg.fubar.backend.room.bl.RoomService;
import be.kdg.fubar.backend.room.model.Room;
import be.kdg.fubar.backend.tile.TileException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
public class PlayerService {
    private final PlayerRepository playerRepository;
    private final FollowerService followerService;
    private final UserService userService;
    @Lazy
    @Autowired
    private RoomService roomService;

    @Autowired
    public PlayerService(FollowerService followerService, UserService userService, PlayerRepository playerRepository) {
        this.followerService = followerService;
        this.userService = userService;
        this.playerRepository = playerRepository;
    }

    public Player getPlayer(Long id) throws PlayerException {
        Optional<Player> optionalPlayer = playerRepository.findById(id);
        if (optionalPlayer.isPresent()) {
            return optionalPlayer.get();
        } else throw new PlayerException("player not found Exception"); // TODO custom exception
    }

    public int getNumberOfFreeFollowers(Long playerId) throws PlayerException {
        return ((int) getPlayer(playerId).getFollowers().stream().filter(f -> !f.isPlaced()).count());
    }

    public Optional<Follower> getUnusedFollower(Long playerId) throws PlayerException {
        return getPlayer(playerId).getFollowers().stream()
                .filter(follower -> !follower.isPlaced()).findFirst();
    }

    //saved room player and user
    public Player creatNewPlayer(String email, Room room) throws UserException { //TODO other parameter then email like i don't know ID?
        ApplicationUser applicationUser = userService.loadUserByEmail(email);
        Player player = new Player();
        player = save(player);
        for (int i = 0; i < 7; i++) {
            player.getFollowers().add(new Follower(player.getId()));
        }
        player.setUsername(applicationUser.getUsername());
        player.setRoomId(room.getId());
        player = save(player);

        room.getPlayers().add(player);
        roomService.saveRoom(room);

        applicationUser.getPlayers().add(player);
        userService.updateUser(applicationUser);
        return player;
    }

    public Optional<Player> deletePlayerFromRoom(String email, Room room) throws UserException, RoomException {
        ApplicationUser applicationUser = userService.loadUserByEmail(email);
        if (room.getPlayers().stream().anyMatch(x -> x.getUsername().equals(applicationUser.getUsername()))) {
            Player toDelete = room.getPlayers().stream().filter(x -> x.getUsername().equals(applicationUser.getUsername())).findFirst().get();
            room.getPlayers().removeIf(x -> x.getUsername().equalsIgnoreCase(toDelete.getUsername()));
            applicationUser.getPlayers().removeIf(x -> x.getRoomId().equals(toDelete.getRoomId()));

            roomService.updateRoom(room);
            userService.updateUser(applicationUser);
            if (room.getPlayers().isEmpty()) {
                roomService.deleteRoom(room);
            }
            return Optional.of(toDelete);
        }
        return Optional.empty();
    }

    public boolean placeFollower(Long playerId, Long partId) throws PlayerException, TileException {
        Optional<Follower> optionalFollower = getUnusedFollower(playerId);
        if (optionalFollower.isPresent()) {
            Follower follower = optionalFollower.get();
            followerService.placeFollower(partId, follower);
            return true;
        }
        return false;
    }

    public Optional<Player> getPlayerFromRoom(ApplicationUser applicationUser, Long roomId) throws RoomException {
        Room r = roomService.getRoom(roomId);
        return applicationUser.getPlayers().stream()
                .filter(player ->
                        r.getPlayers().stream()
                                .anyMatch(r_player ->
                                        r_player.equals(player))
                ).findFirst();
    }


    public Player save(Player player) {
        return playerRepository.save(player);
    }

    /**
     * This method will convert a Player into a PlayerDTO
     * @param player The player that has to be converted
     * @return A PlayerDTO that represents the player.
     */
    public PlayerDTO convertToDTO(Player player) {
        return new PlayerDTO(player.getUsername(),
                player.getFollowers().stream().filter(follower -> !follower.isPlaced()).toArray(Follower[]::new).length,
                player.getScore());
    }
}
