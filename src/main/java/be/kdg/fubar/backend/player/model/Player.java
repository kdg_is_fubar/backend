package be.kdg.fubar.backend.player.model;

import be.kdg.fubar.backend.follower.model.Follower;
import lombok.AccessLevel;
import lombok.Data;
import be.kdg.fubar.backend.tile.model.Tile;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Data
@Entity
@Table(name = "Player")
public class Player {
    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter(AccessLevel.NONE)
    private Long id;
    private short score;
    private Long roomId;
    private boolean host;
    @OneToOne(targetEntity = Tile.class)
    private Tile drawnTile;
    private String username;

    @OneToMany(cascade = {CascadeType.ALL}, targetEntity = Follower.class, fetch = FetchType.EAGER)
    private List<Follower> followers;
    private boolean placed;

    public Player() {
        this.followers = new ArrayList<>();

    }

    public Player(String username) {
        this.username = username;
    }

    public Player(String username, Long roomId) {
        this.roomId = roomId;
        this.username = username;
        this.followers = new ArrayList<>();

    }
    public void addScore(byte points){
        this.score += (int)points;
    }




    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return Objects.equals(id, player.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

}
