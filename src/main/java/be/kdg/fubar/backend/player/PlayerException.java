package be.kdg.fubar.backend.player;

public class PlayerException extends Exception {
    public PlayerException(String message) {
        super(message);
    }
}
