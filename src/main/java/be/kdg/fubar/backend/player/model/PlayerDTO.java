package be.kdg.fubar.backend.player.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class PlayerDTO {
    private String username;
    private int followersAmount;
    private int score;
}
