package be.kdg.fubar.backend.player.controller;

import be.kdg.fubar.backend.player.PlayerException;
import be.kdg.fubar.backend.player.bl.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("player")
@RestController
public class PlayerController {
    private final PlayerService playerService;

    @Autowired
    public PlayerController(PlayerService playerService) {
        this.playerService = playerService;
    }

    /**
     * Endpoint to see if the player a has a follower available to him.
     * @param playerId
     * @return
     */
    @GetMapping("/{player}/hasfollowersinhand")
    public boolean hasFollowersInHand(@PathVariable("player") Long playerId) throws PlayerException {
        return playerService.getNumberOfFreeFollowers(playerId) > 0;
    }
}
