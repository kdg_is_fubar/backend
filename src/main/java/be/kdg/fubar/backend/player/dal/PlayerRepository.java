package be.kdg.fubar.backend.player.dal;

import be.kdg.fubar.backend.player.model.Player;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlayerRepository extends JpaRepository<Player, Long> {
}
