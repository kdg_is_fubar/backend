package be.kdg.fubar.backend.notification.model;

import be.kdg.fubar.backend.room.model.Room;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.context.annotation.Lazy;

import javax.persistence.*;

@Data
@NoArgsConstructor
@Entity
@Table(name = "Notification")
public class Notification {

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter(AccessLevel.NONE)
    private Long id;

    private String message;

    @ManyToOne
    private Room room;

    public Notification(String message,Room room) {
        this.message = message;
        this.room = room;
    }
}
