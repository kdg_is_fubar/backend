package be.kdg.fubar.backend.notification.dal;

import be.kdg.fubar.backend.notification.model.Notification;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NotificationRepository extends JpaRepository<Notification, Long> {
}
