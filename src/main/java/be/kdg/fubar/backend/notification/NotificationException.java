package be.kdg.fubar.backend.notification;

public class NotificationException extends Exception {
    public NotificationException(String message) {
        super(message);
    }
}
