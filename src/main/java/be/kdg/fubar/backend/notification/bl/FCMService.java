package be.kdg.fubar.backend.notification.bl;

import be.kdg.fubar.backend.notification.NotificationException;
import be.kdg.fubar.backend.notification.model.FIREBASEMESSAGETYPE;
import be.kdg.fubar.backend.notification.config.FirebaseConfig;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.*;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.concurrent.ExecutionException;

@Service
public class FCMService {
    /**
     * Will initialize the Google Credentials for the application.
     * @param settings The settings of Firebase
     */
    public FCMService(FirebaseConfig settings) throws NotificationException {
        Path p = Paths.get(settings.getServiceAccountFile());
        try {
            FirebaseOptions options = new FirebaseOptions.Builder()
                    .setCredentials(GoogleCredentials.fromStream(new ClassPathResource("/firebaseFile.json").getInputStream()))
                    .build();
            if(FirebaseApp.getApps().isEmpty()) {
                FirebaseApp.initializeApp(options);
            }
        } catch (IOException e) {
            throw new NotificationException("Notification could not be sent");
        }
    }

    public void send(String body, String destination, FIREBASEMESSAGETYPE type)
            throws InterruptedException, ExecutionException {
        Message message = null;
        if (type.equals(FIREBASEMESSAGETYPE.TOPIC)) {
             message = Message.builder().setTopic(destination)
                    .setWebpushConfig(WebpushConfig.builder().putHeader("ttl", "300")
                            .setNotification(new WebpushNotification("Carcasonne-FUBAR",
                                    body, "mail2.png"))
                            .build())
                    .build();
        } else if (type.equals(FIREBASEMESSAGETYPE.USER)) {
            message = Message.builder().setToken(destination)
                    .setWebpushConfig(WebpushConfig.builder().putHeader("ttl", "300")
                            .setNotification(new WebpushNotification("Carcasonne-FUBAR",
                                    body, "mail2.png"))
                            .build())
                    .build();
        }
        String response = FirebaseMessaging.getInstance().sendAsync(message).get();
        System.out.println("Notification sent");
    }

    public void subscribe(String topic, String clientToken) throws NotificationException {
        try {
            TopicManagementResponse response = FirebaseMessaging.getInstance()
                    .subscribeToTopicAsync(Collections.singletonList(clientToken), topic).get();
            System.out
                    .println(response.getSuccessCount() + " tokens were subscribed successfully");
        }
        catch (InterruptedException | ExecutionException e) {
            throw new NotificationException("Notification could not be sent");
        }
    }
}
