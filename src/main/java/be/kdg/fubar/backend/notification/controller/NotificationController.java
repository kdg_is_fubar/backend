package be.kdg.fubar.backend.notification.controller;

import be.kdg.fubar.backend.identity.bl.UserService;
import be.kdg.fubar.backend.identity.model.ApplicationUser;
import be.kdg.fubar.backend.identity.util.UserUtil;
import be.kdg.fubar.backend.notification.bl.FCMService;
import be.kdg.fubar.backend.notification.bl.NotificationService;
import be.kdg.fubar.backend.notification.model.NotificationDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/notification")
@RestController
public class NotificationController {

    private NotificationService notificationService;
    private UserUtil userUtil;
    private final SimpMessagingTemplate toSendMessage;
    private final UserService userService;

    @Autowired
    public NotificationController(NotificationService notificationService, UserUtil userUtil, SimpMessagingTemplate toSendMessage, UserService userService) {
        this.notificationService = notificationService;
        this.userUtil = userUtil;
        this.toSendMessage = toSendMessage;
        this.userService = userService;
    }

    @GetMapping("")
    public List<NotificationDTO> getNotifications(@RequestHeader("Authorization") String token) {
        return this.notificationService.getNotifications(userUtil.getUserByToken());
    }

    @GetMapping("/delete/{id}")
    public void deleteNotification(@PathVariable("id") Long id, @RequestHeader("Authorization") String token) {
        this.notificationService.removeNotification(userUtil.getUserByToken(), id);
    }

    /**
     * Registration end point of the user's token from fireabase, this will be stored and used later when the user needs to be notified of an action on the site.
     * @param token The Firebase Token of the user
     */
    @PostMapping("/register")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void register(@RequestBody String token) {
        ApplicationUser user = userService.loadUserByUsername(userUtil.getUserByToken());
        user.setNotificationToken(token);
        userService.updateUser(user);
    }




}
