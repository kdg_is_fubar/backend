package be.kdg.fubar.backend.notification.bl;

import be.kdg.fubar.backend.identity.bl.UserService;
import be.kdg.fubar.backend.identity.model.ApplicationUser;
import be.kdg.fubar.backend.notification.dal.NotificationRepository;
import be.kdg.fubar.backend.notification.model.FIREBASEMESSAGETYPE;
import be.kdg.fubar.backend.notification.model.Notification;
import be.kdg.fubar.backend.notification.model.NotificationDTO;
import be.kdg.fubar.backend.room.model.Room;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

@Service
public class NotificationService {

    private final NotificationRepository notificationRepository;
    private final UserService userService;
    private SimpMessagingTemplate toSendMessage;
    private FCMService FCMService;

    @Autowired
    public NotificationService(UserService userService, SimpMessagingTemplate toSendMessage, FCMService FCMService, NotificationRepository notificationRepository) {
        this.userService = userService;
        this.toSendMessage = toSendMessage;
        this.FCMService = FCMService;
        this.notificationRepository = notificationRepository;
    }


    public Optional<Notification> getNotification(Long id) {
        return notificationRepository.findById(id);

    }

    public List<NotificationDTO> getNotifications(String username) {
        return this.userService.loadUserByUsername(username).getNotifications().stream().map(this::parseToDTO).collect(Collectors.toList());
    }

    public void removeNotification(String username, Long notificationId) {
        ApplicationUser applicationUser = this.userService.loadUserByUsername(username);
        getNotification(notificationId).ifPresent(notification ->
        {
            List<Notification> notifications = applicationUser.getNotifications();
            notifications.remove(notification);
            applicationUser.setNotifications(notifications);
            userService.updateUser(applicationUser);
        });
    }


    public Optional<Notification> createNotification(String username, String message, Room room) {
        ApplicationUser applicationUser = this.userService.loadUserByUsername(username);
        applicationUser.getNotifications().add(new Notification(message, room));
        applicationUser = this.userService.updateUser(applicationUser);
        return applicationUser.getNotifications().stream().filter(notify -> notify.getMessage().equals(message)).findFirst();
    }


    private NotificationDTO parseToDTO(Notification notification) {
        NotificationDTO dto = new NotificationDTO();
        dto.setId(notification.getId());
        dto.setMessage(notification.getMessage());
        if (notification.getRoom() != null) {
            dto.setRoomId(notification.getRoom().getId() == null ? 0 : notification.getRoom().getId());
            dto.setRoomName(notification.getRoom().getName() == null ? "noRoom" : notification.getRoom().getName());
            dto.setStarted(notification.getRoom().isGameStarted());
        }
        return dto;
    }

    public void sendNotifications(String receiver, String payload, Room room, FIREBASEMESSAGETYPE messageType) {
        toSendMessage.convertAndSendToUser(receiver, "/notification", payload);
        createNotification(receiver, payload, room);
        try {
            ApplicationUser user = userService.loadUserByUsername(receiver);
            if (user.getNotificationToken() != null) {
                FCMService.send(payload, user.getNotificationToken(), messageType);
            }
        } catch (InterruptedException | ExecutionException e) {
            System.err.println("Message could not be sent to Firebase");
        }
    }
}
