package be.kdg.fubar.backend.notification.model;

/**
 * You can send two types of messages on Firebase, either per user via token or via a topic, this will indicate which contruction to use
 * to build the notification.
 */
public enum FIREBASEMESSAGETYPE {
    USER,TOPIC
}
