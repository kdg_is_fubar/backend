package be.kdg.fubar.backend.notification.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class NotificationDTO {
    private Long id;
    private String message;
    private Long roomId;
    private String roomName;
    private String username;
    private boolean isStarted;


}
