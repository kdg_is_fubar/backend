package be.kdg.fubar.backend.round.bl;

import be.kdg.fubar.backend.player.model.Player;
import be.kdg.fubar.backend.room.bl.RoomService;
import be.kdg.fubar.backend.room.model.Room;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Component
public class RoundService {
    private final Random random;

    @Autowired
    public RoundService() {
        this.random = new Random();
    }

    public Room setupGame(Room room) {
        Player player = getRandomPlayerToStart(room);
        room.setCurrentPlayer(player);
        room.setGameStarted(true);
        return room;
    }

    public Room endRound(Room room) {
        room.getCurrentPlayer().setDrawnTile(null);
        room.nextPlayer();
        room.getCurrentPlayer().setPlaced(false);
        return room;
    }

    /**
     * This method will choose a random player to start in the game
     * @param room The room which has just been started
     * @return The player who will start the game.
     */
    private Player getRandomPlayerToStart(Room room) {
        List<Player> players = room
                .getPlayers()
                .stream()
                .filter(player -> !player.getUsername().matches("AI[0-8]"))
                .collect(Collectors.toList());
        int randomInt=random.nextInt(players.size());
        room.setIndexOfFirstTPlay((byte)randomInt);
        return players.get(randomInt);
    }
}
