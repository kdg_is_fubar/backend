package be.kdg.fubar.backend.flooding.model;

import be.kdg.fubar.backend.tile.model.SurfaceType;
import be.kdg.fubar.backend.tile.model.TileSegmentPart;
import lombok.Data;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;

@Data
public class InfoObject {
    private SurfaceType surfaceType;
    private List<AbstractMap.SimpleEntry<CoordinateDTO, TileSegmentPart>> simpleEntries =new ArrayList<>();
}
