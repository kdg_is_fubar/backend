package be.kdg.fubar.backend.flooding.bl;

import be.kdg.fubar.backend.flooding.model.CoordinateDTO;
import be.kdg.fubar.backend.flooding.model.InfoObject;
import be.kdg.fubar.backend.follower.model.Follower;
import be.kdg.fubar.backend.player.PlayerException;
import be.kdg.fubar.backend.player.bl.PlayerService;
import be.kdg.fubar.backend.player.model.Player;
import be.kdg.fubar.backend.tile.model.SurfaceType;
import be.kdg.fubar.backend.tile.model.TileSegmentPart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

@Component
public class ScoreCalculator {
    @Autowired
    @Lazy
    private PlayerService playerService;
    @Autowired
    @Lazy
    private FloodingService floodingService;

    /**
     * This method will return the size of a certain zone.
     * A zone is a collection of connected FIELD/CITY/MONASTERY/ROAD tiles
     * @param zone a list of zones which consists of a TileSegmentPart which are on a certain Coordinate
     * @return Will return the total size of the zone.
     */
    public byte getNumberOfTilesInZone(List<AbstractMap.SimpleEntry<CoordinateDTO, TileSegmentPart>> zone) {
        List<AbstractMap.SimpleEntry<Integer, Integer>> tileCoords = new ArrayList<>();
        zone.forEach(k -> {
            AbstractMap.SimpleEntry<Integer, Integer> coord = new AbstractMap.SimpleEntry<>(k.getKey().getTileX(), k.getKey().getTileY());
            if (!tileCoords.contains(coord)) {
                tileCoords.add(coord);
            }
        });
        return (byte) tileCoords.size();
    }

    /**
     * This method will return the score the player has earned if the zone was a Road.
     * @param zone List of connected Road TileSegmentParts which are present on certain coordinates
     * @return Will return the score the player has earned.
     */
    public byte scoreRoad(List<AbstractMap.SimpleEntry<CoordinateDTO, TileSegmentPart>> zone) {
        return getNumberOfTilesInZone(zone);
    }
    /**
     * This method will return the score the player has earned if the zone was a City.
     * @param zone List of connected City TileSegmentParts which are present on certain coordinates
     * @return Will return the score the player has earned.
     */
    public byte scoreCity(List<AbstractMap.SimpleEntry<CoordinateDTO, TileSegmentPart>> zone) {
        AtomicInteger score = new AtomicInteger(getNumberOfTilesInZone(zone));
        boolean multiplier = zone.get(0).getValue().isCompleted();
        zone.forEach(k -> {
            if (k.getValue().isHasBonusPoints()) score.updateAndGet(v -> (v + 1));
        });
        if (multiplier) score.updateAndGet(v -> (v * 2));
        return (byte) score.get();
    }

    /**
     * This method will return the score the player has earned when the game is finished and the monastery is not yet finished
     * or when the monastery has been finished.
     * @param infoObjectMap This Map consists of an adjacencyIdentifier as key which will indicate which parts together form a whole thing
     *                      and an InfoObject as value which consists of the SurfaceType and a list of zones.
     * @param monasteryCoordinates This is the coordinates of where the monastery is present.
     * @return This will return the score the player has earned for completing a monastery or when the game has ended.
     */
    public byte scoreMonestary(TreeMap<Short, InfoObject> infoObjectMap, CoordinateDTO monasteryCoordinates) {
        AtomicInteger counter = new AtomicInteger();
        infoObjectMap.values().forEach(inf -> inf.getSimpleEntries().forEach(entry -> {
            int deltaX = Math.abs(Math.abs(entry.getKey().getTileX()) - Math.abs(monasteryCoordinates.getTileX()));
            int deltaY = Math.abs(Math.abs(entry.getKey().getTileY()) - Math.abs(monasteryCoordinates.getTileY()));
            //count every Tile that is only 1 tile away
            if ((deltaX == 0 || deltaX == 1) && (deltaY == 0 || deltaY == 1)) {
                counter.getAndIncrement();
            }
        }));
        return (byte) (counter.get() / 81);
    }

    /**
     * This method will find the majorityShareholder of certain zone, depending on who is the majority shareholder he/she will get more points.
     * @param zone Which zone needs to be evaluated
     * @return Will return the player with the most meeples on the evaluated zone.
     */
    public List<Player>/*0-#Players*/ findMayorityShareHolder(List<AbstractMap.SimpleEntry<CoordinateDTO, TileSegmentPart>> zone) {
        List<Player> majorityShareholders = new ArrayList<>();
        Map<Player, Byte> playersWithAmount = new HashMap<>();
        zone.forEach(item -> {
            Follower follower = (item.getValue().getFollower());
            if (follower != null) {
                Player foundPlayer = null;
                try {
                    foundPlayer = playerService.getPlayer(follower.getPlayerId());
                } catch (PlayerException e) {
                    e.printStackTrace();
                }
                if (playersWithAmount.containsKey(foundPlayer)) {
                    playersWithAmount.put(foundPlayer, (byte) (playersWithAmount.get(foundPlayer) + 1));
                } else {
                    playersWithAmount.put(foundPlayer, (byte) 1);
                }
            }
        });
        Optional<Byte> max = playersWithAmount.values().stream().max(Byte::compare);
        max.ifPresent(aByte -> playersWithAmount.entrySet().stream()
                .filter(k -> k.getValue().equals(aByte))
                .forEach(k -> {
                    majorityShareholders.add(k.getKey());
                }));
        return majorityShareholders;

    }

    /**
     * This method calculates the earned score for a field for a player
     * @param zone The zone that needs to be scored
     * @param matrix The board in matrix format
     * @return The score the player has earned.
     */
    public byte scoreField(List<AbstractMap.SimpleEntry<CoordinateDTO, TileSegmentPart>> zone, TileSegmentPart[][][][][][] matrix) {
        Set<Short> adjacentIdentifiers = new HashSet<>();
        zone.forEach(k -> {
            Map<CoordinateDTO, TileSegmentPart> neighbours = floodingService.getNeighbourParts(k.getKey(), matrix);
            neighbours.forEach((j, m) -> {
                if (m.getType().equals(SurfaceType.CITY) && m.isCompleted()) {
                    adjacentIdentifiers.add(m.getAdjecencyIdentifier());
                }
            });
        });
        return (byte) (adjacentIdentifiers.size() * 3);
    }


}
