package be.kdg.fubar.backend.flooding.bl;

import be.kdg.fubar.backend.flooding.model.CoordinateDTO;
import be.kdg.fubar.backend.flooding.model.InfoObject;
import be.kdg.fubar.backend.follower.model.Follower;
import be.kdg.fubar.backend.player.PlayerException;
import be.kdg.fubar.backend.player.model.Player;
import be.kdg.fubar.backend.player.bl.PlayerService;
import be.kdg.fubar.backend.room.model.Room;
import be.kdg.fubar.backend.tile.model.SurfaceType;
import be.kdg.fubar.backend.tile.model.Tile;
import be.kdg.fubar.backend.tile.model.TileSegment;
import be.kdg.fubar.backend.tile.model.TileSegmentPart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

@Service
public class ScoreService {
    @Lazy
    @Autowired
    private FloodingService floodingService;
    private final PlayerService playerService;
    private final ScoreCalculator scoreCalculator;

    @Autowired
    public ScoreService(PlayerService playerService, ScoreCalculator scoreCalculator) {
        this.playerService = playerService;
        this.scoreCalculator = scoreCalculator;
    }

    /**
     * This method will translate the board in matrix format into a bunch of InfoObjects which consists of which identifier
     * has which kind of zone (CITY/FIELD/MONASTERY/ROAD) and the coordinates + TileSegmentParts of the zone.
     * @param matrix The board in matrix format.
     * @return A map with key = adjacencyIdentifier and an InfoObject as value
     */
    public TreeMap<Short, InfoObject> matrixToInfoObjectMap(TileSegmentPart[][][][][][] matrix) {
        TreeMap<Short, InfoObject> segmentPartAllInfo = new TreeMap<>();
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                for (int k = 0; k < matrix[i][j].length; k++) {
                    for (int l = 0; l < matrix[i][j][k].length; l++) {
                        for (int m = 0; m < matrix[i][j][k][l].length; m++) {
                            for (int n = 0; n < matrix[i][j][k][l][m].length; n++) {
                                if (matrix[i][j][k][l][m][n] != null) {
                                    //For easy use
                                    TileSegmentPart segmentPart = matrix[i][j][k][l][m][n];
                                    //Say which adjecency identifiers exist and which type they belong to
                                    //Put literally everything that can help with identifying what is around that part in one variable
                                    segmentPartAllInfo.computeIfAbsent(segmentPart.getAdjecencyIdentifier(), identifierKey -> new InfoObject());
                                    segmentPartAllInfo.get(segmentPart.getAdjecencyIdentifier()).setSurfaceType(segmentPart.getType());
                                    segmentPartAllInfo.get(segmentPart.getAdjecencyIdentifier()).getSimpleEntries()
                                            .add(new AbstractMap.SimpleEntry<>(
                                                    floodingService.shiftBack(new CoordinateDTO(i, j, k, l, m, n))
                                                    , segmentPart));

                                }
                            }
                        }
                    }
                }
            }
        }
        return segmentPartAllInfo;
    }

    /**
     * This method will set a zone to completed, a zone is scored when it is completed, a zone can only be completed once.
     * Field is never completed until the end of the game.
     * City is completed when the walls connect and there is no empty gap in between the tiles that make the connected walls.
     * Monastery is completed when there are 8 tiles surrounding the tile with the Monastery.
     * Road is completed when it has 2 endings.
     * @param zone The zone that has to be set to completed.
     */
    private void setComplete(List<AbstractMap.SimpleEntry<CoordinateDTO, TileSegmentPart>> zone) {
        zone.forEach(t -> t.getValue().setCompleted(true));
    }

    /**
     * This method will check if a City zone is completed.
     * @param zone The zone that needs to be evaluated.
     * @param matrixToSearch The board in matrix format.
     * @return The method will return true if it is completed, if not, false.
     */
    //region isComplete Checks
    public boolean isCityComplete(List<AbstractMap.SimpleEntry<CoordinateDTO, TileSegmentPart>> zone, TileSegmentPart[][][][][][] matrixToSearch) {
        AtomicBoolean completed = new AtomicBoolean(true);
        zone.forEach(t -> {
            Map<CoordinateDTO, TileSegmentPart> neighbourParts = floodingService.getNeighbourParts(t.getKey(), matrixToSearch);
            if (neighbourParts.size() != 4) {
                completed.set(false);
            }
        });
        return completed.get();
    }

    /**
     * This method will check if a Monastery zone is completed.
     * @param monasteryCoordinates The coordinate of the tile which the monastery is present in.
     * @param infoObjectMap The Objects which are present around the monastery tile.
     * @return Will return true if this is completed, else false.
     */
    private boolean isMonastaryDone(CoordinateDTO monasteryCoordinates, TreeMap<Short, InfoObject> infoObjectMap) {
        return scoreCalculator.scoreMonestary(infoObjectMap, monasteryCoordinates) == 9; //divide by 81:#number of segmentParts per tile
    }

    /**
     * This method will search for completed zones on the board.
     * @param matrixToSearch The board in matrix format.
     */
    //endregion
    public void findCompleted(TileSegmentPart[][][][][][] matrixToSearch) {
        //#1 try to find every different part that is NOT a field.
        TreeMap<Short, InfoObject> segmentPartAllInfo = matrixToInfoObjectMap(matrixToSearch);
        for (Short identifier : segmentPartAllInfo.keySet()) {
            if (identifier != 0) {
                switch (segmentPartAllInfo.get(identifier).getSurfaceType()) {
                    case CITY: {
                        //Connection between the castles
                        //Look one tile forward
                        //Is this tile a seperation between field and inner castle? (Which ones next to this one are fields? are the ones on the opposite side castles?)
                        //keep track of where I began and where I am, eventually this should loop back around.
                        //Do the walls touch anything else but another castle?
                        //Check if there is no gap
                        //If there is a Tile spaced gap between the tiles, this needs to be filled in with the "totally castle"-tile
                        //It's filled in, it's finished
                        //If it's not, then it's not
                        //If there is no tile sized hole between the walls, then it's finished
                        //If it does hit a castle, this is a wall
                        List<AbstractMap.SimpleEntry<CoordinateDTO, TileSegmentPart>> zone = segmentPartAllInfo.get(identifier).getSimpleEntries();
                        if (zoneIsAlreadyDone(zone)) break;
                        if (this.isCityComplete(zone, matrixToSearch)) {
                            this.setComplete(zone);
                        }
                        break;
                    }

                    case ROAD: {
                        // real work
                        //the amount of endpoints
                        AtomicInteger endpoints = new AtomicInteger(0);
                        // get all the tiles with a certain adjacency
                        List<AbstractMap.SimpleEntry<CoordinateDTO, TileSegmentPart>> zone = segmentPartAllInfo.get(identifier).getSimpleEntries();
                        if (zoneIsAlreadyDone(zone)) break;
                        for (AbstractMap.SimpleEntry<CoordinateDTO, TileSegmentPart> coordinateDTOTileSegmentPartSimpleEntry : zone) {
                            // if an endpoint is found it will return a 1
                            if ((nextToNoFieldOrRoad(coordinateDTOTileSegmentPartSimpleEntry.getKey(), coordinateDTOTileSegmentPartSimpleEntry.getValue(), matrixToSearch))) {
                                endpoints.incrementAndGet();
                            }
                            if (endpoints.get() == 2) {
                                this.setComplete(zone);
                            }
                        }

                        // eventually when the endpoints is > 1 the road is done and we can say this road is "finished"
                        break;
                    }

                    case MONASTERY: {
                        // get all the tiles with a certain adjacency
                        List<AbstractMap.SimpleEntry<CoordinateDTO, TileSegmentPart>> zone = segmentPartAllInfo.get(identifier).getSimpleEntries();
                        if (zoneIsAlreadyDone(zone)) break;
                        //Get the neighbours of this exact tile, then recursively see if they have tiles that are "connected" to the monastery
                        // Every function call is responsible for checking 2 tiles, themselves and the one clockwise next to them.
                        // This means the most the function can return is 2, add this to surrounded.
                        // Once surrounded is == 8, a Monastery is finished.

                        if (this.isMonastaryDone(zone.get(0).getKey(), segmentPartAllInfo)) {
                            this.setComplete(zone);
                        }
                        break;
                    }
                }
            }
        }
    }

    /**
     * Checks if a zone is already completed.
     * @param zone The zone that needs to be evaluated.
     * @return will return true if it has already been completed, else false
     */
    private boolean zoneIsAlreadyDone(List<AbstractMap.SimpleEntry<CoordinateDTO, TileSegmentPart>> zone) {
        return zone.get(0).getValue().isCompleted();
    }

    /**
     * This will see if a Road is not touching something that is a FIELD or ROAD. If this happens the road has ended on that side.
     * @param coordinatesOfTile The coordinates of the tile that is being analyzed
     * @param toAnalyzePart The part that needs to be analyzed which is or is not next to a FIELD or ROAD
     * @param matrixToSearch The board in matrix format
     * @return
     */
    private boolean nextToNoFieldOrRoad(CoordinateDTO coordinatesOfTile, TileSegmentPart toAnalyzePart, TileSegmentPart[][][][][][] matrixToSearch) {
        // if one of them touches something that isn't a field or another road return 1
        AtomicInteger endFound = new AtomicInteger(0);
        Map<CoordinateDTO, TileSegmentPart> neighbourParts = floodingService.getNeighbourParts(coordinatesOfTile, matrixToSearch);

        neighbourParts.forEach((c, n) -> {
            analyzeNeighbours(toAnalyzePart, n, endFound);
        });
        return endFound.get() == 1;
    }

    /**
     * This method will analyze the neighbour of a certain TileSegmentPart, it will check if the neighbour is a FIELD or a ROAD.
     * If it isn't then the amount of endings will be upped and returned.
     * If it is, then the road hasn't ended yet and it will just return the amount of endings without incrementing it.
     * @param toAnalyze Which TileSegmentPart we will analyze
     * @param neighbour Which neighbour we are checking
     * @param endFound The amount of ends found on a road up until now
     * @return Will return the amount of endings found up until now after analyzing the current TileSegmentPart.
     */
    private AtomicInteger analyzeNeighbours(TileSegmentPart toAnalyze, TileSegmentPart neighbour, AtomicInteger endFound) {
        if (neighbour == null)
            return endFound;
        //Is not a road
        if (!toAnalyze.getType().equals(neighbour.getType())) {
            if (!neighbour.getType().equals(SurfaceType.FIELD)) {
                endFound.incrementAndGet();
            }
        }
        return endFound;
    }

    /**
     * @param room                current room
     * @param playerId            current player
     * @param adjacencyIdentifier current 'zone' that the player wishes to place a follower i,n
     * @return yes he can; no he can't, place a follower
     * <p>
     * Check if there is already a follower in the current zone
     * ALSO checks if the current player has followers left
     */
    public boolean containsOtherFollower(Room room, Long playerId, short adjacencyIdentifier) throws PlayerException {
        Player player = playerService.getPlayer(playerId);
        Map<Short, List<TileSegmentPart>> partsWithIdentifier = getSegmentPartsWithIdentifier(room.getBoard());
        List<TileSegmentPart> zoneParts = partsWithIdentifier.get(adjacencyIdentifier);
        for (TileSegmentPart part : zoneParts) {
            Follower follower = part.getFollower();
            //If it has a follower and it's not his then there can't be placed a new one
            if (follower != null)
                return true;
        }
        return false;
    }

    /**
     * @param board current board
     * @return a map of identifiers & TileSegmentParts
     * <p>
     * Gets a full treeMap-list of all parts with AdjecencyIdentifier
     */
    private Map<Short, List<TileSegmentPart>> getSegmentPartsWithIdentifier(List<Tile> board) {
        Map<Short, List<TileSegmentPart>> result = new TreeMap<>();
        for (Tile tile : board) {
            TileSegmentPart[][][][] matrix = tile.toMatrix();
            for (int segmentX = 0; segmentX < 3; segmentX++) {
                for (int segmentY = 0; segmentY < 3; segmentY++) {
                    for (int partX = 0; partX < 3; partX++) {
                        for (int partY = 0; partY < 3; partY++) {
                            //Getting the part from the board
                            TileSegmentPart part = matrix[segmentX][segmentY][partX][partY];
                            //Getting the potentially existing array for this identifier
                            List<TileSegmentPart> parts = result.get(part.getAdjecencyIdentifier());
                            //Checking if it actually exists
                            if (parts != null)
                                //Add it to this list
                                parts.add(part);
                            else {
                                //Create a new list
                                List<TileSegmentPart> identifierList = new LinkedList<>();
                                //Adding the current part to it
                                identifierList.add(part);
                                result.put(part.getAdjecencyIdentifier(), identifierList);
                            }
                        }
                    }
                }
            }
        }
        return result;
    }

    /**
     * This method will distribute the score of a board depending on the Tile that has been placed in the current round.
     * @param board The board in matrix format
     * @param lastPlacedTile The tile that has been placed last since this method has been called
     */
    public void distributeScore(List<Tile> board, Tile lastPlacedTile) throws FloodingException {
        TileSegmentPart[][][][][][] boardMatrix = floodingService.parseToMatrix(board);
        TreeMap<Short, InfoObject> boardSorted = matrixToInfoObjectMap(boardMatrix);
        Set<Short> adjacentIdentifiers = new HashSet<>();
        for (TileSegment tileSegment : lastPlacedTile.tileSegments()) {
            for (TileSegmentPart tileSegmentPart : tileSegment.tileSegmentPart()) {
                adjacentIdentifiers.add(tileSegmentPart.getAdjecencyIdentifier());
            }
        }
        Stream<Map.Entry<Short, InfoObject>> board2 = boardSorted.entrySet().stream().filter(shortInfoObjectEntry -> adjacentIdentifiers.contains(shortInfoObjectEntry.getKey()));
        board2.forEach(shortInfoObjectEntry ->
        {
            List<Player> players = new ArrayList<>();
            int score = 0;
            switch (shortInfoObjectEntry.getValue().getSurfaceType()) {
                case CITY:
                    if (shortInfoObjectEntry.getValue().getSimpleEntries().get(0).getValue().isCompleted()) {
                        score = scoreCalculator.scoreCity(shortInfoObjectEntry.getValue().getSimpleEntries());
                        players = scoreCalculator.findMayorityShareHolder(shortInfoObjectEntry.getValue().getSimpleEntries());
                    }
                    break;
                case FIELD:
                    if (shortInfoObjectEntry.getValue().getSimpleEntries().get(0).getValue().isCompleted()) {
                        score = scoreCalculator.scoreField(shortInfoObjectEntry.getValue().getSimpleEntries(), boardMatrix);
                        players = scoreCalculator.findMayorityShareHolder(shortInfoObjectEntry.getValue().getSimpleEntries());
                    }
                    break;
                case ROAD:
                    if (shortInfoObjectEntry.getValue().getSimpleEntries().get(0).getValue().isCompleted()) {
                        score = scoreCalculator.scoreRoad(shortInfoObjectEntry.getValue().getSimpleEntries());
                        players = scoreCalculator.findMayorityShareHolder(shortInfoObjectEntry.getValue().getSimpleEntries());
                    }
                    break;
                case MONASTERY:
                    if (shortInfoObjectEntry.getValue().getSimpleEntries().get(0).getValue().isCompleted()) {
                        CoordinateDTO coordinateDTO = new CoordinateDTO();
                        coordinateDTO.setTileX(lastPlacedTile.getX());
                        coordinateDTO.setTileY(lastPlacedTile.getY());
                        score = scoreCalculator.scoreMonestary(boardSorted, coordinateDTO);
                        players = scoreCalculator.findMayorityShareHolder(shortInfoObjectEntry.getValue().getSimpleEntries());
                    }
                    break;
            }
            for (Player player : players) {
                player.addScore((byte) score);
            }
        });


    }

    /**
     * This method will set a field to complete at the end of the game.
     * @param board The board in matrix format
     */
    public void setFieldComplete(List<Tile> board) throws FloodingException {
        matrixToInfoObjectMap(floodingService.parseToMatrix(board)).forEach((aShort, infoObject) -> {
            if (infoObject.getSurfaceType().equals(SurfaceType.FIELD)) {
                infoObject.getSimpleEntries().forEach(coordinateDTOTileSegmentPartSimpleEntry ->
                        coordinateDTOTileSegmentPartSimpleEntry.getValue().setCompleted(true));
            }
        });
    }
}
