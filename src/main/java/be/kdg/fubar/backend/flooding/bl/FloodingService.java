package be.kdg.fubar.backend.flooding.bl;

import be.kdg.fubar.backend.flooding.model.CoordinateDTO;
import be.kdg.fubar.backend.flooding.model.NeighbourDirection;
import be.kdg.fubar.backend.room.model.Room;
import be.kdg.fubar.backend.tile.model.Coordinate;
import be.kdg.fubar.backend.tile.model.Tile;
import be.kdg.fubar.backend.tile.model.TileSegmentPart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.*;
import java.util.stream.Stream;

@Service
public class FloodingService {
    private int currentIdentifier;
    private final ScoreService scoreService;
    private int transpositionX;
    private int transpositionY;
    private Set<Coordinate> emptyNeighbours;

    @Autowired
    public FloodingService(ScoreService scoreService) {
        this.scoreService = scoreService;
    }

    //region floo

    /**
     * This function will flood over all tiles, then segments, then parts of the board,
     * only in function of the last placed tile, not continuing when the same area, or 'zone', ends,
     * but restarting for all Parts of the last placed tile that haven't been flooded yet,
     * overwriting adjecency identifiers to create zones for placement and scoring.
     *
     * @param room         the current room that the function runs in.
     * @param xFloodOrigin x position of the tile where the flooding will start from.
     * @param yFloodOrigin y position of the tile where the flooding will start from.
     * @return Returns the same board with the adjacency identifiers filled in.
     */
    public void flood(Room room, int xFloodOrigin, int yFloodOrigin) throws FloodingException {
        List<Tile> board = room.getBoard();
        //Initializing local version of emptyNeighbours
        emptyNeighbours = new HashSet<>();
        room.getEmptyNeighbours().removeAll(room.getEmptyNeighbours());
        //Clearing the existing set for read consistency
        //[TileX][TileY][SegmentX][SegmentY][PartX][PartY]
        TileSegmentPart[][][][][][] matrix = parseToMatrix(board);
        for (int segmentX = 0; segmentX < 3; segmentX++) {
            for (int segmentY = 0; segmentY < 3; segmentY++) {
                for (int partX = 0; partX < 3; partX++) {
                    for (int partY = 0; partY < 3; partY++) {
                        currentIdentifier = room.getCurrentAdjecencyIdentifier();
                        CoordinateDTO currentPoint = new CoordinateDTO(xFloodOrigin, yFloodOrigin, segmentX, segmentY, partX, partY);
                        floodZone(matrix, currentPoint);
                        resetFlooded(matrix);
                        room.setCurrentAdjecencyIdentifier(room.getCurrentAdjecencyIdentifier() + 1);
                    }
                }
            }
        }
        scoreService.findCompleted(matrix);
        //Setting the whole neighboursEmptyNeighbours object as one transaction
        room.getEmptyNeighbours().addAll(emptyNeighbours);
    }

    /**
     * This method will reset the flooding variable on every tile.
     * @param matrix the board in matrix format.
     */
    private void resetFlooded(TileSegmentPart[][][][][][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                for (int k = 0; k < matrix[i][j].length; k++) {
                    for (int l = 0; l < matrix[i][j][k].length; l++) {
                        for (int m = 0; m < matrix[i][j][k][l].length; m++) {
                            for (int n = 0; n < matrix[i][j][k][l][m].length; n++) {
                                if (matrix[i][j][k][l][m][n] != null) {
                                    matrix[i][j][k][l][m][n].setFlooded(false);
                                }
                            }

                        }

                    }
                }
            }
        }
    }


    /**
     * This function will calculate the difference between the x an y axis and 0 if they go below 0.
     * his is done to prevent index out of bounds in the matrix.
     *
     * @param board a List of all tiles that will be in the matrix.
     * @code transpositionX will be changed
     * @code transpositionY will be changed
     */
    private void setTrans(List<Tile> board) throws FloodingException {
        int minX = getMin(board.stream().map(Tile::getX));
        int minY = getMin(board.stream().map(Tile::getY));
        final int transpositionX;
        if (minX < 0) transpositionX = Math.abs(minX);
        else transpositionX = 0;
        final int transpositionY;
        if (minY < 0) transpositionY = Math.abs(minY);
        else transpositionY = 0;
        this.transpositionX = transpositionX;
        this.transpositionY = transpositionY;
    }

    /**
     * This method will analyze a coordinate and return the neighbours of that TileSegmentPart
     * @param toAnalyzeCoordinates Which coordinate we will analyze around
     * @param matrixToSearch The board we have to analyze in matrix format
     * @return
     */
    public Map<CoordinateDTO, TileSegmentPart> getNeighbourParts(CoordinateDTO toAnalyzeCoordinates, TileSegmentPart[][][][][][] matrixToSearch) {
        Map<CoordinateDTO, TileSegmentPart> neighbourParts = new HashMap<>();
        List<CoordinateDTO> neighbourCoords = new LinkedList<>();

        neighbourCoords.add(getNeighbour(toAnalyzeCoordinates, NeighbourDirection.UP));
        neighbourCoords.add(getNeighbour(toAnalyzeCoordinates, NeighbourDirection.RIGHT));
        neighbourCoords.add(getNeighbour(toAnalyzeCoordinates, NeighbourDirection.DOWN));
        neighbourCoords.add(getNeighbour(toAnalyzeCoordinates, NeighbourDirection.LEFT));

        neighbourCoords.forEach(c -> {
            Optional<TileSegmentPart> segmentPart = getSegmentPart(matrixToSearch, c);
            segmentPart.ifPresent(tileSegmentPart -> neighbourParts.put(c, tileSegmentPart));
            if (!segmentPart.isPresent()) {
                emptyNeighbours.add(new Coordinate(c.getTileX(), c.getTileY()));
            }
        })
        ;
        return neighbourParts;
    }

    /* TODO REMOVE?
      private AbstractMap.SimpleEntry<CoordinateDTO, TileSegmentPart> getNeighbourTiles(CoordinateDTO monasteryCoordinates, TileSegmentPart[][][][][][] matrixToSearch, NeighbourDirection direction) {
           AbstractMap.SimpleEntry<CoordinateDTO, TileSegmentPart> neighbouringTiles = null;
           switch (direction) {
               case UP: {
                   CoordinateDTO coordsUP = new CoordinateDTO(monasteryCoordinates);
                   coordsUP.setTileY(coordsUP.getTileY() - 1);
                   neighbouringTiles = new AbstractMap.SimpleEntry<CoordinateDTO, TileSegmentPart>(coordsUP, getSegmentPart(matrixToSearch, coordsUP));
                   break;
               }
               case RIGHT: {
                   CoordinateDTO coordsRIGHT = new CoordinateDTO(monasteryCoordinates);
                   coordsRIGHT.setTileX(coordsRIGHT.getTileX() + 1);
                   neighbouringTiles = new AbstractMap.SimpleEntry(coordsRIGHT, getSegmentPart(matrixToSearch, coordsRIGHT));
                   break;
               }
               case DOWN: {
                   CoordinateDTO coordsDOWN = new CoordinateDTO(monasteryCoordinates);
                   coordsDOWN.setTileY(coordsDOWN.getTileY() + 1);
                   neighbouringTiles = new AbstractMap.SimpleEntry(coordsDOWN, getSegmentPart(matrixToSearch, coordsDOWN));
                   break;
               }
               case LEFT: {
                   CoordinateDTO coordsLEFT = new CoordinateDTO(monasteryCoordinates);
                   coordsLEFT.setTileX(coordsLEFT.getTileX() - 1);
                   neighbouringTiles = new AbstractMap.SimpleEntry(coordsLEFT, getSegmentPart(matrixToSearch, coordsLEFT));
                   break;
               }
           }
           return neighbouringTiles;
       }
   */
    public void printMatrix(TileSegmentPart[][][][][][] matrixToPrint) {
        for (int i = 0; i < matrixToPrint.length; i++) {
            for (int j = 0; j < matrixToPrint[i].length; j++) {
                for (int k = 0; k < matrixToPrint[i][j].length; k++) {
                    for (int l = 0; l < matrixToPrint[i][j][k].length; l++) {
                        for (int m = 0; m < matrixToPrint[i][j][k][l].length; m++) {
                            for (int n = 0; n < matrixToPrint[i][j][k][l][m].length; n++) {
                                if (matrixToPrint[i][j][k][l][m][n] != null) {
                                    System.out.print(matrixToPrint[i][j][l][k][n][m].getAdjecencyIdentifier() + " ");
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * @param matrix the board from 'this' room, parsed to a matrix
     * @param coords the coordinates of the current point (TileSegmentPart-level)
     *               <p>
     *               If the tile isn't flooded yet, set it to flooded to prevent backflow & set the adjacency identifier.
     *               Use the neighbourCoordinates to get the neighbour parts,
     *               Loop over them, checking if the parts are equal (SurfaceType)
     *               Finally call this function recursivly for the equal neighbours
     */
    private void floodZone(TileSegmentPart[][][][][][] matrix, CoordinateDTO coords) {
        TileSegmentPart self =
                matrix[coords.getTileX() + transpositionX][coords.getTileY() + transpositionY]
                        [coords.getSegmentX()][coords.getSegmentY()]
                        [coords.getSegmentPartX()][coords.getSegmentPartY()];

        if (self.isFlooded())
            return;

        self.setFlooded(true);
        self.setAdjecencyIdentifier(currentIdentifier);

        Map<CoordinateDTO, TileSegmentPart> neighbourParts = getNeighbourParts(coords, matrix);

        neighbourParts.forEach((c, n) -> {
            if (self.getType().equals(n.getType())) {
                n.setAdjecencyIdentifier(currentIdentifier); // TODO remove: only set self? (enkel u eigen identifier kunnen zetten)
                floodZone(matrix, c);
            }
        });
    }

    /**
     * @param coordsSelf coordinates of the current part, who's neighbour we're looking for
     * @param direction  what direction the neighbour is, relative to the current part
     * @return CoordinateDTO of the neighbour
     * <p>
     * For the specific direction,
     * check if you're going OVER a sigment or tile border & set the neighbour accordingly.
     */
    public CoordinateDTO getNeighbour(CoordinateDTO coordsSelf, NeighbourDirection direction) {
        CoordinateDTO neighbour = new CoordinateDTO(coordsSelf);
        int transPart, transSegment;
        switch (direction) {
            case UP:
                //Calculating new coordinate for the part
                transPart = coordsSelf.getSegmentPartY() - 1;
                //Checking if we crossed the border of a segment
                if (transPart < 0) {
                    //Calculating new coordinate for the segment (we crossed it)
                    transSegment = coordsSelf.getSegmentY() - 1;
                    //Checking if we crossed a tile
                    if (transSegment < 0) {
                        //Setting the segment to the bottom one (we crossed a tile)
                        neighbour.setSegmentY(2);
                        neighbour.setTileY(coordsSelf.getTileY() + 1);
                    } else {
                        //Setting the segment to it's new value. (we didn't cross a tile)
                        neighbour.setSegmentY(transSegment);
                    }
                    neighbour.setSegmentPartY(2);
                } else
                    neighbour.setSegmentPartY(transPart);
                break;
            case RIGHT:
                //Calculating new coordinate for the part
                transPart = coordsSelf.getSegmentPartX() + 1;
                //Checking if we crossed the border of a segment
                if (transPart > 2) {
                    //Calculating new coordinate for the segment (we crossed it)
                    transSegment = coordsSelf.getSegmentX() + 1;
                    //Checking if we crossed a tile
                    if (transSegment > 2) {
                        //Setting the segment to the bottom one (we crossed a tile)
                        neighbour.setSegmentX(0);
                        neighbour.setTileX(coordsSelf.getTileX() + 1);
                    } else {
                        //Setting the segment to it's new value. (we didn't cross a tile)
                        neighbour.setSegmentX(transSegment);
                    }
                    neighbour.setSegmentPartX(0);
                } else
                    neighbour.setSegmentPartX(transPart);
                break;
            case DOWN:
                transPart = coordsSelf.getSegmentPartY() + 1;
                if (transPart > 2) {
                    transSegment = coordsSelf.getSegmentY() + 1;
                    if (transSegment > 2) {
                        neighbour.setSegmentY(0);
                        neighbour.setTileY(coordsSelf.getTileY() - 1);
                    } else {
                        neighbour.setSegmentY(transSegment);
                    }
                    neighbour.setSegmentPartY(0);
                } else {
                    neighbour.setSegmentPartY(transPart);
                }
                break;
            case LEFT:
                transPart = coordsSelf.getSegmentPartX() - 1;
                if (transPart < 0) {
                    transSegment = coordsSelf.getSegmentX() - 1;
                    if (transSegment < 0) {
                        neighbour.setSegmentX(2);
                        neighbour.setTileX(coordsSelf.getTileX() - 1);
                    } else {
                        neighbour.setSegmentX(transSegment);
                    }
                    neighbour.setSegmentPartX(2);
                } else {
                    neighbour.setSegmentPartX(transPart);
                }
                break;
        }
        return neighbour;
    }

    /**
     * This method will return a TileSegmentPart which is present on a certain coordinate on the board.
     * @param matrix The board in matrix format
     * @param coords The coordinates of the returned TileSegmentPart
     * @return The TileSegmentPart of on a certain coordinate.
     */
    private Optional<TileSegmentPart> getSegmentPart(TileSegmentPart[][][][][][] matrix, CoordinateDTO coords) {
        try {
            int x = coords.getTileX() + transpositionX;
            int y = coords.getTileY() + transpositionY;
            int x2 = coords.getSegmentX();
            int y2 = coords.getSegmentY();
            int x3 = coords.getSegmentPartX();
            int y3 = coords.getSegmentPartY();
            if (matrix[x][y][x2][y2][x3][y3] != null)
                return Optional.of(matrix[coords.getTileX() + transpositionX][coords.getTileY() + transpositionY][coords.getSegmentX()][coords.getSegmentY()][coords.getSegmentPartX()][coords.getSegmentPartY()]);
        } catch (IndexOutOfBoundsException iob) {
            return Optional.empty();
        }
        return Optional.empty();
    }

    /**
     * @param board current board
     * @return the freshly created matrix
     * <p>
     * Parsing the board to a matrix, (=> translate from 0,0 for the START-Tile to 0,0 for the LEFT-UP-MOST-Tile)
     * calls the toMatrix of the tile,
     * which in turn gets calls the segments.toMatrix(),
     * that gets the Parts of each segments
     */
    public TileSegmentPart[][][][][][] parseToMatrix(List<Tile> board) throws FloodingException {
        setTrans(board);
        int minX = getMin(board.stream().map(Tile::getX));
        int minY = getMin(board.stream().map(Tile::getY));
        int maxX = getMax(board.stream().map(Tile::getX));
        int maxY = getMax(board.stream().map(Tile::getY));
        int deltaX = (maxX - minX) + 1;
        int deltaY = (maxY - minY) + 1;
        TileSegmentPart[][][][][][] matrix = new TileSegmentPart[deltaX][deltaY][3][3][3][3];
        board.forEach(tile -> matrix[tile.getX() + transpositionX][tile.getY() + transpositionY] = tile.toMatrix());
        return matrix;
    }

    /**
     * @param stream board in stream version (x & y)
     * @return highest int (x/y) in the stream
     * <p>
     * Finds the highest value for the given stream, used  in parseToMatrix()
     */
    private int getMax(Stream<Integer> stream) throws FloodingException {
        Optional<Integer> maxOptional = stream.max(Integer::compareTo);
        if (!maxOptional.isPresent())
            throw new FloodingException("Invalid board");
        return maxOptional.get();
    }

    /**
     * @param stream board in stream version (x & y)
     * @return lowest int (x/y) in the stream
     * <p>
     * Finds the lowest value for the given stream, used in parseToMatrix()
     */
    private int getMin(Stream<Integer> stream) throws FloodingException {
        Optional<Integer> minOptional = stream.min(Integer::compareTo);
        if (!minOptional.isPresent())
            throw new FloodingException("Invalid board");
        return minOptional.get();
    }

    //endregion

    /**
     * @param tile the tile that we want to know the adjacency identifiers
     * @return set of the adjacency identifiers
     * <p>
     * Gets the adjacency identifiers.
     * <p>
     * Adjacency identifiers are used to determine:
     * what parts belong to a single zone,
     * weather or not a follower may be placed on a zone,
     * and partly to check when a zone is completed
     */
    public Set<Short> getAdjacencyIdentifiers(Tile tile) {
        TileSegmentPart[][][][] matrix = tile.toMatrix();
        Set<Short> identifiers = new HashSet<>();
        for (int segmentX = 0; segmentX < 3; segmentX++) {
            for (int segmentY = 0; segmentY < 3; segmentY++) {
                for (int partX = 0; partX < 3; partX++) {
                    for (int partY = 0; partY < 3; partY++) {
                        identifiers.add(matrix[segmentX][segmentY][partX][partY].getAdjecencyIdentifier());
                    }
                }
            }
        }
        return identifiers;
    }

    public CoordinateDTO shiftBack(CoordinateDTO coordinateDTO) {
        coordinateDTO.setTileX(coordinateDTO.getTileX() - transpositionX);
        coordinateDTO.setTileY(coordinateDTO.getTileY() - transpositionY);
        return coordinateDTO;
    }


}