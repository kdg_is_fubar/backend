package be.kdg.fubar.backend.flooding.model;

public enum NeighbourDirection {
    UP, RIGHT, DOWN, LEFT
}
