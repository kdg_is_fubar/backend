package be.kdg.fubar.backend.flooding.bl;

public class FloodingException extends Exception {
    public FloodingException(String message) {
        super(message);
    }
}
