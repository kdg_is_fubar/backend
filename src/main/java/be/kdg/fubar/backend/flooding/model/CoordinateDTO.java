package be.kdg.fubar.backend.flooding.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CoordinateDTO {
    private int tileX, tileY, segmentX, segmentY, segmentPartX, segmentPartY;

    public CoordinateDTO(CoordinateDTO coordinateDTO){
        this.tileX = coordinateDTO.getTileX();
        this.tileY = coordinateDTO.getTileY();
        this.segmentX = coordinateDTO.getSegmentX();
        this.segmentY = coordinateDTO.getSegmentY();
        this.segmentPartX = coordinateDTO.getSegmentPartX();
        this.segmentPartY = coordinateDTO.getSegmentPartY();
    }

}
