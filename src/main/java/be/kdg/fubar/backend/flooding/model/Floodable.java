package be.kdg.fubar.backend.flooding.model;

public interface Floodable<T> {
    T toMatrix();
}
