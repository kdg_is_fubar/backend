package be.kdg.fubar.backend.room.controller;

import be.kdg.fubar.backend.flooding.bl.FloodingException;
import be.kdg.fubar.backend.identity.bl.UserException;
import be.kdg.fubar.backend.identity.bl.UserService;
import be.kdg.fubar.backend.player.bl.PlayerService;
import be.kdg.fubar.backend.player.model.Player;
import be.kdg.fubar.backend.room.RoomException;
import be.kdg.fubar.backend.room.bl.RoomService;
import be.kdg.fubar.backend.room.model.JoinRoomDTO;
import be.kdg.fubar.backend.room.model.JoiningPlayer;
import be.kdg.fubar.backend.room.model.Room;
import be.kdg.fubar.backend.room.model.RoomDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RequestMapping("room")
@RestController
public class RoomController {

    private final RoomService rservice;
    private final PlayerService playerService;
    private final UserService userService;
    private final SimpMessagingTemplate toSendMessage;

    @Autowired
    public RoomController(RoomService rservice, PlayerService playerService, UserService userService, SimpMessagingTemplate simp) {
        this.rservice = rservice;
        this.playerService = playerService;
        this.userService = userService;
        toSendMessage = simp;
    }

    /**
     * Create endpoint for a room
     *
     * @param roomDTO
     */
    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    public RoomDTO createRoom(@RequestBody RoomDTO roomDTO) throws UserException, FloodingException, RoomException {
        Room room = rservice.createNewRoom(roomDTO.getName(), roomDTO.getMaxPlayerCount(), roomDTO.getAiCount(), roomDTO.getTurnTime());
        Player host = playerService.creatNewPlayer(userService.loadUserByUsername(roomDTO.getUsername()).getEmail(), room);
        host.setHost(true);
        playerService.save(host);
        rservice.saveRoom(room);
        rservice.addAI(room, roomDTO.getAiCount());
        room = rservice.getRoom(room.getId());
        return roomToRoomDTO(room);
    }

    @GetMapping("/all")
    public List<RoomDTO> getRooms() {
        return rservice.getRooms().stream().map(this::roomToRoomDTO).collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public RoomDTO getRoom(@PathVariable("id") Long id) throws RoomException {
        return roomToRoomDTO(rservice.getRoom(id));
    }

    /**
     * Websocket endpoint for joining a room
     *
     * @param inputDTO
     * @param roomId
     */
    @MessageMapping("/join/{roomId}")
    public void joinRoom(JoinRoomDTO inputDTO, @DestinationVariable Long roomId) throws Exception {
        Optional<JoiningPlayer> optionalJoiningPlayer = rservice.joinRoom(roomId, inputDTO.getUsername(), inputDTO.isJoining());
        optionalJoiningPlayer.ifPresent(joiningPlayer -> toSendMessage.convertAndSend("/joined/" + roomId, joiningPlayer));
    }

    private RoomDTO roomToRoomDTO(Room room) {
        return new RoomDTO(room.getId(),
                room.getName(),
                room.getMaxPlayerCount(),
                room.getHost().getUsername(),
                room.getAiCount(),
                room.getIndexCurrentPlayer(),
                room.getPlayers(),
                room.getTurnTime(),
                room.isGameStarted(),
                room.isEnded()
        );
    }
}