package be.kdg.fubar.backend.room.model;

public enum InviteStatus {
    INVITATION_SENT,
    NOT_REGISTERED,
    UNABLE_TO_SENT
}