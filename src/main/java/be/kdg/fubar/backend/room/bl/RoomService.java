package be.kdg.fubar.backend.room.bl;

import be.kdg.fubar.backend.ai.bl.AIService;
import be.kdg.fubar.backend.flooding.bl.FloodingException;
import be.kdg.fubar.backend.flooding.bl.FloodingService;
import be.kdg.fubar.backend.identity.bl.UserException;
import be.kdg.fubar.backend.identity.bl.UserService;
import be.kdg.fubar.backend.identity.model.ApplicationUser;
import be.kdg.fubar.backend.player.bl.PlayerService;
import be.kdg.fubar.backend.player.model.Player;
import be.kdg.fubar.backend.room.RoomException;
import be.kdg.fubar.backend.room.dal.RoomRepository;
import be.kdg.fubar.backend.room.model.JoiningPlayer;
import be.kdg.fubar.backend.room.model.Room;
import be.kdg.fubar.backend.tile.bl.TileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class RoomService {
    private final TileService tileService;
    private final PlayerService playerService;
    private final UserService userService;
    private final FloodingService floodingService;
    private final AIService aiService;
    private final RoomRepository roomRepository;

    @Autowired
    public RoomService(TileService tileService,
                       PlayerService playerService,
                       FloodingService floodingService,
                       UserService userService,
                       AIService aiService,
                       RoomRepository roomRepository) {
        this.tileService = tileService;
        this.playerService = playerService;
        this.floodingService = floodingService;
        this.userService = userService;
        this.aiService = aiService;
        this.roomRepository = roomRepository;
    }

    public void deleteRoom(Room room) {
        roomRepository.delete(room);
    }

    /**
     * This method will create and set up a room.
     * @param name the name of the room
     * @param maxPlayerCount the max amount of players
     * @param aiCount the amount of AIs in the room
     * @param millis how long a turn lasts before the AI takes over (in millis)
     * @return the created and set up room
     * @throws FloodingException An exception concerning the Flooding of the first tile
     */
    public Room createNewRoom(String name, int maxPlayerCount, int aiCount, long millis) throws FloodingException {
        Room room = new Room(name, maxPlayerCount, aiCount);
        room.getDeck().addAll(tileService.getNewDeck());
        room.getBoard().add(tileService.getStartTile());
        room.setTurnTime(millis);
        floodingService.flood(room, 0, 0);
        return saveRoom(room);
    }

    public Room getRoom(Long id) throws RoomException {
        Optional<Room> roomOptional = roomRepository.findById(id);
        if (!roomOptional.isPresent())
            throw new RoomException("Room not found");
        return roomOptional.get();
    }

    public List<Room> getRooms() {
        return roomRepository.findAll();
    }

    public Room saveRoom(Room room) {
        return roomRepository.save(room);
    }

    public Room updateRoom(Room updatedRoom) throws RoomException {
        Room toBeUpdatedRoom = getRoom(updatedRoom.getId());

        toBeUpdatedRoom.setGameStarted(updatedRoom.isGameStarted());
        toBeUpdatedRoom.setName(updatedRoom.getName());
        toBeUpdatedRoom.setPlayers(updatedRoom.getPlayers());
        toBeUpdatedRoom.setBoard(updatedRoom.getBoard());

        return saveRoom(toBeUpdatedRoom);
    }

    /**
     * This method will join a player inside a room, or remove the player out of the room.
     * @param roomId The room the player is joining
     * @param username The username of the user who is joining or leaving
     * @param joining true if joining, false if leaving
     * @return  Will return an optional of a JoiningPlayer which will be used in the Front end to add or remove the user
     * @throws UserException An exception concerning Users
     * @throws RoomException An exception concerning Rooms
     */
    public Optional<JoiningPlayer> joinRoom(Long roomId, String username, boolean joining) throws UserException, RoomException {
        Room room = getRoom(roomId);
        if (!room.isGameStarted()) {
            ApplicationUser applicationUser = userService.loadUserByUsername(username);
            if (joining) {
                if (applicationUser.getPlayers().stream().noneMatch(x -> x.getRoomId().equals(room.getId()))) {
                    Player player = playerService.creatNewPlayer(applicationUser.getEmail(), room);
                    return Optional.of(new JoiningPlayer(player, true));
                }
            } else {
                if (applicationUser.getPlayers().stream().anyMatch(x -> x.getRoomId().equals(room.getId()))) {
                    Optional<Player> player = playerService.deletePlayerFromRoom(applicationUser.getEmail(), room);
                    if (player.isPresent()) {
                        return Optional.of(new JoiningPlayer(player.get(), false));
                    }
                }
            }
        }
        return Optional.empty();
    }

    /**
     * This will delegate the method further to the AIService
     * @param room The room which AIs will be added in
     * @param aiCount The amount of AIs that has to be added inside of the room
     * @throws UserException
     */
    public void addAI(Room room, int aiCount) throws UserException {
        aiService.addAI(room, aiCount);
    }
}
