package be.kdg.fubar.backend.room.model;

import be.kdg.fubar.backend.player.model.Player;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class JoiningPlayer {
    private Player joiningPlayer;
    private boolean joining;
}
