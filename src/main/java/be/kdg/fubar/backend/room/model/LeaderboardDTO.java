package be.kdg.fubar.backend.room.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LeaderboardDTO {
    private List<String> playerUsernames;
    private List<Short> scores;
}
