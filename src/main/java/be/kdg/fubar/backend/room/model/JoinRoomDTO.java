package be.kdg.fubar.backend.room.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class JoinRoomDTO {
    private String username;
    private boolean joining;
}
