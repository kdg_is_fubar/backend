package be.kdg.fubar.backend.room.dal;

import be.kdg.fubar.backend.room.model.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface RoomRepository extends JpaRepository<Room, Long> {
}
