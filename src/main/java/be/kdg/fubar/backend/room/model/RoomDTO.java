package be.kdg.fubar.backend.room.model;

import be.kdg.fubar.backend.player.model.Player;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RoomDTO {
    private Long id;
    private String name;
    private int maxPlayerCount;
    private String username;
    private int aiCount;
    private int currentPlayerId;
    private List<Player> players;
    private long turnTime;
    private boolean gameStarted;
    private boolean hasEnded;
}