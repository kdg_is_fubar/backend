package be.kdg.fubar.backend.room.model;

import be.kdg.fubar.backend.player.model.Player;
import be.kdg.fubar.backend.tile.model.Coordinate;
import be.kdg.fubar.backend.tile.model.Tile;
import lombok.*;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Table(name = "Room")
@Entity
@Data
@NoArgsConstructor
public class Room {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter(AccessLevel.NONE)
    private Long id;
    private boolean gameStarted = false;
    @Column
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = {CascadeType.ALL}, targetEntity = Tile.class)
    private List<Tile> deck;
    @Column
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = {CascadeType.ALL}, targetEntity = Tile.class)
    private List<Tile> board;
    private int currentAdjecencyIdentifier = 1;
    @OneToOne
    private Tile lastPlacedTile;
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = {CascadeType.ALL}, targetEntity = Player.class)
    private List<Player> players;
    @ElementCollection
    private List<Short> playersScore;
    private String name;
    private int maxPlayerCount;
    private byte indexOfFirstTPlay;
    private byte currentPlayer;
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = {CascadeType.ALL}, targetEntity = Coordinate.class)
    private Set<Coordinate> emptyNeighbours;
    private byte aiCount;
    private boolean ended = false;
    private long turnTime = 20000;
    @OneToOne(targetEntity = TurnTimer.class, cascade = CascadeType.ALL)
    private TurnTimer timer;

    public Room(String name, int maxPlayerCount, int aiCount) {
        this.name = name;
        this.maxPlayerCount = maxPlayerCount;
        deck = new ArrayList<>();
        board = new ArrayList<>();
        players = new ArrayList<>();
        emptyNeighbours = new HashSet<>();
        playersScore = new ArrayList<>();
    }

    public Player getCurrentPlayer() {
        return players.get(currentPlayer);
    }

    public int getIndexCurrentPlayer() {
        return currentPlayer;
    }

    public void setCurrentPlayer(Player player) {
        this.currentPlayer = (byte) players.indexOf(player);
    }

    public void nextPlayer() {
        if (++currentPlayer > players.size() - 1)
            currentPlayer = 0;
    }

    public Player getHost() {
        return players.stream().filter(Player::isHost).findFirst().get();
    }
}