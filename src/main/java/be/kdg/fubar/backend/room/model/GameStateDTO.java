package be.kdg.fubar.backend.room.model;

import be.kdg.fubar.backend.player.model.PlayerDTO;
import be.kdg.fubar.backend.tile.model.Tile;
import be.kdg.fubar.backend.tile.model.TileDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GameStateDTO {
    private TileDTO[] tiles;
    private String gameName;
    private PlayerDTO[] players;
    private String username;
    private Tile drawnTile;
    private boolean hasEnded;
}
