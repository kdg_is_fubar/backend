package be.kdg.fubar.backend.room;

public class RoomException extends Exception {
    public RoomException(String message) {
        super(message);
    }
}
