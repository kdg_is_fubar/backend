package be.kdg.fubar.backend.room.config;

import be.kdg.fubar.backend.identity.bl.UserService;
import be.kdg.fubar.backend.identity.model.ApplicationUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

@Service
public class SessionHandler {

    private final Map<String, WebSocketSession> sessionMap = new ConcurrentHashMap<>();
    private UserService userService;

    public SessionHandler(UserService userService) {
       this.userService = userService;
    }

    public void register(WebSocketSession session) {

        ApplicationUser justConnectedUser = userService.loadUserByUsername(session.getPrincipal().getName());
        justConnectedUser.setWebsocketSession(session.getId());
        userService.updateUser(justConnectedUser);

        sessionMap.put(session.getId(), session);

    }

    public void unregister(WebSocketSession session) throws IOException {
        ApplicationUser justConnectedUser = userService.loadUserByUsername(session.getPrincipal().getName());
        if (justConnectedUser.getWebsocketSession() != null) {
            sessionMap.get(justConnectedUser.getWebsocketSession()).close();
            sessionMap.remove(justConnectedUser.getWebsocketSession());
        }
    }
}
